import { NgModule } from '@angular/core';

import { TabMenuModule } from 'primeng/tabmenu';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { CalendarModule } from 'primeng/calendar';
import { TooltipModule } from 'primeng/tooltip';
import { DropdownModule } from 'primeng/dropdown';
import { InputTextModule } from 'primeng/inputtext';
import { PasswordModule } from 'primeng/password';
import { PaginatorModule } from 'primeng/paginator';
import { CheckboxModule } from 'primeng/checkbox';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { TriStateCheckboxModule } from 'primeng/tristatecheckbox';
import { FieldsetModule } from 'primeng/fieldset';
import { InputSwitchModule } from 'primeng/inputswitch';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { RadioButtonModule } from 'primeng/radiobutton';
import { SliderModule } from 'primeng/slider';
import { InputMaskModule } from 'primeng/inputmask';
import { KeyFilterModule } from 'primeng/keyfilter';
import { SpinnerModule } from 'primeng/spinner';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { MenuModule } from 'primeng/menu';
import { ToggleButtonModule } from 'primeng/togglebutton';
import { TieredMenuModule } from 'primeng/tieredmenu';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { DialogModule } from 'primeng/dialog';
import { SelectButtonModule } from 'primeng/selectbutton';
import { DragDropModule } from 'primeng/dragdrop';

@NgModule({
    imports: [
        TabMenuModule,
        TableModule,
        ButtonModule,
        CalendarModule,
        TooltipModule,
        DropdownModule,
        InputTextModule,
        PasswordModule,
        PaginatorModule,
        CheckboxModule,
        ProgressSpinnerModule,
        TriStateCheckboxModule,
        FieldsetModule,
        InputSwitchModule,
        AutoCompleteModule,
        RadioButtonModule,
        SliderModule,
        InputMaskModule,
        KeyFilterModule,
        SpinnerModule,
        MessagesModule,
        MessageModule,
        ConfirmDialogModule,
        OverlayPanelModule,
        MenuModule,
        ToggleButtonModule,
        TieredMenuModule,
        ScrollPanelModule,
        DialogModule,
        SelectButtonModule,
        DragDropModule,
    ],
    exports: [
        TabMenuModule,
        TableModule,
        ButtonModule,
        CalendarModule,
        TooltipModule,
        DropdownModule,
        InputTextModule,
        PasswordModule,
        PaginatorModule,
        CheckboxModule,
        ProgressSpinnerModule,
        TriStateCheckboxModule,
        FieldsetModule,
        InputSwitchModule,
        AutoCompleteModule,
        RadioButtonModule,
        SliderModule,
        InputMaskModule,
        KeyFilterModule,
        SpinnerModule,
        MessagesModule,
        MessageModule,
        ConfirmDialogModule,
        OverlayPanelModule,
        MenuModule,
        ToggleButtonModule,
        TieredMenuModule,
        ScrollPanelModule,
        DialogModule,
        SelectButtonModule,
        DragDropModule,
    ],
})
export class PrimeNGModule {}
