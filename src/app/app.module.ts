import { NgModule, APP_INITIALIZER } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { AppConfigService } from './app-config.service';
import { MenubarComponent } from './menubar/menubar.component';
import { UserComponent } from './job-data/user.component';
import { JobComponent } from './job-data/job.component';
import { JobDetailComponent } from './job-data/job-detail.component';
import { ProjectComponent } from './job-data/project.component';
import { SearchComponent } from './search/search.component';
import { FilterComponent } from './job-data/filter.component';
import { ChartComponent } from './job-data/chart.component';
import { JobChartComponent } from './job-data/job-chart.component';
import { IssueComponent } from './job-data/issue.component';
import { IssueJobComponent } from './job-data/issue-job.component';
import { IssueJobDetailComponent } from './job-data/issue-job-detail.component';
import { NodeComponent } from './job-data/node.component';
import { ValidationComponent } from './job-data/validation.component';

import { PrimeNGModule } from './primeng.module';
import { PlotlyViaWindowModule } from 'angular-plotly.js';
import { ErrorComponent } from './error/error.component';
import { FluidHeightDirective } from './directives/fluid-height.directive';

const appInitializerFn = (appConfig: AppConfigService) => {
    return () => {
        return appConfig.loadAppConfig();
    };
};

@NgModule({
    declarations: [
        AppComponent,
        MenubarComponent,
        UserComponent,
        JobComponent,
        JobDetailComponent,
        ProjectComponent,
        SearchComponent,
        FilterComponent,
        ChartComponent,
        JobChartComponent,
        IssueComponent,
        IssueJobComponent,
        IssueJobDetailComponent,
        NodeComponent,
        ValidationComponent,
        ErrorComponent,
        FluidHeightDirective,
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule,
        PrimeNGModule,
        PlotlyViaWindowModule,
    ],
    providers: [
        AppConfigService,
        {
            provide: APP_INITIALIZER,
            useFactory: appInitializerFn,
            multi: true,
            deps: [AppConfigService],
        },
    ],
    bootstrap: [AppComponent],
})
export class AppModule {}
