import { Component, OnInit } from '@angular/core';
import { MonitoringService } from '../service/monitoring.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { SelectItem } from 'primeng/api';
import { EventManager } from '@angular/platform-browser';
import { ConfirmationService } from 'primeng/api';

const convert_footprint_dict: Record<string, any> = {
    'Mean IPC per Core': {
        code: 'ipc_mean_per_core',
        unit: '',
        unit_prefix: 1,
    },
    'Mean CPU Usage per Core': {
        code: 'cpu_used_mean_per_core',
        unit: '',
        unit_prefix: 1,
    },
    'Mean Normalized SP Flops per Core': {
        code: 'flops_any_mean_per_core',
        unit: 'GFlops',
        unit_prefix: 1e9,
    },
    'Mean Memory Bandwidth per Socket': {
        code: 'mem_bw_mean_per_socket',
        unit: 'GB/s',
        unit_prefix: 1e9,
    },
    'Mean CPU Power per Socket': {
        code: 'cpu_power_mean_per_socket',
        unit: 'W',
        unit_prefix: 1,
    },
    'Mean Infiniband Bandwidth per Node': {
        code: 'ib_bw_mean_per_node',
        unit: 'MB/s',
        unit_prefix: 1e6,
    },
    'Max Host Memory Usage per Node': {
        code: 'host_mem_used_max_per_node',
        unit: 'GB',
        unit_prefix: 1e9,
    },
    'Total Read Size': { code: 'read_bytes', unit: 'TB', unit_prefix: 1e12 },
    'Total Write Size': { code: 'write_bytes', unit: 'TB', unit_prefix: 1e12 },
    'Mean GPU Usage': { code: 'used_mean_per_gpu', unit: '', unit_prefix: 1 },
    'Max GPU Memory': {
        code: 'mem_used_max_per_gpu',
        unit: 'GB',
        unit_prefix: 1e9,
    },
    'Mean GPU Power': { code: 'power_mean_per_gpu', unit: 'W', unit_prefix: 1 },
};
const convert_time_in_seconds_dict: Record<string, number> = {
    min: 60,
    h: 3600,
    d: 86400,
    y: 31536000,
};

class ValueInterval {
    caption: string;
    storage_name: string;
    min: number | null;
    max: number | null;
    min_value: number | null;
    max_value: number | null;
    units: SelectItem[];
    selected_unit: string | null = null;

    constructor(
        caption: string,
        storage_name: string,
        min: number,
        max: number | null,
        units: string[]
    ) {
        this.caption = caption;
        this.storage_name = storage_name;
        this.min = min;
        if (max == null) this.max = Number.MAX_VALUE;
        else this.max = max;
        this.min_value = null;
        this.max_value = null;

        this.units = [];

        for (let i = 0; i < units.length; i++) {
            this.units.push({ label: units[i], value: units[i] });
        }

        if (units.length > 0) this.selected_unit = units[0];
    }

    checkInterval() {
        setTimeout(() => {
            if (this.min_value! > this.max_value!) {
                if (this.max_value != 0 && this.max_value != null) {
                    this.max_value = this.min_value;
                } else {
                    this.max_value = null;
                }
            }
        }, 4000);
    }
}

@Component({
    selector: 'app-search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.css'],
    providers: [ConfirmationService],
})
export class SearchComponent implements OnInit {
    /* top begin ************************************************************************/
    disable_time_interval: boolean;

    jobid: number | null = null;
    id_option: SelectItem[];
    selected_id_option: string | null = null;

    search_option: SelectItem[];
    selected_search_option: string | null = null;

    live_option: SelectItem[];
    selected_live_option: string | null = null;
    live_mode: string = '';

    allocation_type: SelectItem[];
    selected_allocation_type: string | null = null;
    /* top end **************************************************************************/

    /* left column begin *****************************************************************/
    projects: string[] = [];
    filtered_projects: any[] = [];
    project: string | null = null;
    project_loading: boolean;

    users: string[] = [];
    filtered_users: any[] = [];
    user: string | null = null;
    user_loading: boolean;
    admin_visible: boolean = false;

    jobstatus_option: string[];
    jobstatus: string | null = null;
    filtered_jobstatus: any[] = [];

    partitions: string[];
    selected_partition: string | null = null;
    filtered_partitions: any[] = [];

    smt_modes: any;
    selected_smt_mode: string | null | undefined = null;
    filtered_smt_modes: any[] = [];

    file_systems: string[];
    selected_file_system: string | null = null;
    filtered_file_systems: any[] = [];
    /* left column end *******************************************************************/

    /* middle column begin ***************************************************************/
    jobname: string | null = null;

    node_name: string | null = null;

    jobtag_bool: boolean[] = [];
    tag_num: number;
    jobtag_value: number[] | null = [];
    jobtag_label: string[] | null = [];
    selected_jobtags: string | null | undefined = null;
    jobtag_option: SelectItem[];
    selected_jobtag_option: string | null = null;

    footprints: string[];
    footprint_name: string | null = null;
    footprint_code: string | null = null;
    filteredFootprint: any[] = [];
    footprint_min: number | null = 0;
    footprint_max: number | null = 0;
    footprint_unit: string | null = null;
    footprint_unitprefix: number | null = null;
    /* middle column end ****************************************************************/

    /* right column begin ***************************************************************/
    interval_array: ValueInterval[];
    array_len: number = 0;
    /* right column end *****************************************************************/

    login_user: string;

    stateKey: string;

    noProject: string = 'No matching projects found';
    noUser: string = 'No matching users found';
    noJobstatus: string = 'Please select a correct status';
    noPartition: string = 'No matching partition found';
    noSMTMode: string = 'No matching SMT mode found';
    noFootprint: string = 'No matching footprint found';

    show_token_expired_dialog = false;

    constructor(
        private monitoringservice: MonitoringService,
        private route: ActivatedRoute,
        private router: Router,
        private eventManager: EventManager,
        private confirmationService: ConfirmationService
    ) {
        this.disable_time_interval = false;
        this.interval_array = [];

        this.login_user = this.monitoringservice.getUserRole();
        this.stateKey = 'search';
        this.monitoringservice.setActiveComponent(this.stateKey);

        this.monitoringservice.clearFilter();
        this.monitoringservice.clearFootprintFilter();

        this.id_option = [];
        this.id_option.push({ label: 'Job ID', value: 'job_id' });
        this.id_option.push({ label: 'Array ID', value: 'array_id' });

        this.search_option = [];
        this.search_option.push({ label: 'Live', value: 'Live' });
        this.search_option.push({ label: 'Job', value: 'Job' });
        this.search_option.push({ label: 'Footprint', value: 'Footprint' });

        this.live_option = [];
        this.live_option.push({ label: 'Job', value: 'Live-Job' });

        this.allocation_type = [
            { label: 'Exclusive', value: '1' },
            { label: 'Shared', value: '0' },
        ];
        this.partitions = this.monitoringservice.getPartitionList();
        this.file_systems = this.monitoringservice.getFileSystemInstanceNames();

        this.tag_num = this.monitoringservice.getTagNum();
        this.jobtag_option = [];
        this.jobtag_option.push({ label: 'Any', value: 'or' });
        this.jobtag_option.push({ label: 'All', value: 'and' });
        this.jobtag_label = this.monitoringservice.getTagLabel('label');

        this.footprints = [];
        for (let label in convert_footprint_dict) {
            this.footprints.push(label);
        }

        this.jobstatus_option = [
            'completed',
            'timeout',
            'failed',
            'cancelled',
            'OOM',
            'corrupt',
        ];

        this.smt_modes = new Map([
            ['1', '1 Hardware Thread per Core'],
            ['2', '2 Hardware Threads per Core'],
            ['4', '4 Hardware Threads per Core'],
        ]);

        // load default value
        this.defaultValue();

        // check user
        if (this.login_user == 'admin' || this.login_user == 'pi') {
            this.admin_visible = true;
            this.search_option.splice(1, 0, {
                label: 'Project',
                value: 'Project',
            });
            this.search_option.splice(2, 0, { label: 'User', value: 'User' });
            this.live_option.splice(0, 0, {
                label: 'User',
                value: 'Live-User',
            });
            // this.user = null;
        } else if (this.login_user != 'unknown') {
            this.admin_visible = false;
            // this.user = this.login_user;
        }

        // check if local storage is available
        if (localStorage.getItem('ID_OPTION')) {
            this.selected_id_option = localStorage.getItem('ID_OPTION');
            if (this.selected_id_option == 'null')
                this.selected_id_option = 'job_id';
            if (localStorage.getItem(this.selected_id_option!))
                this.jobid = Number(
                    localStorage.getItem(this.selected_id_option!)
                );
            if (this.jobid == 0) {
                this.jobid = null;
            }
        }

        if (localStorage.getItem('search-storage')) {
            // console.log("search-storage: ", localStorage.getItem('search-storage'));
            var searchStorage: any = JSON.parse(
                localStorage.getItem('search-storage')!
            );

            if (localStorage.getItem('SEARCHBY')) {
                this.selected_search_option = localStorage.getItem('SEARCHBY');
                if (this.selected_search_option == 'null')
                    this.selected_search_option = null;
            }
            // console.log("Liveoption: ", localStorage.getItem('LIVEOPTION'));
            if (
                localStorage.getItem('LIVEOPTION') &&
                this.monitoringservice.enablePendingJobs()
            ) {
                this.selected_live_option = localStorage.getItem('LIVEOPTION');
                if (this.selected_live_option == 'null')
                    this.selected_live_option = null;
            }
            if (
                localStorage.getItem('LIVEMODE') &&
                this.monitoringservice.enablePendingJobs()
            ) {
                var livemodeValue = localStorage.getItem('LIVEMODE');
                this.live_mode =
                    livemodeValue == null ? 'running' : livemodeValue;
            }

            if (searchStorage['job_name_like'])
                this.jobname = searchStorage['job_name_like'];
            if (searchStorage['project_name'])
                this.project = searchStorage['project_name'];
            if (searchStorage['user_name'])
                this.user = searchStorage['user_name'];
            if (searchStorage['partition_name_like'])
                this.selected_partition = searchStorage['partition_name_like'];
            if (searchStorage['smt_mode'])
                this.selected_smt_mode = this.smt_modes.get(
                    searchStorage['smt_mode']
                );
            if (searchStorage['fsname'])
                this.selected_file_system = searchStorage['fsname'];
            if (searchStorage['node_name_like'])
                this.node_name = searchStorage['node_name_like'];
            if (searchStorage['footprint']) {
                this.footprint_code = searchStorage['footprint']['name'];
                this.footprint_name = localStorage.getItem('FOOTPRINTS');
                this.footprint_unit = localStorage.getItem('FOOTPRINT_UNIT');
                this.footprint_unitprefix = Number(
                    localStorage.getItem('FOOTPRINT_UNITPREFIX')
                );

                this.footprint_min = null;
                this.footprint_max = null;
                if (searchStorage['footprint'].length > 0) {
                    if (searchStorage['footprint'][0]['min'])
                        this.footprint_min =
                            searchStorage['footprint'][0]['min'] /
                            this.footprint_unitprefix;
                    if (searchStorage['footprint'][0]['max'])
                        this.footprint_max =
                            searchStorage['footprint'][0]['max'] /
                            this.footprint_unitprefix;
                }
            }
            if (searchStorage['job_state'])
                this.jobstatus = searchStorage['job_state'];
            if (localStorage.getItem('JOBTAG_OPTION')) {
                this.selected_jobtag_option =
                    localStorage.getItem('JOBTAG_OPTION');
            }
            if (searchStorage['tags']) {
                this.jobtag_value = searchStorage['tags'];
                for (let i = 0; i < this.tag_num; i++) {
                    this.jobtag_bool[i] =
                        localStorage.getItem(
                            this.jobtag_label![i] + '_BOOL'
                        ) === 'true';
                }
                this.selected_jobtags = localStorage.getItem('SELECTED_JTG');
            }

            if (searchStorage['exclusive']) {
                this.selected_allocation_type = searchStorage['exclusive'];
            }

            /* get values for third column */
            for (let i = 0; i < this.interval_array.length; i++) {
                if (searchStorage[this.interval_array[i].storage_name]) {
                    /* convert string to array */
                    var values =
                        searchStorage[this.interval_array[i].storage_name];

                    /* check for stored units and restore them */
                    if (this.interval_array[i].selected_unit) {
                        if (
                            localStorage.getItem(
                                this.interval_array[i].storage_name + '_unit'
                            )
                        ) {
                            this.interval_array[i].selected_unit =
                                localStorage.getItem(
                                    this.interval_array[i].storage_name +
                                        '_unit'
                                );

                            /* restore values with current unit */
                            this.interval_array[i].min_value =
                                values['min'] /
                                convert_time_in_seconds_dict[
                                    this.interval_array[i].selected_unit!
                                ];
                            this.interval_array[i].max_value =
                                values['max'] /
                                convert_time_in_seconds_dict[
                                    this.interval_array[i].selected_unit!
                                ];
                        }
                    } else {
                        /* restore values */
                        this.interval_array[i].min_value = values['min'];
                        this.interval_array[i].max_value = values['max'];
                    }
                    if (!this.interval_array[i].min_value) {
                        this.interval_array[i].min_value = null;
                    }
                    if (!this.interval_array[i].max_value) {
                        this.interval_array[i].max_value = null;
                    }
                }
            }
            if (this.selected_search_option == 'Live') {
                this.interval_array.pop();
                this.interval_array.pop();
                this.interval_array.pop();
            }
        }

        this.project_loading = true;
        this.user_loading = true;
    }

    ngOnInit() {
        this.loadProjectsAndUsers();

        this.eventManager.addEventListener(document.body, 'keyup.esc', () => {
            this.confirmReset();
        });
    }

    loadProjectsAndUsers() {
        // set current date filter
        this.monitoringservice.addFilterItem(
            'use_date_interval',
            (!this.disable_time_interval).toString()
        );

        this.project_loading = false;
        this.user_loading = false;
        if (this.disable_time_interval == false) {
            console.log('Reload projects and users...');
            // check if live or not
            if (this.selected_search_option != 'Live') {
                this.monitoringservice.addFilterItem('live', 'false');
            } else {
                this.monitoringservice.addFilterItem('live', 'true');
            }

            // get project names
            this.projects = [];
            this.project_loading = true;
            this.monitoringservice.getStatisticData('project').subscribe({
                next: (data: any) => {
                    for (var key in data) {
                        this.projects.push(data[key]['project_name']);
                    }
                    // console.log("Search Projects: ", this.projects);
                    this.project_loading = false;
                },
                error: (e) => {
                    this.project_loading = false;
                    this.show_token_expired_dialog =
                        this.monitoringservice.showExpiredSessionDialog(e);
                },
            });

            if (this.login_user == 'admin' || this.login_user == 'pi') {
                // get user names
                this.users = [];
                this.user_loading = true;
                this.monitoringservice.getStatisticData('user').subscribe({
                    next: (data: any) => {
                        for (var key in data) {
                            this.users.push(data[key]['user_name']);
                        }
                        //console.log("Search Users: ", this.users);
                        this.user_loading = false;
                    },
                    error: (e) => {
                        this.user_loading = false;
                        this.show_token_expired_dialog =
                            this.monitoringservice.showExpiredSessionDialog(e);
                    },
                });
            }
        }
    }

    confirmReset() {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to reset all filters?',
            accept: () => {
                this.reset();
            },
        });
    }

    reset() {
        this.monitoringservice.clearFilter();
        sessionStorage.removeItem('_filter');
        sessionStorage.clear();
        this.clearFilterSession();
        localStorage.removeItem('job_id');
        localStorage.removeItem('array_id');
        localStorage.removeItem('search-storage');
        this.defaultValue();
    }

    reload() {
        this.loadProjectsAndUsers();
        // location.reload();
    }

    defaultValue() {
        this.jobid = null;
        this.selected_id_option = 'job_id';
        this.selected_search_option = null;
        this.selected_live_option = null;
        this.live_mode = 'running';
        this.selected_allocation_type = null;

        this.disable_time_interval = false;

        this.project = null;
        if (this.login_user == 'admin' || this.login_user == 'pi') {
            this.user = null;
        }
        this.jobstatus = null;
        this.selected_partition = null;
        this.selected_smt_mode = null;
        this.selected_file_system = null;

        this.jobname = null;
        this.node_name = null;
        this.jobtag_bool = Array<boolean>(this.tag_num);
        for (let i = 0; i < this.tag_num; i++) {
            this.jobtag_bool[i] = false;
        }
        this.tag_num = this.monitoringservice.getTagNum();
        this.jobtag_label = this.monitoringservice.getTagLabel('label');
        this.jobtag_value = [];
        this.selected_jobtag_option = 'or';
        this.selected_jobtags = null;
        this.footprint_code = null;
        this.footprint_name = null;
        this.footprint_min = null;
        this.footprint_max = null;
        this.footprint_unit = null;

        /* set default values for third column */
        this.interval_array = [];
        this.interval_array.push(
            new ValueInterval(
                'Number of Nodes',
                'node_count_interval',
                0,
                2000,
                []
            )
        );
        this.interval_array.push(
            new ValueInterval(
                'Number of Cores',
                'core_count_interval',
                0,
                null,
                []
            )
        );
        this.interval_array.push(
            new ValueInterval(
                'Number of GPUs',
                'gpu_count_interval',
                0,
                null,
                []
            )
        );
        this.interval_array.push(
            new ValueInterval('Time Limit', 'time_limit_interval', 0, null, [
                'min',
                'h',
                'd',
            ])
        );
        this.interval_array.push(
            new ValueInterval(
                'Pending Time',
                'pending_time_interval',
                0,
                null,
                ['min', 'h', 'd']
            )
        );
        this.interval_array.push(
            new ValueInterval('Duration', 'duration_interval', 0, null, [
                'min',
                'h',
                'd',
            ])
        );
        this.interval_array.push(
            new ValueInterval(
                'Core Duration',
                'core_duration_interval',
                0,
                null,
                ['h', 'd', 'y']
            )
        );
        this.array_len = this.interval_array.length;
    }
    getSearchChange(event: any) {
        if (this.selected_search_option != null) {
            this.footprint_name = null;
            this.footprint_code = null;
        }
        if (this.interval_array.length == this.array_len) {
            if (this.selected_search_option == 'Live') {
                this.interval_array.pop();
                this.interval_array.pop();
                this.interval_array.pop();
            }
        } else {
            if (this.selected_search_option != 'Live') {
                this.interval_array.push(
                    new ValueInterval(
                        'Pending Time',
                        'pending_time_interval',
                        0,
                        null,
                        ['min', 'h', 'd']
                    )
                );
                this.interval_array.push(
                    new ValueInterval(
                        'Duration',
                        'duration_interval',
                        0,
                        null,
                        ['min', 'h', 'd']
                    )
                );
                this.interval_array.push(
                    new ValueInterval(
                        'Core Duration',
                        'core_duration_interval',
                        0,
                        null,
                        ['h', 'd', 'y']
                    )
                );
            }
        }
    }
    filterProjects(event: any) {
        this.filtered_projects = [];
        for (let i = 0; i < this.projects.length; i++) {
            let project = this.projects[i];
            if (project.toLowerCase().indexOf(event.query.toLowerCase()) == 0) {
                this.filtered_projects.push(project);
            }
        }
    }

    filterUsers(event: any) {
        this.filtered_users = [];
        for (let i = 0; i < this.users.length; i++) {
            let user = this.users[i];
            if (user.toLowerCase().indexOf(event.query.toLowerCase()) == 0) {
                this.filtered_users.push(user);
            }
        }
    }

    filterPartitions(event: any) {
        this.filtered_partitions = [];
        for (let i = 0; i < this.partitions.length; i++) {
            let selected_partition = this.partitions[i];
            if (
                selected_partition
                    .toLowerCase()
                    .indexOf(event.query.toLowerCase()) == 0
            ) {
                this.filtered_partitions.push(selected_partition);
            }
        }
    }

    filterSMTModes(event: any) {
        this.filtered_smt_modes = [];
        for (let [, value] of this.smt_modes) {
            let selected_smt_mode = value;
            if (
                selected_smt_mode
                    .toLowerCase()
                    .indexOf(event.query.toLowerCase()) == 0
            ) {
                this.filtered_smt_modes.push(selected_smt_mode);
            }
        }
    }

    filterFileSystems(event: any) {
        this.filtered_file_systems = [];
        for (let i = 0; i < this.file_systems.length; i++) {
            let selected_file_system = this.file_systems[i];
            if (
                selected_file_system
                    .toLowerCase()
                    .indexOf(event.query.toLowerCase()) == 0
            ) {
                this.filtered_file_systems.push(selected_file_system);
            }
        }
    }

    filterJobstatus(event: any) {
        this.filtered_jobstatus = [];
        for (let i = 0; i < this.jobstatus_option.length; i++) {
            let jobstatus = this.jobstatus_option[i];
            if (
                jobstatus.toLowerCase().indexOf(event.query.toLowerCase()) == 0
            ) {
                this.filtered_jobstatus.push(jobstatus);
            }
        }
    }

    checkJobtags() {
        var label_value = this.monitoringservice.getTagLabel('value');
        var selected_tag_num = 0;
        var jobtag_arr: number[] = [];
        var jobtag_display;
        this.jobtag_value = [];
        for (let i = 0; i < this.tag_num; i++) {
            if (this.jobtag_bool[i]) {
                jobtag_arr.push(Number(label_value![i]));
                selected_tag_num++;
            }
        }
        if (this.selected_jobtag_option == 'or') {
            this.jobtag_value = jobtag_arr;
        } else {
            if (jobtag_arr.length > 1) {
                this.jobtag_value = [eval(jobtag_arr.join('+'))];
            } else {
                this.jobtag_value = jobtag_arr;
            }
        }
        for (let i = 0; i < this.tag_num; i++) {
            if (this.jobtag_bool[i]) {
                jobtag_display = this.monitoringservice.getJobtags(
                    label_value![i]
                );
                break;
            }
        }
        if (selected_tag_num == 0) {
            this.selected_jobtags = null;
        } else if (selected_tag_num == 1) {
            this.selected_jobtags = jobtag_display;
        } else {
            this.selected_jobtags = `${jobtag_display} ${
                this.selected_jobtag_option
            } ${selected_tag_num - 1} more`;
        }
    }

    filterFootprint(event: any) {
        this.filteredFootprint = [];
        for (let i = 0; i < this.footprints.length; i++) {
            let footprint_name = this.footprints[i];
            if (
                footprint_name
                    .toLowerCase()
                    .indexOf(event.query.toLowerCase()) == 0
            ) {
                this.filteredFootprint.push(footprint_name);
            }
        }
    }

    footprintChange(event: any) {
        this.footprint_min = null;
        this.footprint_max = null;
        if (this.footprint_name) {
            this.footprint_unit =
                convert_footprint_dict[this.footprint_name].unit;
        } else {
            this.footprint_code = null;
        }
    }

    searchJobId() {
        console.log('jobid', this.jobid);
        this.monitoringservice.clearFilter();
        sessionStorage.removeItem('_filter');
        sessionStorage.clear();
        this.clearFilterSession();
        localStorage.removeItem(this.selected_id_option!);
        localStorage.setItem('ID_OPTION', this.selected_id_option!);
        if (this.jobid) {
            this.monitoringservice.addFilterItem(
                this.selected_id_option!,
                this.jobid.toString()
            );
            this.monitoringservice.addFilterItem('use_date_interval', 'false');
            localStorage.setItem(
                this.selected_id_option!,
                this.jobid.toString()
            );
            var navigationSearchText =
                this.selected_id_option + '=' + this.jobid.toString();
            this.router.navigate([navigationSearchText, 'job-details'], {
                relativeTo: this.route,
            });
        }
    }

    search() {
        this.monitoringservice.clearFilter();
        sessionStorage.removeItem('_filter');
        sessionStorage.clear();
        this.clearFilterSession();
        localStorage.removeItem('job_id');
        localStorage.removeItem('array_id');
        ///////

        if (this.jobname)
            this.monitoringservice.addFilterItem('job_name_like', this.jobname);
        if (this.project)
            this.monitoringservice.addFilterItem('project_name', this.project);
        if (this.user && this.user != 'unknown')
            this.monitoringservice.addFilterItem('user_name', this.user);
        if (this.selected_partition)
            this.monitoringservice.addFilterItem(
                'partition_name_like',
                this.selected_partition
            );
        if (this.selected_smt_mode)
            this.monitoringservice.addFilterItem(
                'smt_mode',
                this.getKeyByValue(this.smt_modes, this.selected_smt_mode)
            );
        if (this.selected_file_system)
            this.monitoringservice.addFilterItem(
                'fsname',
                this.selected_file_system
            );
        if (this.node_name)
            this.monitoringservice.addFilterItem(
                'node_name_like',
                this.node_name
            );
        if (this.footprint_name) {
            this.footprint_code =
                convert_footprint_dict[this.footprint_name].code;
            this.footprint_unitprefix =
                convert_footprint_dict[this.footprint_name].unit_prefix;
            localStorage.setItem('FOOTPRINTS', this.footprint_name);
            localStorage.setItem('FOOTPRINT_UNIT', this.footprint_unit!);
            localStorage.setItem(
                'FOOTPRINT_UNITPREFIX',
                this.footprint_unitprefix!.toString()
            );
            let filter_interval: { [key: string]: any } = {};
            filter_interval['name'] = this.footprint_code;
            if (this.footprint_min || this.footprint_max) {
                this.footprint_min =
                    this.footprint_min! * this.footprint_unitprefix!;
                this.footprint_max =
                    this.footprint_max! * this.footprint_unitprefix!;
                if (this.footprint_min == 0) {
                    this.footprint_min = null;
                }
                if (this.footprint_max == 0) {
                    this.footprint_max = null;
                }

                filter_interval['min'] = this.footprint_min?.toString();
                filter_interval['max'] = this.footprint_max?.toString();
            }
            let filter_interval_array = [];
            filter_interval_array.push(filter_interval);
            this.monitoringservice.addFilterItem(
                'footprint',
                filter_interval_array
            );
        }
        if (this.jobstatus) {
            if (this.selected_search_option != 'Live') {
                this.monitoringservice.addFilterItem(
                    'job_state',
                    this.jobstatus
                );
            }
        } else {
            if (this.selected_search_option != 'Live') {
                this.monitoringservice.addFilterItem('live', 'false');
            }
        }

        for (let i = 0; i < this.tag_num; i++) {
            localStorage.setItem(
                this.jobtag_label![i] + '_BOOL',
                this.jobtag_bool[i].toString()
            );
        }

        if (this.jobtag_value && this.jobtag_value.length > 0) {
            this.monitoringservice.addFilterItem('tags', this.jobtag_value);
        }
        if (this.selected_jobtags) {
            localStorage.setItem('SELECTED_JTG', this.selected_jobtags);
        }

        if (this.selected_allocation_type != null) {
            this.monitoringservice.addFilterItem(
                'exclusive',
                this.selected_allocation_type
            );
        }

        this.monitoringservice.addFilterItem(
            'use_date_interval',
            (!this.disable_time_interval).toString()
        );

        /////////

        /* add filters for third column */
        for (let i = 0; i < this.interval_array.length; i++) {
            if (
                this.interval_array[i].min_value ||
                this.interval_array[i].max_value
            ) {
                // console.log("Filter: ", this.interval_array[i].storage_name);

                var min_value = this.interval_array[i].min_value;
                var max_value = this.interval_array[i].max_value;

                // when the filtering of the number of GPUs is enabled, the minimum value is 1 instead of null
                if (
                    this.interval_array[i].storage_name ==
                        'gpu_count_interval' &&
                    min_value == null
                )
                    min_value = 1;

                // check if filter contains units
                if (this.interval_array[i].selected_unit) {
                    // convert values into seconds
                    if (min_value) {
                        min_value *=
                            convert_time_in_seconds_dict[
                                this.interval_array[i].selected_unit!
                            ];
                    }
                    if (max_value) {
                        max_value *=
                            convert_time_in_seconds_dict[
                                this.interval_array[i].selected_unit!
                            ];
                    }
                }
                let filter_interval = {};
                filter_interval = {
                    min: min_value?.toString(),
                    max: max_value?.toString(),
                };
                this.monitoringservice.addFilterItem(
                    this.interval_array[i].storage_name.toLowerCase(),
                    filter_interval
                );

                // store current unit in localStore
                localStorage.setItem(
                    this.interval_array[i].storage_name + '_unit',
                    this.interval_array[i].selected_unit!
                );
            }
        }

        // store all settings in local storage
        localStorage.setItem(
            'search-storage',
            this.monitoringservice.getFilterClause(true)
        );
        localStorage.setItem('ID_OPTION', this.selected_id_option!);
        localStorage.setItem('SEARCHBY', this.selected_search_option!);
        if (this.monitoringservice.enablePendingJobs()) {
            localStorage.setItem('LIVEOPTION', this.selected_live_option!);
            localStorage.setItem('LIVEMODE', this.live_mode);
        }
        localStorage.setItem('JOBTAG_OPTION', this.selected_jobtag_option!);
        if (this.jobid) {
            localStorage.setItem(
                this.selected_id_option!,
                this.jobid.toString()
            );
        }
        if (this.selected_search_option == 'Live') {
            this.monitoringservice.setLiveMode(this.live_mode);
            var live_mode_capitalized =
                this.live_mode.charAt(0).toUpperCase() +
                this.live_mode.slice(1);
            if (this.selected_live_option == 'Live-User') {
                this.router.navigate(
                    [
                        this.selected_search_option +
                            '-' +
                            live_mode_capitalized,
                        'users-live',
                    ],
                    { relativeTo: this.route }
                );
            } else if (this.selected_live_option == 'Live-Job') {
                this.router.navigate(
                    [
                        this.selected_search_option +
                            '-' +
                            live_mode_capitalized,
                        'jobs-live',
                    ],
                    { relativeTo: this.route }
                );
            } else {
                this.router.navigate(
                    [
                        this.selected_search_option +
                            '-' +
                            live_mode_capitalized,
                        'details-live',
                    ],
                    { relativeTo: this.route }
                );
            }
        } else if (this.selected_search_option == 'Project')
            this.router.navigate([this.selected_search_option, 'projects'], {
                relativeTo: this.route,
            });
        else if (this.selected_search_option == 'User')
            this.router.navigate([this.selected_search_option, 'users'], {
                relativeTo: this.route,
            });
        else if (this.selected_search_option == 'Footprint')
            this.router.navigate([this.selected_search_option, 'footprints'], {
                relativeTo: this.route,
            });
        else if (this.selected_search_option == 'Job')
            this.router.navigate([this.selected_search_option, 'jobs'], {
                relativeTo: this.route,
            });
        else
            this.router.navigate(['Detail', 'job-details'], {
                relativeTo: this.route,
            });
    }

    clearFilterSession() {
        var arr = []; // Array to hold the keys
        // Iterate over sessionStorage and insert the keys that meet the condition into arr
        for (var i = 0; i < sessionStorage.length; i++) {
            if (sessionStorage.key(i)!.includes('search/filter__'))
                arr.push(sessionStorage.key(i));
        }
        // Iterate over arr and remove the items by key
        for (var i = 0; i < arr.length; i++) {
            sessionStorage.removeItem(arr[i]!);
        }
    }

    getCurrentYear() {
        return this.monitoringservice.getCurrentYear();
    }

    getKeyByValue(
        map: Map<string, string>,
        searchValue: string
    ): string | undefined {
        for (let [key, value] of map) {
            if (value === searchValue) {
                return key;
            }
        }
        return undefined; // Return undefined if the value is not found
    }
}
