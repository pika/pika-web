import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserComponent } from './job-data/user.component';
import { JobComponent } from './job-data/job.component';
import { JobDetailComponent } from './job-data/job-detail.component';
import { JobChartComponent } from './job-data/job-chart.component';
import { ProjectComponent } from './job-data/project.component';
import { SearchComponent } from './search/search.component';
import { FilterComponent } from './job-data/filter.component';
import { ChartComponent } from './job-data/chart.component';
import { IssueComponent } from './job-data/issue.component';
import { IssueJobComponent } from './job-data/issue-job.component';
import { IssueJobDetailComponent } from './job-data/issue-job-detail.component';
import { NodeComponent } from './job-data/node.component';
import { ErrorComponent } from './error/error.component';
import { UserGuard, RoleGuard } from './role.guard';

const routes: Routes = [
    //User View ****************************************************************
    /* Live */
    {
        path: 'user/jobs-live',
        data: { live_tab_start: 'true', state: 'live', clear_filter: 'true' },
        component: JobComponent,
        canActivate: [UserGuard],
    },
    {
        path: 'user/jobs-live/:job',
        data: { live_tab_start: 'true', state: 'live' },
        component: JobDetailComponent,
        canActivate: [UserGuard],
    },
    {
        path: 'user/jobs-live/:job/:jobid/:start/:partition',
        data: { state: 'live' },
        component: JobChartComponent,
        canActivate: [UserGuard],
    },

    /* Job */
    {
        path: 'user/jobs',
        data: { clear_filter: 'true' },
        component: JobComponent,
        canActivate: [UserGuard],
    },
    { path: 'user/jobs/:job', component: JobDetailComponent },
    {
        path: 'user/jobs/:job/footprints',
        data: { footprint_link: 'job' },
        component: ChartComponent,
        canActivate: [UserGuard],
    },
    {
        path: 'user/jobs/:job/footprints/:footprint',
        data: { footprint_link: 'job' },
        component: FilterComponent,
        canActivate: [UserGuard],
    },
    {
        path: 'user/jobs/:job/footprints/:footprint/:jobid/:start/:partition',
        data: { footprint_link: 'job' },
        component: JobChartComponent,
        canActivate: [UserGuard],
    },

    {
        path: 'user/jobs/:job/:jobid/:start/:partition',
        component: JobChartComponent,
        canActivate: [UserGuard],
    },

    /* Footprint */
    {
        path: 'user/footprints',
        data: {
            state: 'all',
            footprint_link: 'none',
            clear_filter: 'true',
            clear_footprint_filter: 'true',
        },
        component: ChartComponent,
        canActivate: [UserGuard],
    },
    {
        path: 'user/footprints/:footprint',
        data: { state: 'all', footprint_link: 'none' },
        component: FilterComponent,
        canActivate: [UserGuard],
    },
    {
        path: 'user/footprints/:footprint/:jobid/:start/:partition',
        data: { state: 'all', footprint_link: 'none' },
        component: JobChartComponent,
        canActivate: [UserGuard],
    },

    /* Search */
    {
        path: 'user/search',
        data: { state: 'all' },
        component: SearchComponent,
        canActivate: [UserGuard],
    },

    /* Search-Detail */
    {
        path: 'user/search/:filter/job-details',
        data: { state: 'all' },
        component: FilterComponent,
        canActivate: [UserGuard],
    },
    {
        path: 'user/search/:filter/job-details/:jobid/:start/:partition',
        data: { state: 'all' },
        component: JobChartComponent,
        canActivate: [UserGuard],
    },

    /* Search-Live */
    {
        path: 'user/search/:filter/jobs-live',
        data: { state: 'live' },
        component: JobComponent,
        canActivate: [UserGuard],
    },
    {
        path: 'user/search/:filter/jobs-live/:job',
        data: { state: 'live' },
        component: JobDetailComponent,
        canActivate: [UserGuard],
    },
    {
        path: 'user/search/:filter/jobs-live/:job/:jobid/:start/:partition',
        data: { state: 'live' },
        component: JobChartComponent,
        canActivate: [UserGuard],
    },

    {
        path: 'user/search/:filter/details-live',
        data: { state: 'live' },
        component: JobDetailComponent,
        canActivate: [UserGuard],
    },
    {
        path: 'user/search/:filter/details-live/:jobid/:start/:partition',
        data: { state: 'live' },
        component: JobChartComponent,
        canActivate: [UserGuard],
    },

    /* Search-Job */
    {
        path: 'user/search/:filter/jobs',
        data: { state: 'all' },
        component: JobComponent,
        canActivate: [UserGuard],
    },
    {
        path: 'user/search/:filter/jobs/:job',
        data: { state: 'all' },
        component: JobDetailComponent,
        canActivate: [UserGuard],
    },
    {
        path: 'user/search/:filter/jobs/:job/footprints',
        data: { state: 'all', footprint_link: 'job' },
        component: ChartComponent,
        canActivate: [UserGuard],
    },
    {
        path: 'user/search/:filter/jobs/:job/:footprint',
        data: { state: 'all', footprint_link: 'job' },
        component: FilterComponent,
        canActivate: [UserGuard],
    },
    {
        path: 'user/search/:filter/jobs/:job/:footprint/:jobid/:start/:partition',
        data: { state: 'all', footprint_link: 'job' },
        component: JobChartComponent,
        canActivate: [UserGuard],
    },

    {
        path: 'user/search/:filter/jobs/:job/:jobid/:start/:partition',
        data: { state: 'all' },
        component: JobChartComponent,
        canActivate: [UserGuard],
    },

    /* Search-Footprint */
    {
        path: 'user/search/:filter/footprints',
        data: { state: 'all', footprint_link: 'none' },
        component: ChartComponent,
        canActivate: [UserGuard],
    },
    {
        path: 'user/search/:filter/footprints/:footprint',
        data: { state: 'all', footprint_link: 'none' },
        component: FilterComponent,
        canActivate: [UserGuard],
    },
    {
        path: 'user/search/:filter/footprints/:footprint/:jobid/:start/:partition',
        data: { state: 'all', footprint_link: 'none' },
        component: JobChartComponent,
        canActivate: [UserGuard],
    },

    /* Issue */
    {
        path: 'user/issues',
        data: { state: 'all', footprint_link: 'none', clear_filter: 'true' },
        component: IssueJobComponent,
        canActivate: [UserGuard],
    },
    {
        path: 'user/issues/:job',
        data: { state: 'all', footprint_link: 'none' },
        component: IssueJobDetailComponent,
        canActivate: [UserGuard],
    },
    {
        path: 'user/issues/:job/:jobid/:start/:partition',
        data: { state: 'all', footprint_link: 'none' },
        component: JobChartComponent,
        canActivate: [UserGuard],
    },

    /* Node */
    {
        path: 'user/nodes',
        data: { state: 'all', footprint_link: 'none', clear_filter: 'true' },
        component: NodeComponent,
        canActivate: [UserGuard],
    },

    //Admin or PI View ****************************************************************
    /* Live */
    {
        path: ':role/users-live',
        data: { live_tab_start: 'true', state: 'live', clear_filter: 'true' },
        component: UserComponent,
        canActivate: [RoleGuard],
    },
    {
        path: ':role/users-live/:user',
        data: { live_tab_start: 'true', state: 'live' },
        component: JobComponent,
        canActivate: [RoleGuard],
    },
    {
        path: ':role/users-live/:user/:job',
        data: { live_tab_start: 'true', state: 'live' },
        component: JobDetailComponent,
        canActivate: [RoleGuard],
    },
    {
        path: ':role/users-live/:user/:job/:jobid/:start/:partition',
        data: { state: 'live' },
        component: JobChartComponent,
        canActivate: [RoleGuard],
    },

    /* Project */
    {
        path: ':role/projects',
        data: { clear_filter: 'true' },
        component: ProjectComponent,
        canActivate: [RoleGuard],
    },

    {
        path: ':role/projects/:project',
        component: UserComponent,
        canActivate: [RoleGuard],
    },
    {
        path: ':role/projects/:project/footprints',
        data: { footprint_link: 'project' },
        component: ChartComponent,
        canActivate: [RoleGuard],
    },
    {
        path: ':role/projects/:project/footprints/:footprint',
        data: { footprint_link: 'project' },
        component: FilterComponent,
        canActivate: [RoleGuard],
    },
    {
        path: ':role/projects/:project/footprints/:footprint/:jobid/:start/:partition',
        data: { footprint_link: 'project' },
        component: JobChartComponent,
        canActivate: [RoleGuard],
    },

    {
        path: ':role/projects/:project/:user',
        component: JobComponent,
        canActivate: [RoleGuard],
    },
    {
        path: ':role/projects/:project/:user/footprints',
        data: { footprint_link: 'user' },
        component: ChartComponent,
        canActivate: [RoleGuard],
    },
    {
        path: ':role/projects/:project/:user/footprints/:footprint',
        data: { footprint_link: 'user' },
        component: FilterComponent,
        canActivate: [RoleGuard],
    },
    {
        path: ':role/projects/:project/:user/footprints/:footprint/:jobid/:start/:partition',
        data: { footprint_link: 'user' },
        component: JobChartComponent,
        canActivate: [RoleGuard],
    },

    {
        path: ':role/projects/:project/:user/:job',
        component: JobDetailComponent,
        canActivate: [RoleGuard],
    },
    {
        path: ':role/projects/:project/:user/:job/footprints',
        data: { footprint_link: 'job' },
        component: ChartComponent,
        canActivate: [RoleGuard],
    },
    {
        path: ':role/projects/:project/:user/:job/footprints/:footprint',
        data: { footprint_link: 'job' },
        component: FilterComponent,
        canActivate: [RoleGuard],
    },
    {
        path: ':role/projects/:project/:user/:job/footprints/:footprint/:jobid/:start/:partition',
        data: { footprint_link: 'job' },
        component: JobChartComponent,
        canActivate: [RoleGuard],
    },

    {
        path: ':role/projects/:project/:user/:job/:jobid/:start/:partition',
        component: JobChartComponent,
        canActivate: [RoleGuard],
    },

    /* User */
    {
        path: ':role/users',
        data: { clear_filter: 'true' },
        component: UserComponent,
        canActivate: [RoleGuard],
    },

    { path: ':role/users/:user', component: JobComponent },
    {
        path: ':role/users/:user/footprints',
        data: { footprint_link: 'user' },
        component: ChartComponent,
        canActivate: [RoleGuard],
    },
    {
        path: ':role/users/:user/footprints/:footprint',
        data: { footprint_link: 'user' },
        component: FilterComponent,
        canActivate: [RoleGuard],
    },
    {
        path: ':role/users/:user/footprints/:footprint/:jobid/:start/:partition',
        data: { footprint_link: 'user' },
        component: JobChartComponent,
        canActivate: [RoleGuard],
    },

    { path: ':role/users/:user/:job', component: JobDetailComponent },
    {
        path: ':role/users/:user/:job/footprints',
        data: { footprint_link: 'job' },
        component: ChartComponent,
        canActivate: [RoleGuard],
    },
    {
        path: ':role/users/:user/:job/footprints/:footprint',
        data: { footprint_link: 'job' },
        component: FilterComponent,
        canActivate: [RoleGuard],
    },
    {
        path: ':role/users/:user/:job/footprints/:footprint/:jobid/:start/:partition',
        data: { footprint_link: 'job' },
        component: JobChartComponent,
        canActivate: [RoleGuard],
    },

    {
        path: ':role/users/:user/:job/:jobid/:start/:partition',
        component: JobChartComponent,
        canActivate: [RoleGuard],
    },

    /* Job */
    {
        path: ':role/jobs',
        data: { clear_filter: 'true' },
        component: JobComponent,
        canActivate: [RoleGuard],
    },

    { path: ':role/jobs/:job', component: JobDetailComponent },
    {
        path: ':role/jobs/:job/footprints',
        data: { footprint_link: 'job' },
        component: ChartComponent,
        canActivate: [RoleGuard],
    },
    {
        path: ':role/jobs/:job/footprints/:footprint',
        data: { footprint_link: 'job' },
        component: FilterComponent,
        canActivate: [RoleGuard],
    },
    {
        path: ':role/jobs/:job/footprints/:footprint/:jobid/:start/:partition',
        data: { footprint_link: 'job' },
        component: JobChartComponent,
        canActivate: [RoleGuard],
    },

    {
        path: ':role/jobs/:job/:jobid/:start/:partition',
        component: JobChartComponent,
        canActivate: [RoleGuard],
    },

    /* Footprint */
    {
        path: ':role/footprints',
        data: {
            state: 'all',
            footprint_link: 'none',
            clear_filter: 'true',
            clear_footprint_filter: 'true',
        },
        component: ChartComponent,
        canActivate: [RoleGuard],
    },
    {
        path: ':role/footprints/:footprint',
        data: { state: 'all', footprint_link: 'none' },
        component: FilterComponent,
        canActivate: [RoleGuard],
    },
    {
        path: ':role/footprints/:footprint/:jobid/:start/:partition',
        data: { state: 'all', footprint_link: 'none' },
        component: JobChartComponent,
        canActivate: [RoleGuard],
    },

    /* Search */
    {
        path: ':role/search',
        data: { state: 'all' },
        component: SearchComponent,
        canActivate: [RoleGuard],
    },

    /* Search-Detail */
    {
        path: ':role/search/:filter/job-details',
        data: { state: 'all' },
        component: FilterComponent,
        canActivate: [RoleGuard],
    },
    {
        path: ':role/search/:filter/job-details/:jobid/:start/:partition',
        data: { state: 'all' },
        component: JobChartComponent,
        canActivate: [RoleGuard],
    },

    /* Search-Live */
    {
        path: ':role/search/:filter/users-live',
        data: { state: 'live' },
        component: UserComponent,
        canActivate: [RoleGuard],
    },
    {
        path: ':role/search/:filter/users-live/:user',
        data: { state: 'live' },
        component: JobComponent,
        canActivate: [RoleGuard],
    },
    {
        path: ':role/search/:filter/users-live/:user/:job',
        data: { state: 'live' },
        component: JobDetailComponent,
        canActivate: [RoleGuard],
    },
    {
        path: ':role/search/:filter/users-live/:user/:job/:jobid/:start/:partition',
        data: { state: 'live' },
        component: JobChartComponent,
        canActivate: [RoleGuard],
    },

    {
        path: ':role/search/:filter/jobs-live',
        data: { state: 'live' },
        component: JobComponent,
        canActivate: [RoleGuard],
    },
    {
        path: ':role/search/:filter/jobs-live/:job',
        data: { state: 'live' },
        component: JobDetailComponent,
        canActivate: [RoleGuard],
    },
    {
        path: ':role/search/:filter/jobs-live/:job/:jobid/:start/:partition',
        data: { state: 'live' },
        component: JobChartComponent,
        canActivate: [RoleGuard],
    },

    {
        path: ':role/search/:filter/details-live',
        data: { state: 'live' },
        component: JobDetailComponent,
        canActivate: [RoleGuard],
    },
    {
        path: ':role/search/:filter/details-live/:jobid/:start/:partition',
        data: { state: 'live' },
        component: JobChartComponent,
        canActivate: [RoleGuard],
    },

    /* Search-Project */
    {
        path: ':role/search/:filter/projects',
        data: { state: 'all' },
        component: ProjectComponent,
        canActivate: [RoleGuard],
    },
    {
        path: ':role/search/:filter/projects/:project',
        data: { state: 'all' },
        component: UserComponent,
        canActivate: [RoleGuard],
    },
    {
        path: ':role/search/:filter/projects/:project/footprints',
        data: { state: 'all', footprint_link: 'project' },
        component: ChartComponent,
        canActivate: [RoleGuard],
    },
    {
        path: ':role/search/:filter/projects/:project/footprints/:footprint',
        data: { state: 'all', footprint_link: 'project' },
        component: FilterComponent,
        canActivate: [RoleGuard],
    },
    {
        path: ':role/search/:filter/projects/:project/footprints/:footprint/:jobid/:start/:partition',
        data: { state: 'all', footprint_link: 'project' },
        component: JobChartComponent,
        canActivate: [RoleGuard],
    },

    {
        path: ':role/search/:filter/projects/:project/:user',
        data: { state: 'all' },
        component: JobComponent,
        canActivate: [RoleGuard],
    },
    {
        path: ':role/search/:filter/projects/:project/:user/footprints',
        data: { state: 'all', footprint_link: 'user' },
        component: ChartComponent,
        canActivate: [RoleGuard],
    },
    {
        path: ':role/search/:filter/projects/:project/:user/footprints/:footprint',
        data: { state: 'all', footprint_link: 'user' },
        component: FilterComponent,
        canActivate: [RoleGuard],
    },
    {
        path: ':role/search/:filter/projects/:project/:user/footprints/:footprint/:jobid/:start/:partition',
        data: { state: 'all', footprint_link: 'user' },
        component: JobChartComponent,
        canActivate: [RoleGuard],
    },

    {
        path: ':role/search/:filter/projects/:project/:user/:job',
        data: { state: 'all' },
        component: JobDetailComponent,
        canActivate: [RoleGuard],
    },
    {
        path: ':role/search/:filter/projects/:project/:user/:job/footprints',
        data: { state: 'all', footprint_link: 'job' },
        component: ChartComponent,
        canActivate: [RoleGuard],
    },
    {
        path: ':role/search/:filter/projects/:project/:user/:job/footprints/:footprint',
        data: { state: 'all', footprint_link: 'job' },
        component: FilterComponent,
        canActivate: [RoleGuard],
    },
    {
        path: ':role/search/:filter/projects/:project/:user/:job/footprints/:footprint/:jobid/:start/:partition',
        data: { state: 'all', footprint_link: 'job' },
        component: JobChartComponent,
        canActivate: [RoleGuard],
    },

    {
        path: ':role/search/:filter/projects/:project/:user/:job/:jobid/:start/:partition',
        data: { state: 'all' },
        component: JobChartComponent,
        canActivate: [RoleGuard],
    },

    /* Search-User */
    {
        path: ':role/search/:filter/users',
        data: { state: 'all' },
        component: UserComponent,
        canActivate: [RoleGuard],
    },
    {
        path: ':role/search/:filter/users/:user',
        data: { state: 'all' },
        component: JobComponent,
        canActivate: [RoleGuard],
    },
    {
        path: ':role/search/:filter/users/:user/footprints',
        data: { state: 'all', footprint_link: 'user' },
        component: ChartComponent,
        canActivate: [RoleGuard],
    },
    {
        path: ':role/search/:filter/users/:user/footprints/:footprint',
        data: { state: 'all', footprint_link: 'user' },
        component: FilterComponent,
        canActivate: [RoleGuard],
    },
    {
        path: ':role/search/:filter/users/:user/footprints/:footprint/:jobid/:start/:partition',
        data: { state: 'all', footprint_link: 'user' },
        component: JobChartComponent,
        canActivate: [RoleGuard],
    },

    {
        path: ':role/search/:filter/users/:user/:job',
        data: { state: 'all' },
        component: JobDetailComponent,
        canActivate: [RoleGuard],
    },
    {
        path: ':role/search/:filter/users/:user/:job/footprints',
        data: { state: 'all', footprint_link: 'job' },
        component: ChartComponent,
        canActivate: [RoleGuard],
    },
    {
        path: ':role/search/:filter/users/:user/:job/footprints/:footprint',
        data: { state: 'all', footprint_link: 'job' },
        component: FilterComponent,
        canActivate: [RoleGuard],
    },
    {
        path: ':role/search/:filter/users/:user/:job/footprints/:footprint/:jobid/:start/:partition',
        data: { state: 'all', footprint_link: 'job' },
        component: JobChartComponent,
        canActivate: [RoleGuard],
    },

    {
        path: ':role/search/:filter/users/:user/:job/:jobid/:start/:partition',
        data: { state: 'all' },
        component: JobChartComponent,
        canActivate: [RoleGuard],
    },

    /* Search-Job */
    {
        path: ':role/search/:filter/jobs',
        data: { state: 'all' },
        component: JobComponent,
        canActivate: [RoleGuard],
    },
    {
        path: ':role/search/:filter/jobs/:job',
        data: { state: 'all' },
        component: JobDetailComponent,
        canActivate: [RoleGuard],
    },
    {
        path: ':role/search/:filter/jobs/:job/footprints',
        data: { state: 'all', footprint_link: 'job' },
        component: ChartComponent,
        canActivate: [RoleGuard],
    },
    {
        path: ':role/search/:filter/jobs/:job/footprints/:footprint',
        data: { state: 'all', footprint_link: 'job' },
        component: FilterComponent,
        canActivate: [RoleGuard],
    },
    {
        path: ':role/search/:filter/jobs/:job/footprints/:footprint/:jobid/:start/:partition',
        data: { state: 'all', footprint_link: 'job' },
        component: JobChartComponent,
        canActivate: [RoleGuard],
    },

    {
        path: ':role/search/:filter/jobs/:job/:jobid/:start/:partition',
        component: JobChartComponent,
        canActivate: [RoleGuard],
    },

    /* Search-Footprint */
    {
        path: ':role/search/:filter/footprints',
        data: { state: 'all', footprint_link: 'none' },
        component: ChartComponent,
        canActivate: [RoleGuard],
    },
    {
        path: ':role/search/:filter/footprints/:footprint',
        data: { state: 'all', footprint_link: 'none' },
        component: FilterComponent,
        canActivate: [RoleGuard],
    },
    {
        path: ':role/search/:filter/footprints/:footprint/:jobid/:start/:partition',
        data: { state: 'all', footprint_link: 'none' },
        component: JobChartComponent,
        canActivate: [RoleGuard],
    },

    /* Issue */
    {
        path: ':role/issues',
        data: { state: 'all', clear_filter: 'true' },
        component: IssueComponent,
        canActivate: [RoleGuard],
    },
    {
        path: ':role/issues/:user',
        data: { state: 'all', footprint_link: 'none' },
        component: IssueJobComponent,
        canActivate: [RoleGuard],
    },
    {
        path: ':role/issues/:user/:job',
        data: { state: 'all', footprint_link: 'none' },
        component: IssueJobDetailComponent,
        canActivate: [RoleGuard],
    },
    {
        path: ':role/issues/:user/:job/:jobid/:start/:partition',
        data: { state: 'all', footprint_link: 'none' },
        component: JobChartComponent,
        canActivate: [RoleGuard],
    },

    /* Node */
    {
        path: ':role/nodes',
        data: { state: 'all', clear_filter: 'true' },
        component: NodeComponent,
        canActivate: [RoleGuard],
    },

    /* Validation */
    //   { path: ':role/validation', data: {state: 'all'}, component: ValidationComponent },
    //   { path: ':role/validation/:jobid/:start/:partition', data: {state: 'all', footprint_link: 'none'}, component: JobChartComponent },

    /* Error */
    { path: 'error', data: { state: 'all' }, component: ErrorComponent },
    { path: '**', data: { state: 'all' }, component: ErrorComponent },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule {}
