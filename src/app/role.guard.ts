import { CanActivateFn, Router } from '@angular/router';
import { inject } from '@angular/core';
import { MonitoringService } from './service/monitoring.service';

export const RoleGuard: CanActivateFn = (route) => {
    const monitoringservice = inject(MonitoringService);
    const router = inject(Router);
    const allowedRoles = ['admin', 'pi'];
    const role = route.paramMap.get('role');
    if (
        role &&
        allowedRoles.includes(role) &&
        role == monitoringservice.getUserRole()
    ) {
        return true;
    } else {
        // Redirect to default page
        return router.navigate(['/']);
    }
};

export const UserGuard: CanActivateFn = (route) => {
    const monitoringservice = inject(MonitoringService);
    const router = inject(Router);
    if (
        route.url?.toString().startsWith('user') &&
        monitoringservice.getUserRole() == 'user'
    ) {
        return true;
    } else {
        // Redirect to default page
        return router.navigate(['/']);
    }
};
