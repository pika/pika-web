import { Component, OnInit, HostListener } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { MonitoringService } from '../service/monitoring.service';
import { SelectItem } from 'primeng/api';
//import { fromEvent } from 'rxjs';
//import { debounceTime } from 'rxjs/operators';

@Component({
    selector: 'app-menubar',
    templateUrl: './menubar.component.html',
    styleUrls: ['./menubar.component.css'],
})
export class MenubarComponent implements OnInit {
    show_menu_large: boolean = false;
    show_menu_small: boolean = false;
    items: MenuItem[] = [];
    no_icon_items: MenuItem[] = [];
    rangeDates: Date[] = [];
    current_user: string = '';
    user_param: string = '';
    minDateValue: Date = new Date('September 1, 2018 00:00:00');
    timefilters: SelectItem[] = [];
    selectedTimeFilter: number = 0;
    showUser: boolean = false;
    is_pi: boolean = false;
    is_admin: boolean = false;
    logout_url: string = '';
    hideIcons = false;

    calendar_width: number = 310;
    calendar_width_offset: number = 0;
    date_format: string = 'dd/mm/yy';
    hour_format: number = 24;

    admin_width_large_icon: number = 0;
    admin_width_menu_small: number = 0;
    admin_width_small_icon: number = 0;
    user_width_large_icon: number = 0;
    user_width_menu_small: number = 0;
    user_width_small_icon: number = 0;
    node_tab_width: number = 0;
    node_icon_tab_width: number = 0;
    vertical_scrollbar_width: number = 0;

    roles: any = [];
    selected_role: string = 'user';
    user_icons: any = {
        user: 'fa-solid fa-user',
        pi: 'fa-solid fa-user-group', //'fa-solid fa-user-tie', //'fa-solid fa-user-plus',
        admin: 'fa-solid fa-user-plus', //'fa-solid fa-user-gear', //'fa-solid fa-user-lock',
    };
    current_user_icon = this.user_icons['user'];

    constructor(private monitoringservice: MonitoringService) {}

    ngOnInit() {
        // determine time format
        if (this.monitoringservice.getTimeFormat() == 'en-US') {
            this.calendar_width = 365;
            this.calendar_width_offset = 55;
            this.date_format = 'mm/dd/yy';
            this.hour_format = 12;
        }

        if (this.monitoringservice.enablePendingJobs()) {
            this.node_tab_width = 74;
            this.node_icon_tab_width = 100;
        }
        this.admin_width_large_icon =
            1265 +
            this.node_icon_tab_width +
            this.vertical_scrollbar_width +
            this.calendar_width_offset;
        this.admin_width_menu_small =
            1088 +
            this.node_tab_width +
            this.vertical_scrollbar_width +
            this.calendar_width_offset;
        this.admin_width_small_icon =
            760 +
            this.node_icon_tab_width +
            this.vertical_scrollbar_width +
            this.calendar_width_offset;

        this.user_width_large_icon =
            1150 +
            this.node_icon_tab_width +
            this.vertical_scrollbar_width +
            this.calendar_width_offset;
        this.user_width_menu_small =
            995 +
            this.node_tab_width +
            this.vertical_scrollbar_width +
            this.calendar_width_offset;
        this.user_width_small_icon =
            625 +
            this.node_icon_tab_width +
            this.vertical_scrollbar_width +
            this.calendar_width_offset;

        this.show_menu_large = false;
        this.show_menu_small = false;

        this.timefilters = [
            { label: 'Last hour', value: 3600 },
            { label: 'Last day', value: 3600 * 24 },
            { label: 'Last week', value: 3600 * 24 * 7 },
            { label: 'Last month', value: 3600 * 24 * 7 * 4.4 },
            { label: 'Last year', value: 3600 * 24 * 7 * 4.4 * 12 },
        ];

        this.selectedTimeFilter = 3600 * 24 * 7;

        // create menu bar after reset
        console.log(
            `user: ${this.monitoringservice.getUser()}`,
            `isAdmin=${this.monitoringservice.getAdminStatus()}`,
            `isPI=${this.monitoringservice.getPIStatus()}`,
            `role=${this.monitoringservice.getUserRole()}`
        );
        this.is_pi = this.monitoringservice.getPIStatus();
        this.is_admin = this.monitoringservice.getAdminStatus();

        this.selected_role = this.monitoringservice.getUserRole();
        this.current_user = this.monitoringservice.getUser();
        this.current_user_icon = this.user_icons[this.selected_role];

        if (this.is_pi && !this.is_admin) {
            this.roles.push(
                {
                    label: ' User',
                    value: 'user',
                    icon: this.user_icons['user'],
                },
                {
                    label: 'PI',
                    value: 'pi',
                    icon: this.user_icons['pi'],
                }
            );
        } else if (!this.is_pi && this.is_admin) {
            this.roles.push(
                {
                    label: ' User',
                    value: 'user',
                    icon: this.user_icons['user'],
                },
                {
                    label: 'Admin',
                    value: 'admin',
                    icon: this.user_icons['admin'],
                }
            );
        } else if (this.is_pi && this.is_admin) {
            this.roles.push(
                {
                    label: ' User',
                    value: 'user',
                    icon: this.user_icons['user'],
                },
                {
                    label: 'PI',
                    value: 'pi',
                    icon: this.user_icons['pi'],
                },
                {
                    label: 'Admin',
                    value: 'admin',
                    icon: this.user_icons['admin'],
                }
            );
        }

        if (
            this.current_user != 'unknown' &&
            this.monitoringservice.clientServerCompatible()
        ) {
            this.init();
        }

        // fromEvent(window, 'resize')
        //     .pipe(debounceTime(0))
        //     .subscribe(() => {
        //         if (this.current_user !== 'unknown') {
        //             this.adjustMenuWidth();
        //         }
        //     });
    }

    init() {
        this.items = [];
        this.no_icon_items = [];
        this.rangeDates = this.monitoringservice.getGlobalRangeDates();
        this.showUser = true;
        this.logout_url = this.monitoringservice.getLogoutURL();

        this.createMenu(this.items);
        this.createMenu(this.no_icon_items, false);
        setTimeout(() => {
            this.adjustMenuWidth();
        }, 0);
    }

    createMenu(items: MenuItem[], show_icon = true) {
        if (this.selected_role == 'admin' || this.selected_role == 'pi') {
            items.push(
                ...[
                    // {
                    //     label: 'Pend', routerLink: ['/admin/search'],
                    //     icon: show_icon ? 'fa-regular fa-hourglass-half' : undefined,
                    //     tooltip: "Show pending jobs"
                    // },
                    {
                        label: 'Live',
                        routerLink: ['/', this.selected_role, 'users-live'],
                        icon: show_icon ? 'pi pi-fw pi-clock' : undefined,
                    },
                    {
                        label: 'Project',
                        routerLink: ['/', this.selected_role, 'projects'],
                        icon: show_icon ? 'pi pi-fw pi-users' : undefined,
                    },
                    {
                        label: 'User',
                        routerLink: ['/', this.selected_role, 'users'],
                        icon: show_icon ? 'pi pi-fw pi-user' : undefined,
                    },
                    {
                        label: 'Job',
                        routerLink: ['/', this.selected_role, 'jobs'],
                        icon: show_icon ? 'pi pi-fw pi-file' : undefined,
                    },
                    {
                        label: 'Footprint',
                        routerLink: ['/', this.selected_role, 'footprints'],
                        icon: show_icon ? 'pi pi-fw pi-chart-bar' : undefined,
                    },
                    {
                        label: 'Search',
                        routerLink: ['/', this.selected_role, 'search'],
                        icon: show_icon ? 'pi pi-fw pi-search' : undefined,
                    },
                    {
                        label: 'Issue',
                        routerLink: ['/', this.selected_role, 'issues'],
                        icon: show_icon ? 'pi pi-fw pi-bell' : undefined,
                    },
                ]
            );
            if (this.monitoringservice.enablePendingJobs())
                items.push({
                    label: 'Node',
                    routerLink: ['/', this.selected_role, 'nodes'],
                    icon: show_icon ? 'pi pi-fw pi-sliders-h' : undefined,
                });
        } else {
            items.push(
                ...[
                    // {
                    //     label: 'Pend', routerLink: ['/', this.current_user, 'search'],
                    //     icon: show_icon ? 'fa-regular fa-hourglass-half' : undefined
                    // },
                    {
                        label: 'Live',
                        routerLink: ['/', this.selected_role, 'jobs-live'],
                        icon: show_icon ? 'pi pi-fw pi-clock' : undefined,
                    },
                    {
                        label: 'Job',
                        routerLink: ['/', this.selected_role, 'jobs'],
                        icon: show_icon ? 'pi pi-fw pi-file' : undefined,
                    },
                    {
                        label: 'Footprint',
                        routerLink: ['/', this.selected_role, 'footprints'],
                        icon: show_icon ? 'pi pi-fw pi-chart-bar' : undefined,
                    },
                    {
                        label: 'Search',
                        routerLink: ['/', this.selected_role, 'search'],
                        icon: show_icon ? 'pi pi-fw pi-search' : undefined,
                    },
                    {
                        label: 'Issue',
                        routerLink: ['/', this.selected_role, 'issues'],
                        icon: show_icon ? 'pi pi-fw pi-bell' : undefined,
                    },
                ]
            );
            if (this.monitoringservice.enablePendingJobs())
                items.push({
                    label: 'Node',
                    routerLink: ['/', this.selected_role, 'nodes'],
                    icon: show_icon ? 'pi pi-fw pi-sliders-h' : undefined,
                });
            items.push({
                label: 'Help',
                url: 'https://doc.zih.tu-dresden.de/software/pika/',
                target: '_blank',
                icon: show_icon ? 'pi pi-fw pi-question' : undefined,
            });
        }
    }

    adjustMenuWidth() {
        // check available space and set hideIcons flag

        // this is the width without a possible scrollbar
        var window_width = document.documentElement.clientWidth;
        if (['admin', 'pi'].includes(this.selected_role)) {
            if (window_width > this.admin_width_menu_small) {
                this.show_menu_large = true;
                this.show_menu_small = false;
                this.hideIcons = window_width <= this.admin_width_large_icon;
            } else {
                this.show_menu_large = false;
                this.show_menu_small = true;
                this.hideIcons = window_width <= this.admin_width_small_icon;
            }
        } else {
            if (window_width > this.user_width_menu_small) {
                this.show_menu_large = true;
                this.show_menu_small = false;
                this.hideIcons = window_width <= this.user_width_large_icon;
            } else {
                this.show_menu_large = false;
                this.show_menu_small = true;
                this.hideIcons = window_width <= this.user_width_small_icon;
            }
        }
    }

    handleRangeDates(rangeDates: Date[]) {
        //check end value
        if (rangeDates[1] == null) {
            rangeDates[1] = new Date();
            this.rangeDates = [rangeDates[0], rangeDates[1]];
            console.log('New Date: ', rangeDates);
        }

        // console.log("New Date: ", rangeDates);
        // console.log("Last Date: ", rangeDates);

        // only update job data if new date was set by user
        if (rangeDates != this.monitoringservice.getGlobalRangeDates()) {
            this.monitoringservice.setGlobalRangeDates(rangeDates);
            console.log('Session clear');
            sessionStorage.clear();
            this.monitoringservice.reloadData();
        }
    }

    fastFilter() {
        var start_date = new Date();
        start_date.setTime(
            start_date.getTime() - 1000 * this.selectedTimeFilter
        );
        var end_date = new Date();

        this.rangeDates = [start_date, end_date];
    }

    switchRole(event: any) {
        localStorage.setItem('user_switch_requested', 'yes');
        localStorage.setItem('user_role', this.selected_role);
        location.reload();
    }

    @HostListener('window:resize', ['$event'])
    onResize(event: any) {
        if (this.current_user != 'unknown') this.adjustMenuWidth();
    }
}
