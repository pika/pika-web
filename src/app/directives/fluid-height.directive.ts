import {
    AfterViewInit,
    Directive,
    ElementRef,
    HostBinding,
    Input,
    OnDestroy,
    Renderer2,
} from '@angular/core';
import { fromEvent, Subscription } from 'rxjs';
import { debounceTime, throttleTime } from 'rxjs/operators';
import { MonitoringService } from '../service/monitoring.service';

@Directive({
    selector: '[fluidHeight]',
})
export class FluidHeightDirective implements AfterViewInit, OnDestroy {
    @Input() minHeight: number | undefined;
    @Input('fluidHeight') topOffset: number | undefined;
    @HostBinding('style.overflow-y') overflowY = 'auto';
    @HostBinding('style.overflow-x') overflowX = 'hidden';

    private domElement: HTMLElement;
    private resizeSub: Subscription;
    private tableHeaderSub: Subscription;

    constructor(
        private renderer: Renderer2,
        private elementRef: ElementRef,
        monitoringservice: MonitoringService
    ) {
        // get ref HTML element
        this.domElement = this.elementRef.nativeElement as HTMLElement;

        // register on window resize event
        this.resizeSub = fromEvent(window, 'resize')
            .pipe(throttleTime(500), debounceTime(500))
            .subscribe(() => this.setHeight());

        // register on table header resize event
        this.tableHeaderSub =
            monitoringservice.JobDataTableHeaderResized.subscribe(() => {
                this.setHeight();
            });
    }

    ngAfterViewInit() {
        this.setHeight();
    }

    ngOnDestroy() {
        this.resizeSub.unsubscribe();
    }

    private setHeight() {
        // console.log('setHeight....');
        const windowHeight = window?.innerHeight;
        const topOffset = this.topOffset || this.calcTopOffset();
        let height = windowHeight - topOffset;

        if (this.minHeight && height < this.minHeight) {
            height = this.minHeight;
        }

        this.renderer.setStyle(this.domElement, 'height', `${height}px`);
    }

    private calcTopOffset(): number {
        try {
            const rect = this.domElement.getBoundingClientRect();
            const scrollTop =
                window.scrollY || document.documentElement.scrollTop;

            return rect.top + scrollTop + 10;
        } catch (e) {
            return 0;
        }
    }
}
