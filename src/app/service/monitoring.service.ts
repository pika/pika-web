import { Injectable, Output, EventEmitter } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { AppConfigService } from '../app-config.service';
import { NavigationEnd, Router } from '@angular/router';

@Injectable({
    providedIn: 'root',
})
export class MonitoringService {
    // global date filter
    global_range_dates: any;
    global_start: any;
    global_end: any;
    filterData: any;
    PartitionConfig: string[] = [];
    activeComponent: string;
    lastURL: string;

    // table config
    sort_field: string = 'core_duration_idle';

    // current value from issue table
    issue_value: string = 'cpu';

    // current partition in node view
    node_partition_value: string = 'all';

    // current value from job table
    live_value: string = 'running';

    // footprint filter
    footprintFilterData: any;

    // mark all routes that contain footprint charts (needed for footprint filter)
    footprintRoute: boolean;

    utc_client_offset: number;

    io_timeline_captions: Record<string, any> = {};

    // used for reloading job table after changing date in menubar
    @Output() Reload: EventEmitter<string> = new EventEmitter();
    // used for peforming synchronous actions in timelines, e.g. zooming
    @Output() TimelinePropagation: EventEmitter<any> = new EventEmitter();
    // used to adjusted height for timeline charts after table header resized
    @Output() JobDataTableHeaderResized: EventEmitter<any> = new EventEmitter();

    constructor(
        private http: HttpClient,
        private appConfig: AppConfigService,
        private router: Router
    ) {
        // set default time range
        var start_date = new Date();
        this.utc_client_offset = start_date.getTimezoneOffset();

        if (this.getUserRole() == 'admin') {
            this.activeComponent = 'user-live';
            start_date.setDate(start_date.getDate() - 7);
        } else if (this.getUserRole() == 'pi') {
            this.activeComponent = 'projects';
            start_date.setDate(start_date.getDate() - 28);
        } else {
            this.activeComponent = 'jobs';
            start_date.setDate(start_date.getDate() - 180);
        }

        this.router.events.subscribe((event) => {
            if (event instanceof NavigationEnd) {
                if (
                    event.urlAfterRedirects.toString() == '/' ||
                    event.urlAfterRedirects.toString() == '/error' ||
                    localStorage.getItem('user_switch_requested') == 'yes'
                ) {
                    localStorage.setItem('user_switch_requested', 'no');
                    if (this.getUserRole() == 'admin')
                        this.router.navigate([
                            this.getUserRole(),
                            'users-live',
                        ]);
                    else if (this.getUserRole() == 'pi')
                        this.router.navigate([this.getUserRole(), 'projects']);
                    else if (this.getUser() != 'unknown')
                        this.router.navigate([this.getUserRole(), 'jobs']);
                    else {
                        // implement Error component!!!
                        this.router.navigate(['error']);
                    }
                }
                /* if the user has refreshed a page, check if url contains "search" or "footprint" and
                   truncate the end of the url as the filter was deleted. */
                let url_check = event.urlAfterRedirects.toString().split('/');
                if (url_check.length > 3) {
                    /* page is reloaded by the user if event.id = 1 */
                    if (url_check[2] == 'footprints' && event.id == 1) {
                        this.router.navigate([
                            this.getUserRole(),
                            'footprints',
                        ]);
                    } else if (
                        url_check[2] == 'search' &&
                        !url_check[3].includes('job_id') &&
                        !url_check[3].includes('array_id') &&
                        event.id == 1 &&
                        url_check.length < 8
                    ) {
                        /* refreshing the timeline view from search tab is allowed */
                        this.router.navigate([this.getUserRole(), 'search']);
                    } else if (
                        decodeURIComponent(url_check[3]).includes('job_id=')
                    ) {
                        this.addFilterItem(
                            'job_id',
                            decodeURIComponent(url_check[3]).split('=')[1]
                        );
                        this.addFilterItem('use_date_interval', 'false');
                    } else if (
                        decodeURIComponent(url_check[3]).includes('array_id=')
                    ) {
                        this.addFilterItem(
                            'array_id',
                            decodeURIComponent(url_check[3]).split('=')[1]
                        );
                        this.addFilterItem('use_date_interval', 'false');
                    }
                }
            }
        });

        var end_date = new Date();

        // for testing
        // start_date = new   Date(1679353200000);
        // var end_date = new Date(1679439600000);

        this.global_range_dates = [start_date, end_date];

        // unix timestamps for queries
        this.global_start = Date.parse(this.global_range_dates[0]) / 1000;
        this.global_end = Date.parse(this.global_range_dates[1]) / 1000;

        console.log(
            'start_time:',
            start_date,
            this.global_start,
            'UTCTimezone:',
            this.getUTCTimezone()
        );

        // clean up all sessions
        sessionStorage.clear();

        // collection of elements of the current filter
        this.filterData = {};

        // collection of elements of the current footprint filter
        this.footprintFilterData = {};
        this.lastURL = '';
        this.footprintRoute = false;

        // create io captions for single io metrics
        this.createIOTimelineCaptions();
    }

    getTimeFormat() {
        return this.appConfig.getConfig().AppConfig.time_format;
    }

    getDateFormat(
        two_digit_year: boolean = false,
        skip_seconds: boolean = false
    ) {
        const year = two_digit_year ? '%y' : '%Y';
        const seconds = skip_seconds ? '' : ':%S';
        let date_format_string = `%d/%m/${year} %H:%M${seconds}`;
        if (this.getTimeFormat() == 'en-US')
            date_format_string = `%m/%d/${year} %I:%M${seconds} %p`;
        return date_format_string;
    }

    createIOTimelineCaptions() {
        const file_systems = this.appConfig.getConfig().FileSystems;
        // console.log(file_systems);
        for (const category in file_systems) {
            for (const io_type in file_systems[category]) {
                const label = file_systems[category][io_type].label;
                const unit = file_systems[category][io_type].unit;
                const metrics = file_systems[category][io_type].options;
                for (const metric in metrics) {
                    this.io_timeline_captions[metric] = [
                        label + ' IO',
                        unit,
                        'submenu-option',
                        '(% across %)',
                        'node',
                    ];
                }
            }
        }
    }

    clientServerCompatible() {
        return this.appConfig.clientServerCompatible();
    }

    getFormattedTime(unixtime: any) {
        //console.log("unixtime: ", unixtime);
        if (unixtime != 'n/a') {
            var local_unixtime = unixtime;
            var d = new Date(local_unixtime * 1000);
            var hour_12: boolean = false;

            if (this.getTimeFormat() == 'en-US') {
                hour_12 = true;
            }

            const dateOptions: Intl.DateTimeFormatOptions = {
                year: '2-digit',
                month: '2-digit',
                day: '2-digit',
            };
            const timeOptions: Intl.DateTimeFormatOptions = {
                hour: '2-digit',
                minute: '2-digit',
                second: '2-digit',
                hour12: hour_12,
            };
            const datePart = d.toLocaleDateString(
                this.getTimeFormat(),
                dateOptions
            );
            const timePart = d.toLocaleTimeString(
                this.getTimeFormat(),
                timeOptions
            );

            return `${datePart} ${timePart}`;
        } else {
            return unixtime;
        }
    }

    getUTCTimezone() {
        if (this.utc_client_offset == 0) return '00:00';

        // Determine the sign
        const sign = this.utc_client_offset <= 0 ? '+' : '-';

        // Convert minutes to positive value
        const absMinutes = Math.abs(this.utc_client_offset);

        // Calculate hours and minutes
        const hours = Math.floor(absMinutes / 60);
        const remainingMinutes = absMinutes % 60;

        // Format the timezone offset
        const formattedHours = hours.toString().padStart(2, '0');
        const formattedMinutes = remainingMinutes.toString().padStart(2, '0');

        return `${sign}${formattedHours}:${formattedMinutes}`;
    }

    setFootprintRoute(flag: boolean) {
        this.footprintRoute = flag;
    }

    getBackend() {
        return this.appConfig.getConfig().AppConfig.backend;
    }

    enablePendingJobs(): boolean {
        return this.appConfig.getConfig().AppConfig.pending_jobs;
    }

    enableShowEnergy(): boolean {
        return this.appConfig.getConfig().AppConfig.show_energy;
    }

    getHeaders() {
        //console.log('current_user', this.getUser(), this.appConfig.getTokenFromBackend(this.getUser()));

        // check if token will be expired soon and renew it in advance
        // if (this.appConfig.getTokenExpiredTimestamp() - Math.floor(Date.now() / 1000) < 600) {
        //     console.log("Token expired. Generating new token...");
        //     this.appConfig.loadAppConfig();
        // }

        const token = this.appConfig.getTokenFromBackend(this.getUserRole());
        return { 'content-type': 'application/json', Authorization: token };
    }

    getLogoutURL() {
        return this.appConfig.getConfig().AppConfig.logout_url;
    }

    addFilterItem(key: string, value: any) {
        this.filterData[key] = value;
    }

    getFilterItem(key: string) {
        return this.filterData[key];
    }

    getFilterClause(localStorage: boolean = false) {
        var filter: any = {};
        if (!localStorage) {
            filter['format_result'] = {
                utc_timezone: this.getUTCTimezone(),
                time_format: this.getDateFormat(),
            };
        }

        var dateFilter = true;
        if (this.getFilterItem('use_date_interval')) {
            if (!localStorage) {
                filter['use_date_interval'] =
                    this.getFilterItem('use_date_interval');
            }
            if (this.getFilterItem('use_date_interval') == 'false') {
                this.removeFilterItem('start_time');
                this.removeFilterItem('end_time');
                dateFilter = false;
            }
        }

        if (dateFilter == true) {
            if (this.getFilterItem('live') == 'true') {
                //set time for live jobs
                var start_date = new Date();
                start_date.setDate(start_date.getDate() - 14);
                var end_date = new Date();
                var start: any = Date.parse(start_date.toString()) / 1000;
                var end: any = Date.parse(end_date.toString()) / 1000;
                this.addFilterItem('start_time', start);
                this.addFilterItem('end_time', end);
            } else {
                this.addFilterItem('start_time', String(this.global_start));
                this.addFilterItem('end_time', String(this.global_end));
            }
        }

        console.log('Check for footprint route: ', this.footprintRoute);

        var filterDataString: string;

        // check for footprint filter
        if (this.footprintRoute) {
            // make a copy of current filter dictionary
            var tempFilterData: Record<string, any> = {};
            for (let key in this.filterData) {
                tempFilterData[key] = this.filterData[key];
            }
            // add footprint filter
            for (let key in this.footprintFilterData) {
                // if footprint already exists from search component, add footprint value from bar or scatter plot selection
                if (key == 'footprint' && 'footprint' in tempFilterData) {
                    for (
                        let i = 0;
                        i < this.footprintFilterData[key].length;
                        i++
                    ) {
                        tempFilterData[key].push(
                            this.footprintFilterData[key][i]
                        );
                    }
                } else {
                    tempFilterData[key] = this.footprintFilterData[key];
                }
            }

            if (!localStorage) {
                filter['filter'] = tempFilterData;
                filterDataString = JSON.stringify(filter);
            } else {
                filterDataString = JSON.stringify(tempFilterData);
            }
        } else {
            this.clearFootprintFilter();
            if (!localStorage) {
                filter['filter'] = this.filterData;
                filterDataString = JSON.stringify(filter);
            } else {
                filterDataString = JSON.stringify(this.filterData);
            }
        }

        console.log(
            'filterData: localStorage=',
            localStorage,
            filterDataString
        );

        return filterDataString;
    }

    removeFilterItem(key: string) {
        delete this.filterData[key];
    }

    clearFilter() {
        this.filterData = {};
    }

    addFootprintFilterItem(key: string, value: any) {
        this.footprintFilterData[key] = value;
    }

    getFootprintFilter() {
        return this.footprintFilterData;
    }

    getFootprintFilterItem(key: string) {
        return this.footprintFilterData[key];
    }

    removeFootprintFilterItem(key: string) {
        delete this.footprintFilterData[key];
    }

    clearFootprintFilter() {
        this.footprintFilterData = {};
    }

    getUserRole() {
        const getStrongestUserRole = () => {
            // use strongest user role as default
            if (this.getAdminStatus() == true) return 'admin';
            else if (this.getPIStatus() == true) return 'pi';
            return 'user';
        };
        if (localStorage.getItem('user_role') == undefined) {
            return getStrongestUserRole();
        } else {
            // use role from localStorage
            if (
                localStorage.getItem('user_role') == 'admin' &&
                this.getAdminStatus() == true
            )
                return 'admin';
            else if (
                localStorage.getItem('user_role') == 'pi' &&
                this.getPIStatus() == true
            )
                return 'pi';
            else if (localStorage.getItem('user_role') == 'user') return 'user';
            return getStrongestUserRole();
        }
    }

    getUser() {
        return this.appConfig.getUserFromBackend();
    }

    getPIStatus() {
        return this.appConfig.getPIStatus();
    }

    getAdminStatus() {
        return this.appConfig.getAdminStatus();
    }

    getGlobalRangeDates() {
        //console.log("Get Date ", this.global_range_dates);
        return this.global_range_dates;
    }

    setGlobalRangeDates(range_dates: any) {
        //console.log("Set Date ", range_dates);
        this.global_range_dates = range_dates;
        this.global_start = Date.parse(range_dates[0]) / 1000;
        this.global_end = Date.parse(range_dates[1]) / 1000;
    }

    setActiveComponent(componentName: string) {
        this.activeComponent = componentName;
    }

    reloadData() {
        this.Reload.emit(this.activeComponent);
    }

    propagateTimelineAction(data: any) {
        this.TimelinePropagation.emit(data);
    }

    jobDataTableHeaderResize() {
        this.JobDataTableHeaderResized.emit();
    }

    getProjectData() {
        return this.http.post(
            this.getBackend() + '/jobtable/project',
            this.getFilterClause(),
            {
                headers: this.getHeaders(),
            }
        );
    }

    getUserData(project: string, pend: boolean = false) {
        let filter = JSON.parse(this.getFilterClause());

        if (project) filter['filter']['project_name'] = project;

        if (pend) {
            return this.http.post(
                this.getBackend() + '/jobtable_pend/user',
                filter,
                {
                    headers: this.getHeaders(),
                }
            );
        } else {
            return this.http.post(
                this.getBackend() + '/jobtable/user',
                filter,
                {
                    headers: this.getHeaders(),
                }
            );
        }
    }

    getJobData(user: string, project: string, pend: boolean = false) {
        let filter = JSON.parse(this.getFilterClause());

        if (user) filter['filter']['user_name'] = user;

        if (project) filter['filter']['project_name'] = project;

        if (pend) {
            return this.http.post(
                this.getBackend() + '/jobtable_pend/job',
                filter,
                {
                    headers: this.getHeaders(),
                }
            );
        } else {
            return this.http.post(this.getBackend() + '/jobtable/job', filter, {
                headers: this.getHeaders(),
            });
        }
    }

    getJobDetailData(
        user: string,
        job: string,
        project: string,
        pend: boolean = false
    ) {
        let filter = JSON.parse(this.getFilterClause());

        if (job) filter['filter']['job_name'] = job;

        if (user) filter['filter']['user_name'] = user;

        if (project) filter['filter']['project_name'] = project;

        if (pend) {
            filter['filter']['reservation_data'] = true;
            return this.http.post(
                this.getBackend() + '/jobtable_pend/jobid',
                filter,
                { headers: this.getHeaders() }
            );
        } else {
            return this.http.post(
                this.getBackend() + '/jobtable/jobid',
                filter,
                {
                    headers: this.getHeaders(),
                }
            );
        }
    }

    /* not used anymore */
    getFootprintData(job_id: string, start: string) {
        const params = new HttpParams()
            .set('job_id', job_id)
            .set('start', start);
        return this.http.post(
            this.appConfig.getConfig().AppConfig.footprint_url +
                'Footprint.php',
            params
        );
    }

    getFilterData(footprint: string) {
        return this.http.post(
            this.getBackend() + '/jobtable/jobid',
            this.getFilterClause(),
            { headers: this.getHeaders() }
        );
    }

    getUniqueJobData(jobid: string, jobstart: string, partition: string) {
        var filter: any = {};
        filter['format_result'] = {
            utc_timezone: this.getUTCTimezone(),
            time_format: this.getDateFormat(),
        };

        return this.http.post(
            this.getBackend() + `/job/${jobid}/${jobstart}/${partition}`,
            filter,
            { headers: this.getHeaders() }
        );
    }

    getChartData(
        footprint1: string,
        footprint2: string,
        class_num: number,
        x_start: number | undefined,
        x_end: number | undefined,
        y_start: number | undefined,
        y_end: number | undefined
    ) {
        let filter = JSON.parse(this.getFilterClause());

        filter['filter']['binning'] = class_num;

        let footprints = [];
        if (footprint1 != 'undefined') {
            footprints.push({ name: footprint1, min: x_start, max: x_end });
        }
        if (footprint2 != 'undefined') {
            footprints.push({ name: footprint2, min: y_start, max: y_end });
        }

        filter['filter']['visualization'] = footprints;

        console.log('footprint filter', filter);
        return this.http.post(this.getBackend() + '/footprint', filter, {
            headers: this.getHeaders(),
        });
    }

    /* not used anymore */
    getValidationData() {
        const params = new HttpParams().set('filter', this.getFilterClause());
        return this.http.post(this.getBackend() + 'Validation.php', params);
    }

    getIssueData() {
        let filter = JSON.parse(this.getFilterClause());
        return this.http.post(this.getBackend() + '/issuetable/user', filter, {
            headers: this.getHeaders(),
        });
    }

    getNodeData(sample_time: number | undefined = undefined) {
        var current_time = Math.floor(Date.now() / 1000);
        // use previous sample if current sample is less than 30s old
        if (sample_time && current_time - sample_time < 30)
            sample_time = sample_time - 300;
        let filter = JSON.parse(this.getFilterClause());
        if (sample_time) filter['filter']['sample_time'] = sample_time;
        filter['filter']['reservation_data'] = true;
        return this.http.post(this.getBackend() + '/node', filter, {
            headers: this.getHeaders(),
        });
    }

    getIssueJobData(user: string) {
        let filter = JSON.parse(this.getFilterClause());
        filter['filter']['user_name'] = user;
        return this.http.post(this.getBackend() + '/issuetable/job', filter, {
            headers: this.getHeaders(),
        });
    }

    getIssueJobDetailData(user: string, job: string) {
        let filter = JSON.parse(this.getFilterClause());
        filter['filter']['user_name'] = user;
        filter['filter']['job_name'] = job;
        console.log(filter);
        return this.http.post(this.getBackend() + '/issuetable/jobid', filter, {
            headers: this.getHeaders(),
        });
    }

    getStatisticData(statistic: string) {
        // limit time interval for user and project dropbox to one week
        let filter = JSON.parse(this.getFilterClause());
        if (statistic == 'project' || statistic == 'user') {
            let current_time = Math.floor(Date.now() / 1000);
            filter['filter']['start_time'] = current_time - 604800;
            filter['filter']['end_time'] = current_time;
        }
        return this.http.post(
            this.getBackend() + `/statistic/${statistic}`,
            filter,
            { headers: this.getHeaders() }
        );
    }

    getTimelineData(
        start: number,
        end: number,
        metric: string,
        value_type: string,
        job_data: any,
        mean_line: boolean
    ) {
        var job_data_dict = job_data[0];
        var route = '/timeline/';
        if (metric == 'node_power') route = '/timeline_extern/';

        return this.http.post(
            this.getBackend() +
                route +
                metric +
                '/' +
                job_data_dict['job_id'] +
                '/' +
                job_data_dict['start_unixtime'] +
                '/' +
                job_data_dict['partition_name'],
            {
                start: start,
                end: end,
                timeline_type: value_type,
                mean_line: mean_line,
            },
            { headers: this.getHeaders() }
        );
    }

    getTimelineHistogramData(start: number, metric: string, job_data: any) {
        var job_data_dict = job_data[0];
        return this.http.post(
            this.getBackend() +
                '/timeline/' +
                metric +
                '/' +
                job_data_dict['job_id'] +
                '/' +
                job_data_dict['start_unixtime'] +
                '/' +
                job_data_dict['partition_name'],
            { start: start, end: start, timeline_type: 'histogram' },
            { headers: this.getHeaders() }
        );
    }

    setLastURL(url: string) {
        this.lastURL = url;
    }

    getLastURL() {
        return this.lastURL;
    }

    getPartitionList() {
        return Object.keys(this.appConfig.getPartitionData());
    }

    shortenLargeNumber(num: any, digits: number, space = '') {
        var units = ['k', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y'],
            decimal: any;
        for (var i = units.length - 1; i >= 0; i--) {
            decimal = Math.pow(1000, i + 1);
            if (num <= -decimal || num >= decimal) {
                return +(num / decimal).toFixed(digits) + space + units[i];
            }
        }
        return num + space;
    }

    revertLargeNumber(formatted_number: string): number {
        if (!formatted_number || typeof formatted_number !== 'string') {
            return 0;
        }
        const units: { [key: string]: number } = {
            k: 1e3,
            M: 1e6,
            G: 1e9,
            T: 1e12,
            P: 1e15,
            E: 1e18,
            Z: 1e21,
            Y: 1e24,
        };

        // Remove any spaces and split the number from the unit
        const match = formatted_number.match(/^([\d.]+)([kMGTPEZY])?$/);

        if (!match) {
            return 0;
        }

        const number = parseFloat(match[1]);
        const unit = match[2];

        // If there is a unit, multiply by the corresponding factor, otherwise return the number as is
        return number * (unit ? units[unit] : 1);
    }

    getPartitions() {
        return Object.keys(this.appConfig.getConfig().PartitionConfig);
    }

    getActivePartitions() {
        var partitions = this.appConfig.getPartitionData();
        var active_partitions: string[] = [];
        for (const partition in partitions) {
            if (partitions[partition]['active'] == 1)
                active_partitions.push(partition);
        }
        return active_partitions;
    }

    getDisplays(partition: string, start_unixtime: number = 0) {
        var displays =
            this.appConfig.getConfig().PartitionConfig[partition]['displays'];
        if (displays.length > 1) {
            if (displays[0] == 'DefaultTimelinesTop') {
                var displays_top: string[] =
                    this.appConfig.getConfig().DefaultTimelinesTop;
                // combine default displays and additional displays
                displays = displays_top.concat(displays);
                // remove "DefaultTimelinesTop" from displays
                var j = displays_top.length;
                displays.splice(j, 1);
            }
        } else {
            displays = this.appConfig.getConfig().DefaultTimelinesTop;
        }

        // replace ipc with cpi before July 17, 2019 at 2pm
        if (start_unixtime < 1563364800) {
            displays = displays.map((item: string) =>
                item === 'ipc' ? 'cpi' : item
            );
        }
        // remove node power before January 1, 2024 at 2am
        if (displays.includes('node_power') && start_unixtime < 1704074400)
            displays = displays.filter((item: string) => item !== 'node_power');

        // replace job_mem_used with mem_used before January 17, 2025
        if (displays.includes('job_mem_used') && start_unixtime < 1737072000)
            displays = displays.map((item: string) =>
                item === 'job_mem_used' ? 'mem_used' : item
            );
        // removed ethernet_bw before January 17, 2025
        if (displays.includes('ethernet_bw') && start_unixtime < 1737072000)
            displays = displays.filter(
                (item: string) => item !== 'ethernet_bw'
            );
        return displays;
    }

    getFileSystemInstanceNames() {
        return this.appConfig.getConfig().FileSystemInstanceNames;
    }

    getIOinfos(partition: string) {
        var io_infos: Record<string, any> = {};
        io_infos['io'] =
            this.appConfig.getConfig().PartitionConfig[partition]['io'];
        io_infos['io_meta'] =
            this.appConfig.getConfig().PartitionConfig[partition]['io_meta'];
        return io_infos;
    }

    getJobtags(tag: string) {
        var tag_number = Object.keys(this.appConfig.getJobTags()).length - 1;
        var value = tag.replace(':', '');
        if (value == '0') {
            return this.appConfig.getJobTags()['0'];
        } else {
            var bound_tags: string[] = [];
            var heavy_tags: string[] = [];
            var misc_tags: string[] = [];
            var bound_string: string = '';
            var heavy_string: string = '';
            var misc_string: string = '';
            var all_tags: string = '';

            for (let i = 0; i < tag_number; i++) {
                var tag_option = Math.pow(2, i);
                if (Number(value) & tag_option) {
                    var tag_name: string =
                        this.appConfig.getJobTags()[tag_option];
                    if (tag_name.includes('bound'))
                        bound_tags.push(tag_name.replace('-bound', ''));
                    else if (tag_name.includes('heavy'))
                        heavy_tags.push(tag_name.replace('-heavy', ''));
                    else misc_tags.push(tag_name);
                }
            }

            /* determine bound string */
            if (bound_tags.length > 0) {
                bound_string = bound_tags.join('-').concat('-bound');
            }
            /* determine heavy string */
            if (heavy_tags.length > 0) {
                heavy_string = heavy_tags.join('-').concat('-heavy');
            }
            /* determine misc string */
            if (misc_tags.length > 0) {
                misc_string = misc_tags.join(' and <br>');
            }

            /* combine all tags */
            if (bound_string.length > 0)
                all_tags = all_tags.concat(bound_string);

            if (heavy_string.length > 0) {
                if (all_tags.length > 0)
                    all_tags = all_tags.concat(' and <br>', heavy_string);
                else all_tags = all_tags.concat(heavy_string);
            }

            if (misc_string.length > 0) {
                if (all_tags.length > 0)
                    all_tags = all_tags.concat(' and <br>', misc_string);
                else all_tags = all_tags.concat(misc_string);
            }

            return all_tags;
        }
    }

    getTagLabel(option: string) {
        var tag_number = Object.keys(this.appConfig.getJobTags()).length - 1;
        var bound_tags: string[] = [];
        var bound_value: string[] = [];
        var heavy_tags: string[] = [];
        var heavy_value: string[] = [];
        var misc_tags: string[] = [];
        var misc_value: string[] = [];
        var tag_label: string[] = [];
        var tag_value: string[] = [];
        for (let i = 0; i < tag_number; i++) {
            var tag_option = Math.pow(2, i);
            var tag_name: string = this.appConfig.getJobTags()[tag_option];
            if (tag_name.includes('bound')) {
                bound_tags.push(tag_name);
                bound_value.push(tag_option.toString());
            } else if (tag_name.includes('heavy')) {
                heavy_tags.push(tag_name);
                heavy_value.push(tag_option.toString());
            } else {
                misc_tags.push(tag_name);
                misc_value.push(tag_option.toString());
            }
        }
        tag_label = bound_tags.concat(heavy_tags);
        tag_value = bound_value.concat(heavy_value);
        if (misc_tags.length > 0) {
            tag_label = tag_label.concat(misc_tags);
            tag_value = tag_value.concat(misc_value);
        }
        if (option == 'label') {
            return tag_label;
        } else if (option == 'value') {
            return tag_value;
        }
        return null;
    }

    getTagNum() {
        return Object.keys(this.appConfig.getJobTags()).length - 1;
    }

    getTimelineMenu() {
        return this.appConfig.getConfig().TimelineMenu;
    }

    getTimelineCaptions(metric: string) {
        // if metric does not have a definition, it is an io metric
        if (!this.appConfig.getConfig().TimelineCaptions[metric]) {
            return this.io_timeline_captions[metric];
        }
        return this.appConfig.getConfig().TimelineCaptions[metric];
    }

    getFileSystems(io_category: string = 'io') {
        return this.appConfig.getConfig().FileSystems[io_category];
    }

    getTableDefinition(option: string) {
        if (option == 'Definition') {
            var arr = this.appConfig.getConfig().Table_Definition;
            var keys = Object.keys(this.appConfig.getConfig().Table_Definition);
            var col_define = new Map();
            for (let key of keys) {
                col_define.set(key, arr[key]);
            }
            return col_define;
        } else {
            return this.appConfig.getConfig()[option];
        }
    }

    getPartitionSpec(name: string) {
        var partition_spec = this.appConfig.getPartitionData();
        if (name in partition_spec) return partition_spec[name];
        else return partition_spec['default'];
    }

    formatOverlayText(text: string) {
        if (text) {
            text = text.replace(/],/gi, '], ');
            text = text.replace(/,/gi, ', ');
        }
        return text;
    }

    getCurrentYear() {
        return new Date().getFullYear();
    }

    download(
        jobid: string,
        job_start: string,
        partition: string,
        download_data: string[]
    ) {
        let filter: Record<string, any> = {};
        download_data.forEach((elem) => {
            switch (elem) {
                case 'data':
                    filter['performance_data'] = true;
                    break;
                case 'meta':
                    filter['job_metadata'] = true;
                    break;
                case 'cluster':
                    filter['cluster_data'] = true;
                    break;
            }
        });

        return this.http.post(
            this.getBackend() + `/download/${jobid}/${job_start}/${partition}`,
            filter,
            { headers: this.getHeaders(), responseType: 'blob' }
        );
    }

    setSortField(value: string) {
        // set sortField according to issue value
        switch (value) {
            case 'cpu':
                this.sort_field = 'core_duration_idle';
                break;
            case 'gpu':
                this.sort_field = 'gpu_duration_idle';
                break;
            case 'other':
                this.sort_field = 'max_io_blocking';
                break;
            default:
                this.sort_field = 'core_duration_idle';
                break;
        }
    }

    getSortField() {
        return this.sort_field;
    }

    setIssueValue(value: string) {
        this.issue_value = value;
    }

    getIssueValue() {
        return this.issue_value;
    }

    showExpiredSessionDialog(e: any) {
        console.log('Error', e.status, e.error);
        // e.status = 0 --> Shibboleth session might be expired
        // e.status = 401 --> token at pika-server expired
        if (
            e.status === 0 ||
            (e.status === 401 && e.error && e.error.detail === 'Token expired')
        )
            return true;
        return false;
    }

    setLiveMode(value: string) {
        this.live_value = value;
    }

    getLiveMode() {
        return this.live_value;
    }

    beautify(item: any, field: string) {
        if (
            this.appConfig.getConfig().Table_Definition[field] &&
            this.appConfig.getConfig().Table_Definition[field]['beautify'] ===
                true
        ) {
            if (item === null || item === undefined) return item;
            const space = '\u202F';
            item = item
                .replace('y', `${space}y`)
                .replace('d', `${space}d`)
                .replace('h', `${space}h`)
                .replace('min', `${space}min`)
                .replace('s', `${space}s`);
        }
        return item;
    }
}
