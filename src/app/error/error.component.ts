import { Component } from '@angular/core';
import { MonitoringService } from '../service/monitoring.service';

@Component({
    selector: 'app-error',
    templateUrl: './error.component.html',
    styleUrls: ['./error.component.css'],
})
export class ErrorComponent {
    show_server_connection_text: boolean = true;
    show_version_compatible_text: boolean = false;
    constructor(protected monitoringservice: MonitoringService) {
        if (!this.monitoringservice.clientServerCompatible()) {
            this.show_server_connection_text = false;
            this.show_version_compatible_text = true;
        }
    }

    reload() {
        console.log('reload');
        location.reload();
    }
}
