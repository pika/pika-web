import { Component } from '@angular/core';
import { MonitoringService } from '../service/monitoring.service';
import { ActivatedRoute, Router } from '@angular/router';
import { JobBaseComponent } from './job-base.component';
import * as clone from 'lodash';

@Component({
    selector: 'app-issue-job-detail',
    templateUrl: './job-base.component.html',
    styleUrls: ['./job-base.component.css'],
})
export class IssueJobDetailComponent extends JobBaseComponent {
    stateKey: string = 'issue-job-detail';
    col_defines_deep_copy: any; // needed to change column headers
    constructor(
        monitoringservice: MonitoringService,
        route: ActivatedRoute,
        router: Router
    ) {
        super(monitoringservice, route, router);
    }

    override componentInit() {
        this.stateKey = 'issue-job-detail';
        this.monitoringservice.setActiveComponent(this.stateKey);
        this.monitoringservice.Reload.subscribe((data) => {
            if (this.stateKey == data) {
                console.log('Reload issue data');
                this.reloadTable();
            }
        });
        this.show_issue_selection = true;
    }

    override setTableColums() {
        this.resizableColumns = false;
        this.cols = [];
        this.col_defines_deep_copy = clone.cloneDeep(this.col_defines);
        var issue_value = this.monitoringservice.getIssueValue();

        /* get Issue table */
        var issue_table = this.monitoringservice.getTableDefinition(
            'IssueJobDetail_Table'
        );

        for (let index of issue_table) {
            let col_def = this.col_defines_deep_copy.get(index);

            if (
                (index == 'job_id_link' || index == 'project_name') &&
                issue_value != 'all'
            )
                this.cols.push(col_def);
            if (issue_value == 'cpu' && index.includes('core')) {
                if ('header_long' in col_def)
                    col_def['header'] = col_def['header_long'];
                this.cols.push(col_def);
            } else if (
                issue_value == 'gpu' &&
                (index.includes('gpu') || index.includes('offloading'))
            ) {
                if ('header_long' in col_def)
                    col_def['header'] = col_def['header_long'];
                this.cols.push(col_def);
            } else if (
                issue_value == 'other' &&
                (index.includes('io_') ||
                    index.includes('mem_leak') ||
                    index.includes('unused_mem'))
            ) {
                if ('header_long' in col_def)
                    col_def['header'] = col_def['header_long'];
                this.cols.push(col_def);
            } else if (issue_value == 'all') {
                this.cols.push(col_def);
            }
        }
        this.caption_total_name = 'Issue Runs';
        this.footprint_route = '';
    }

    override getTableData() {
        if (sessionStorage.getItem(this.sessionStorageName)) {
            console.log(
                'Session found for ',
                this.stateKey,
                this.sessionStorageName
            );
            this.rows = JSON.parse(
                sessionStorage.getItem(this.sessionStorageName)!
            );
            this.loading = false;
        } else {
            this.monitoringservice
                .getIssueJobDetailData(this.user, this.job)
                .subscribe({
                    next: (data) => {
                        this.rows = data;
                        try {
                            sessionStorage.setItem(
                                this.sessionStorageName,
                                JSON.stringify(data)
                            );
                        } catch {
                            console.log(
                                'SessionStorage failed due to quota. Reset all stored sessions.'
                            );
                            sessionStorage.clear();
                        }
                        this.loading = false;
                    },
                    error: (e) => {
                        this.loading = false;
                        this.show_token_expired_dialog =
                            this.monitoringservice.showExpiredSessionDialog(e);
                    },
                });
        }
    }
}
