import { Component, OnInit } from '@angular/core';
import { MonitoringService } from '../service/monitoring.service';
import { ActivatedRoute, Router } from '@angular/router';
import { JobBaseComponent } from './job-base.component';

@Component({
    selector: 'app-filter',
    templateUrl: './job-base.component.html',
    styleUrls: ['./job-base.component.css'],
})
export class FilterComponent extends JobBaseComponent {
    footprint: string = ''; //column
    stateKey: string = 'filter';

    constructor(
        monitoringservice: MonitoringService,
        route: ActivatedRoute,
        router: Router
    ) {
        super(monitoringservice, route, router);
    }

    override componentInit() {
        this.stateKey = 'filter';
        this.monitoringservice.setActiveComponent(this.stateKey);
    }

    override setTableColums() {
        this.cols = [];
        this.cols.push(this.col_defines.get('job_id_link'));
        if (this.project == '')
            this.cols.push(this.col_defines.get('project_name'));
        if (this.user == '') this.cols.push(this.col_defines.get('user_name'));
        if (this.job == '') this.cols.push(this.col_defines.get('job_name'));
        /* get Filter table */
        var filter_table =
            this.monitoringservice.getTableDefinition('Filter_Table');
        for (let index of filter_table) {
            this.cols.push(this.col_defines.get(index));
        }

        this.getAllFilterItems();
    }

    getAllFilterItems() {
        if (this.monitoringservice.getFootprintFilterItem('footprint')) {
            if (
                this.monitoringservice.getFootprintFilterItem('footprint')
                    .length > 0
            ) {
                this.footprint =
                    this.monitoringservice.getFootprintFilterItem(
                        'footprint'
                    )[0]['name'];
                this.cols.push({
                    field: this.footprint,
                    header: this.footprint.replace(/_/g, ' '),
                });
            }
        } else if (this.monitoringservice.getFilterItem('footprint')) {
            if (this.monitoringservice.getFilterItem('footprint').length > 0) {
                this.footprint =
                    this.monitoringservice.getFilterItem('footprint')[0][
                        'name'
                    ];
                this.cols.push({
                    field: this.footprint,
                    header: this.footprint.replace(/_/g, ' '),
                });
            }
        }
    }

    override getTableData() {
        if (sessionStorage.getItem(this.sessionStorageName)) {
            console.log(
                'Session found for ',
                this.stateKey,
                this.sessionStorageName
            );
            this.rows = JSON.parse(
                sessionStorage.getItem(this.sessionStorageName)!
            );
            this.loading = false;
        } else {
            this.monitoringservice.getFilterData(this.footprint).subscribe({
                next: (data) => {
                    this.rows = data;
                    try {
                        sessionStorage.setItem(
                            this.sessionStorageName,
                            JSON.stringify(data)
                        );
                    } catch {
                        console.log(
                            'SessionStorage failed due to quota. Reset all stored sessions.'
                        );
                        sessionStorage.clear();
                    }
                    this.loading = false;
                },
                error: (e) => {
                    console.error(e);
                    this.loading = false;
                },
                complete: () => console.info('complete'),
            });
        }
    }
}
