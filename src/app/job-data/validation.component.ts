import { Component } from '@angular/core';
import { MonitoringService } from '../service/monitoring.service';
import { ActivatedRoute, Router } from '@angular/router';
import { JobBaseComponent } from './job-base.component';

@Component({
    selector: 'app-user',
    templateUrl: './job-base.component.html',
    styleUrls: ['./job-base.component.css'],
})
export class ValidationComponent extends JobBaseComponent {
    stateKey: string = 'validation';
    constructor(
        monitoringservice: MonitoringService,
        route: ActivatedRoute,
        router: Router
    ) {
        super(monitoringservice, route, router);
    }

    override componentInit() {
        this.stateKey = 'validation';
        this.monitoringservice.setActiveComponent(this.stateKey);
        this.monitoringservice.Reload.subscribe((data) => {
            if (this.stateKey == data) {
                console.log('Reload validation data');
                this.reloadTable();
            }
        });
    }

    override setTableColums() {
        this.cols = [];
        this.cols.push({
            field: 'job_id',
            header: 'Job ID',
            link: '1',
            tooltip: 'job_id',
            string_cut: '1',
        });
        this.cols.push({ field: 'BENCHMARK', header: 'Benchmark' });
        this.cols.push({ field: 'start_time', header: 'Date' });
        this.cols.push({
            field: 'partition_name',
            header: 'Partition',
            tooltip: 'partition_name',
            string_cut: '1',
        });
        this.cols.push({
            field: 'METRIC',
            header: 'Metric',
            tooltip: 'METRIC',
            string_cut: '1',
        });
        this.cols.push({ field: 'B_VALUE', header: 'Benchmark Value' });
        this.cols.push({ field: 'PIKA_VALUE', header: 'Measured Value' });
        this.cols.push({ field: 'DEVIATION_VALUE', header: 'Value Deviation' });
        this.cols.push({
            field: 'B_job_duration',
            header: 'Benchmark Duration',
        });
        this.cols.push({
            field: 'PIKA_job_duration',
            header: 'Measured Duration',
        });
        this.cols.push({
            field: 'DEVIATION_job_duration',
            header: 'Duration Deviation',
        });

        this.caption_total_name = 'Validations';
        this.footprint_route = 'project_name';
    }

    override getFootprintData(job_id: string, start: string) {
        this.loading = true;
        this.monitoringservice.getFootprintData(job_id, start).subscribe({
            next: (data: any) => {
                console.log('Footprints: ', data);
                if (data.toString().length > 0) this.loading = false;
                this.reloadTable();
            },
            error: (e) => {
                console.error('Error', e);
                this.loading = false;
            },
        });
    }

    override getTableData() {
        if (sessionStorage.getItem(this.sessionStorageName)) {
            console.log(
                'Session found for ',
                this.stateKey,
                this.sessionStorageName
            );
            this.rows = JSON.parse(
                sessionStorage.getItem(this.sessionStorageName)!
            );
            this.loading = false;
        } else {
            this.monitoringservice.getValidationData().subscribe({
                next: (data) => {
                    this.rows = data;
                    try {
                        sessionStorage.setItem(
                            this.sessionStorageName,
                            JSON.stringify(data)
                        );
                    } catch {
                        console.log(
                            'SessionStorage failed due to quota. Reset all stored sessions.'
                        );
                        sessionStorage.clear();
                    }
                    this.loading = false;
                },
                error: (e) => {
                    console.error('Error', e);
                    this.loading = false;
                },
            });
        }
    }
}
