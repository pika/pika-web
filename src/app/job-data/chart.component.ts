import { Component } from '@angular/core';
import { MonitoringService } from '../service/monitoring.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { JobBaseComponent } from './job-base.component';
import { FootprintChart } from './footprint';

@Component({
    selector: 'app-chart',
    templateUrl: './job-base.component.html',
    styleUrls: ['./job-base.component.css'],
})
export class ChartComponent extends JobBaseComponent {
    override displays: FootprintChart[];
    override recorded_footprints: number;
    override recorded_jobs: number;
    default_charts_added: boolean;
    stateKey: string = 'charts';

    constructor(
        monitoringservice: MonitoringService,
        route: ActivatedRoute,
        router: Router
    ) {
        super(monitoringservice, route, router);

        this.displays = [];
        this.recorded_footprints = 0;
        this.recorded_jobs = 0;
        this.user = this.monitoringservice.getUser();
        this.showChartButton = true;
        this.showCharts = true;
        this.default_charts_added = false;
    }

    override componentInit() {
        this.stateKey = 'charts';
        this.monitoringservice.setActiveComponent(this.stateKey);
        this.monitoringservice.Reload.subscribe((data) => {
            if (this.stateKey == data) {
                console.log('Reload charts');
                this.reloadTable();
            }
        });
    }

    override getChartData() {
        this.loading = false;
        this.caption_total_name = 'Footprints';
        this.paginator = false;

        // create filter for all chart queries
        this.monitoringservice.removeFootprintFilterItem('project_name');
        this.monitoringservice.removeFootprintFilterItem('user_name');
        this.monitoringservice.removeFootprintFilterItem('job_name');

        if (this.project != '')
            this.monitoringservice.addFootprintFilterItem(
                'project_name',
                this.project
            );
        if (this.user != '')
            this.monitoringservice.addFootprintFilterItem(
                'user_name',
                this.user
            );

        if (this.job != '')
            this.monitoringservice.addFootprintFilterItem('job_name', this.job);

        // get number of recorded footprints
        this.recorded_footprints = 0;
        this.recorded_jobs = 0;
        this.monitoringservice
            .getStatisticData('footprint_job_count')
            .subscribe({
                next: (data: any) => {
                    this.recorded_footprints = data[0]['footprint_count'];
                    this.recorded_jobs = data[0]['job_count'];
                },
                error: (e) => console.error(e),
                complete: () => console.info('complete'),
            });

        if (this.default_charts_added == false) {
            var num = 2;

            // remember last session
            if (localStorage.getItem('footprint-charts-number'))
                num = JSON.parse(
                    localStorage.getItem('footprint-charts-number')!
                );

            for (let i = 0; i < num; i++) {
                this.addChart('undefined', 'undefined', 0);
            }

            this.default_charts_added = true;
        } else {
            for (var display of this.displays) display.redraw(true);
        }
        setTimeout(() => {
            this.monitoringservice.jobDataTableHeaderResize();
        });
    }

    override addChart(
        footprint1: string,
        footprint2: string,
        class_num: number
    ) {
        var num = this.displays.length;
        var chart = new FootprintChart(
            num,
            this.monitoringservice,
            this.route,
            this.router,
            footprint1,
            footprint2,
            class_num
        );
        this.displays.push(chart);
        localStorage.setItem(
            'footprint-charts-number',
            JSON.stringify(this.displays.length)
        );
    }

    override resetCharts() {
        localStorage.removeItem('footprint-charts-number');

        //remove all display settings
        var arr = []; // Array to hold the keys
        // Iterate over localStorage and insert the keys that meet the condition into arr
        for (var i = 0; i < localStorage.length; i++) {
            //console.log(" ALL localStorage: ", localStorage.key(i));
            if (localStorage.key(i)!.substring(0, 16) == 'footprint-chart-') {
                //console.log("localStorage: ", localStorage.key(i));
                arr.push(localStorage.key(i));
            }
        }
        // Iterate over arr and remove the items by key
        for (var i = 0; i < arr.length; i++) {
            localStorage.removeItem(arr[i]!);
        }

        //remove footprint chart sessions
        arr = []; // Array to hold the keys
        // Iterate over sessionStorage and insert the keys that meet the condition into arr
        for (var i = 0; i < sessionStorage.length; i++) {
            //console.log(" ALL sessionStorage: ", sessionStorage.key(i));
            if (
                sessionStorage.key(i)!.substring(0, 23) ==
                'footprint-chart-values_'
            ) {
                //console.log("sessionStorage: ", sessionStorage.key(i));
                arr.push(sessionStorage.key(i));
            }
        }
        // Iterate over arr and remove the items by key
        for (var i = 0; i < arr.length; i++) {
            sessionStorage.removeItem(arr[i]!);
        }

        this.default_charts_added = false;
        this.displays = [];
        this.getChartData();
    }
}
