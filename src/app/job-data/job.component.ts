import { Component } from '@angular/core';
import { MonitoringService } from '../service/monitoring.service';
import { ActivatedRoute, Router } from '@angular/router';
import { JobBaseComponent } from './job-base.component';

@Component({
    selector: 'app-job',
    templateUrl: './job-base.component.html',
    styleUrls: ['./job-base.component.css'],
})
export class JobComponent extends JobBaseComponent {
    stateKey: string = 'job';
    table_definition = 'Job_Table';
    constructor(
        monitoringservice: MonitoringService,
        route: ActivatedRoute,
        router: Router
    ) {
        super(monitoringservice, route, router);
    }

    override componentInit() {
        if (this.monitoringservice.getFilterItem('live') == 'true') {
            // we only show live mode buttons, if the route starts with the Live tab
            if (
                this.live_tab_start &&
                this.monitoringservice.enablePendingJobs()
            )
                this.show_live_modes = true;

            if (this.monitoringservice.getLiveMode() == 'pending') {
                this.stateKey = 'job-pending';
                this.caption_total_name = 'Jobs';
                this.table_definition = 'Job_Pend_Table';
            } else {
                this.stateKey = 'job-running';
                this.caption_total_name = 'Jobs';
                this.table_definition = 'Job_Live_Table';
            }
        } else {
            this.show_live_modes = false;
            this.stateKey = 'job';
            this.caption_total_name = 'Jobs';
            this.table_definition = 'Job_Table';
            this.footprint_route = 'job_name';
        }

        // used for reloading job table after changing date in menubar
        this.monitoringservice.setActiveComponent(this.stateKey);
        this.monitoringservice.Reload.subscribe((data) => {
            if (this.stateKey == data) {
                console.log('Reload ', this.stateKey);
                this.reloadTable();
            }
        });
    }

    override setTableColums() {
        this.cols = [];

        this.cols.push(this.col_defines.get('job_name_link'));
        if (this.project == '')
            this.cols.push(this.col_defines.get('project_name'));
        if (this.user == '') this.cols.push(this.col_defines.get('user_name'));

        /* get table columns from appConfig.json */
        var user_table = this.monitoringservice.getTableDefinition(
            this.table_definition
        );
        for (let index of user_table) {
            this.cols.push(this.col_defines.get(index));
        }
    }

    override getTableData() {
        if (
            sessionStorage.getItem(this.sessionStorageName) &&
            !this.monitoringservice.getFilterItem('live')
        ) {
            console.log(
                'Session found for ',
                this.stateKey,
                this.sessionStorageName
            );
            this.rows = JSON.parse(
                sessionStorage.getItem(this.sessionStorageName)!
            );
            this.loading = false;
        } else {
            var pend = false;
            if (
                this.monitoringservice.getFilterItem('live') == 'true' &&
                this.monitoringservice.getLiveMode() == 'pending'
            )
                pend = true;
            this.monitoringservice
                .getJobData(this.user, this.project, pend)
                .subscribe({
                    next: (data) => {
                        this.rows = data;
                        try {
                            sessionStorage.setItem(
                                this.sessionStorageName,
                                JSON.stringify(data)
                            );
                        } catch {
                            console.log(
                                'SessionStorage failed due to quota. Reset all stored sessions.'
                            );
                            sessionStorage.clear();
                        }
                        this.loading = false;
                    },
                    error: (e) => {
                        this.loading = false;
                        this.show_token_expired_dialog =
                            this.monitoringservice.showExpiredSessionDialog(e);
                    },
                });
        }
    }
}
