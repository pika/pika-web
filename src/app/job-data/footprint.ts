import { MonitoringService } from '../service/monitoring.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { SelectItem } from 'primeng/api';

const scatterplot_captions: Record<string, string> = {
    node_count: 'Number of Nodes',
    core_count: 'Number of Cores',
    time_limit: 'Walltime',
    duration: 'Duration',
    core_duration: 'Core Duration',
    pending_time: 'Pending Time',
    ipc_mean_per_core: 'IPC',
    cpu_used_mean_per_core: 'CPU Usage',
    flops_any_mean_per_core: 'FLOPS',
    mem_bw_mean_per_socket: 'Memory Bandwidth',
    cpu_power_mean_per_socket: 'CPU Power',
    energy_in_kwh: 'Job Energy',
    energy_efficiency_in_gflops_per_watt: 'Job Energy Efficiency',
    ib_bw_mean_per_node: 'Infiniband Bandwidth',
    host_mem_used_max_per_node: 'Memory Usage',
    read_bytes: 'Total Read Size',
    write_bytes: 'Total Write Size',
    used_mean_per_gpu: 'GPU Usage',
    mem_used_max_per_gpu: 'GPU Memory',
    power_mean_per_gpu: 'CPU Power',
    gpu_count: 'Number of GPUs',
};

export class FootprintChart {
    display_num: number;
    monitoringservice: MonitoringService;
    footprints: SelectItem[];
    selectedFootprint1: string = 'undefined';
    selectedFootprint2: string = 'undefined';
    class_num: SelectItem[];
    selectedClassNumber: number = 0;
    plots_hide: boolean;
    spinner_hide: boolean;
    no_data_string: string;
    refresh_button_disabled: boolean;
    show_token_expired_dialog = false;

    /* chart data for plotly */
    data: any;
    layout: any;
    config: any;

    /* internal chart data */
    private units: any;
    private tags: any;
    private title: string = '';
    chart_title_caption: string;
    private rows1: any[] = [];
    private rows1_label: any[] = [];
    private label_array: any[] = [];
    private rows2: any[] = [];
    private rows3: any[] = [];
    private router: Router;
    private route: ActivatedRoute;
    private draw_histogram: boolean;
    // for select a job in scatter plot
    private x_width: number = 0;
    private y_width: number = 0;
    // zooming in scatter plot
    private x_axis_start: number | undefined;
    private x_axis_end: number | undefined;
    private y_axis_start: number | undefined;
    private y_axis_end: number | undefined;
    private last_x_axis_start: number | undefined;
    private last_x_axis_end: number | undefined;
    private last_y_axis_start: number | undefined;
    private last_y_axis_end: number | undefined;

    constructor(
        display_num: number,
        monitoringservice: MonitoringService,
        route: ActivatedRoute,
        router: Router,
        footprint1: string,
        footprint2: string,
        class_number: number
    ) {
        this.display_num = display_num;
        this.chart_title_caption = '';

        // console.log('Display number: ', this.display_num, class_number);

        if (
            localStorage.getItem(
                'footprint-chart-' + this.display_num.toString()
            )
        ) {
            // console.log("Display:", localStorage.getItem('footprint-chart-' + this.display_num.toString()));
            var displaySettings: any = JSON.parse(
                localStorage.getItem(
                    'footprint-chart-' + this.display_num.toString()
                )!
            );
            this.selectedFootprint1 = displaySettings[0];
            this.selectedFootprint2 = displaySettings[1];
            this.selectedClassNumber = displaySettings[2];
        } else {
            if (this.display_num < 2) {
                // defaults if no storage is available
                if (this.display_num == 0) {
                    this.selectedFootprint1 = 'states';
                    this.selectedFootprint2 = 'undefined';
                    this.selectedClassNumber = 15;
                }
                if (this.display_num == 1) {
                    this.selectedFootprint1 = 'duration';
                    this.selectedFootprint2 = 'time_limit';
                    this.selectedClassNumber = 50;
                }
            } else {
                this.selectedFootprint1 = footprint1;
                this.selectedFootprint2 = footprint2;
                this.selectedClassNumber = class_number;
            }

            // save display settings
            localStorage.setItem(
                'footprint-chart-' + this.display_num.toString(),
                JSON.stringify([
                    this.selectedFootprint1,
                    this.selectedFootprint2,
                    this.selectedClassNumber,
                ])
            );
        }

        this.monitoringservice = monitoringservice;
        this.route = route;
        this.router = router;

        this.footprints = [
            { label: '--', value: 'undefined' },
            { label: 'Job State', value: 'states' },
            { label: 'Job Tag', value: 'tags' },
        ];

        if (this.monitoringservice.enableShowEnergy()) {
            this.footprints.push(
                {
                    label: 'Job Energy',
                    value: 'energy_in_kwh',
                },
                {
                    label: 'Job Energy Efficiency',
                    value: 'energy_efficiency_in_gflops_per_watt',
                }
            );
        }

        this.footprints.push(
            // { label: 'Roofline', value: 'roofline' },
            { label: 'Number of Nodes', value: 'node_count' },
            { label: 'Number of Cores', value: 'core_count' },
            { label: 'Number of GPUs', value: 'gpu_count' },
            { label: 'Walltime', value: 'time_limit' },
            { label: 'Duration', value: 'duration' },
            { label: 'Core Duration', value: 'core_duration' },
            { label: 'Pending Time', value: 'pending_time' },
            { label: 'Mean IPC per Core', value: 'ipc_mean_per_core' },
            {
                label: 'Mean CPU Usage per Core',
                value: 'cpu_used_mean_per_core',
            },
            {
                label: 'Mean Normalized SP Flops per Core',
                value: 'flops_any_mean_per_core',
            },
            {
                label: 'Mean Memory Bandwidth per Socket',
                value: 'mem_bw_mean_per_socket',
            },
            {
                label: 'Mean CPU Power per Socket',
                value: 'cpu_power_mean_per_socket',
            },
            {
                label: 'Mean Infiniband Bandwidth per Node',
                value: 'ib_bw_mean_per_node',
            },
            {
                label: 'Max Host Memory Usage per Node',
                value: 'host_mem_used_max_per_node',
            },
            { label: 'Total Read Size', value: 'read_bytes' },
            { label: 'Total Write Size', value: 'write_bytes' },
            { label: 'Mean GPU Usage', value: 'used_mean_per_gpu' },
            { label: 'Max GPU Memory', value: 'mem_used_max_per_gpu' },
            { label: 'Mean GPU Power', value: 'power_mean_per_gpu' }
        );

        var max_class_number = 50;
        this.class_num = [];
        for (let i = 5; i <= max_class_number; i++) {
            this.class_num.push({ label: i.toString(), value: i });
        }

        this.units = new Map();
        this.units.set('states', '');
        this.units.set('tags', '');
        // this.units.set('roofline', '');
        this.units.set('node_count', '');
        this.units.set('core_count', '');
        this.units.set('time_limit', ' in hours');
        this.units.set('duration', ' in hours');
        this.units.set('core_duration', ' in hours');
        this.units.set('pending_time', ' in hours');
        this.units.set('ipc_mean_per_core', '');
        this.units.set('cpu_used_mean_per_core', '');
        this.units.set('flops_any_mean_per_core', '');
        this.units.set('mem_bw_mean_per_socket', ' in Bytes/s');
        this.units.set('cpu_power_mean_per_socket', ' in W');
        this.units.set('energy_in_kwh', ' in kWh');
        this.units.set('energy_efficiency_in_gflops_per_watt', ' in GFLOPS/W');
        this.units.set('ib_bw_mean_per_node', ' in Bytes/s');
        this.units.set('host_mem_used_max_per_node', ' in Bytes');
        this.units.set('read_bytes', ' in Bytes');
        this.units.set('write_bytes', ' in Bytes');
        this.units.set('used_mean_per_gpu', '');
        this.units.set('mem_used_max_per_gpu', ' in Bytes');
        this.units.set('power_mean_per_gpu', ' in W');
        this.units.set('gpu_count', '');

        this.label_array = [];
        for (let i = 0; i <= max_class_number; i++) {
            this.label_array.push(i);
        }

        this.refresh_button_disabled = true;
        this.plots_hide = true;
        this.no_data_string = '';
        this.spinner_hide = false;
        this.draw_histogram = true;
        this.drawChart();

        this.config = {
            responsive: true,
            displaylogo: false,
            displayModeBar: false,
        };
    }

    redraw(refresh: boolean, max_binning = this.selectedClassNumber) {
        // save display settings
        localStorage.setItem(
            'footprint-chart-' + this.display_num.toString(),
            JSON.stringify([
                this.selectedFootprint1,
                this.selectedFootprint2,
                this.selectedClassNumber,
            ])
        );

        // remove last session
        var footprintSessionName =
            'footprint-chart-values_' +
            this.selectedFootprint1 +
            '_' +
            this.selectedFootprint2 +
            '_' +
            this.selectedClassNumber +
            this.route.snapshot.url.join('');
        sessionStorage.removeItem(footprintSessionName);

        if (refresh) {
            this.x_axis_start = undefined;
            this.x_axis_end = undefined;
            this.last_x_axis_start = undefined;
            this.last_x_axis_end = undefined;
            this.y_axis_start = undefined;
            this.y_axis_end = undefined;
            this.last_y_axis_start = undefined;
            this.last_y_axis_end = undefined;
        }
        this.refresh_button_disabled = true;
        this.plots_hide = true;
        this.spinner_hide = false;
        this.drawChart(max_binning);
    }

    drawChart(max_binning = this.selectedClassNumber) {
        this.title = this.getFootprintTitle(this.selectedFootprint1)!;

        if (
            this.selectedFootprint1 != 'undefined' &&
            this.selectedFootprint2 == 'undefined'
        )
            this.chart_title_caption = this.title;
        else
            this.chart_title_caption =
                scatterplot_captions[this.selectedFootprint1] +
                ' / ' +
                scatterplot_captions[this.selectedFootprint2];

        this.rows1 = [];
        this.rows1_label = [];
        this.rows2 = [];
        this.rows3 = [];
        this.no_data_string = '';

        // remember last session
        var footprintSessionName =
            'footprint-chart-values_' +
            this.selectedFootprint1 +
            '_' +
            this.selectedFootprint2 +
            '_' +
            this.selectedClassNumber +
            this.route.snapshot.url.join('');
        if (sessionStorage.getItem(footprintSessionName)) {
            console.log('found fp session: ', footprintSessionName);
            try {
                let session_data: any = JSON.parse(
                    sessionStorage.getItem(footprintSessionName)!
                );
                this.rows1 = session_data[0];
                this.rows1_label = session_data[1];
                this.rows2 = session_data[2];
                this.rows3 = session_data[3];
                if (
                    this.selectedFootprint1 != 'undefined' &&
                    this.selectedFootprint2 == 'undefined'
                ) {
                    this.draw_histogram = true;
                } else {
                    this.x_width = session_data[4];
                    this.y_width = session_data[5];
                    this.draw_histogram = false;
                }
            } catch {
                console.log('Session data corrupt for ', footprintSessionName);
                sessionStorage.removeItem('footprintSessionName');
                this.redraw(true);
                return;
            }

            if (this.rows1.length > 0) {
                this.drawData();
                this.plots_hide = false;
            } else {
                console.log('Restoring session data failed');
                this.redraw(true);
                return;
            }
            this.spinner_hide = true;
            this.refresh_button_disabled = false;
        } else {
            if (
                this.selectedFootprint1 == 'states' ||
                this.selectedFootprint1 == 'tags'
            )
                this.selectedFootprint2 = 'undefined';
            this.monitoringservice
                .getChartData(
                    this.selectedFootprint1,
                    this.selectedFootprint2,
                    max_binning,
                    this.x_axis_start,
                    this.x_axis_end,
                    this.y_axis_start,
                    this.y_axis_end
                )
                .subscribe({
                    next: (data: any) => {
                        for (var key in data) {
                            if (data.hasOwnProperty(key)) {
                                if (
                                    this.selectedFootprint1 != 'undefined' &&
                                    this.selectedFootprint2 == 'undefined'
                                ) {
                                    if (data[key]['RANGE']) {
                                        this.rows1.push(data[key]['RANGE']);
                                        this.rows1_label.push(
                                            this.convertRange(
                                                data[key]['RANGE']
                                            )
                                        );
                                        this.rows2.push(
                                            data[key]['footprint_1']
                                        );
                                    } else if (data[key]['job_state']) {
                                        this.rows1.push(data[key]['job_state']);
                                        this.rows1_label.push(
                                            data[key]['job_state']
                                        );
                                        this.rows2.push(
                                            data[key]['footprint_1']
                                        );
                                    } else if ('tags' in data[key]) {
                                        // console.log("tag: ", data[key]["tags"], this.monitoringservice.getJobtags(data[key]["tags"]), data[key]["footprint_1"]);
                                        let tag_value: string =
                                            ':' + data[key]['tags'].toString();
                                        this.rows1.push(tag_value);
                                        this.rows1_label.push(
                                            this.monitoringservice.getJobtags(
                                                tag_value
                                            )
                                        );
                                        // console.log("debug: ", this.rows1, this.rows1_label);
                                        // (this.rows1_label).push(data[key]["tags"]);
                                        this.rows2.push(
                                            data[key]['footprint_1']
                                        );
                                    } else
                                        console.log(
                                            'No Range or Status available!'
                                        );
                                    this.draw_histogram = true;
                                } else if (
                                    this.selectedFootprint1 != 'undefined' &&
                                    this.selectedFootprint1 != 'states' &&
                                    this.selectedFootprint1 != 'tags' &&
                                    this.selectedFootprint2 != 'undefined' &&
                                    this.selectedFootprint2 != 'states' &&
                                    this.selectedFootprint2 != 'tags'
                                ) {
                                    if (data[key]['class_width_1'])
                                        this.x_width = this.convertValue(
                                            data[key]['class_width_1'],
                                            this.selectedFootprint1
                                        );
                                    if (data[key]['class_width_2'])
                                        this.y_width = this.convertValue(
                                            data[key]['class_width_2'],
                                            this.selectedFootprint2
                                        );

                                    if (data[key]['footprint_1'])
                                        this.rows1.push(
                                            this.convertValue(
                                                data[key]['footprint_1'],
                                                this.selectedFootprint1
                                            )
                                        );
                                    if (data[key]['footprint_2'])
                                        this.rows2.push(
                                            this.convertValue(
                                                data[key]['footprint_2'],
                                                this.selectedFootprint2
                                            )
                                        );
                                    if (data[key]['footprint_count'])
                                        this.rows3.push(
                                            data[key]['footprint_count']
                                        );
                                    this.draw_histogram = false;
                                }
                            }
                        }
                        //console.log("number of data:", this.rows1.length)
                        try {
                            let session_data: any = [
                                this.rows1,
                                this.rows1_label,
                                this.rows2,
                                this.rows3,
                                this.x_width,
                                this.y_width,
                            ];
                            sessionStorage.setItem(
                                footprintSessionName,
                                JSON.stringify(session_data)
                            );
                        } catch {
                            console.log(
                                'SessionStorage failed due to quota. Reset all stored sessions.'
                            );
                            sessionStorage.clear();
                        }

                        if (
                            (this.rows1.length > 0 && this.draw_histogram) ||
                            (this.rows1.length > 1 &&
                                this.rows2.length > 1 &&
                                !this.draw_histogram)
                        ) {
                            this.drawData();
                            this.plots_hide = false;
                        } else {
                            this.no_data_string =
                                'No data available in the selected time interval.';
                            // if ( this.draw_histogram )
                            //   this.no_data_string = "No data available for \"" + scatterplot_captions[this.selectedFootprint1] + "\" in the selected time interval.";
                            // else
                            // this.no_data_string = "No data available for \"" + scatterplot_captions[this.selectedFootprint1] + " / " + scatterplot_captions[this.selectedFootprint2] + "\" in the selected time interval.";
                        }
                        this.spinner_hide = true;
                        this.refresh_button_disabled = false;
                    },
                    error: (e) => {
                        this.spinner_hide = true;
                        this.refresh_button_disabled = false;
                        this.show_token_expired_dialog =
                            this.monitoringservice.showExpiredSessionDialog(e);
                    },
                });
        }
    }

    drawData() {
        if (this.draw_histogram == true) this.histogram_chart();
        else this.scatterplot();
    }

    getFootprintTitle(value: string) {
        let i = this.footprints.find((item) => item.value == value);
        var title = i ? i.label : '';
        //add unit
        var units = this.units.get(value);
        title = title + units;
        return title;
    }

    convertRange(range: string) {
        // console.log("Convert Range for ", this.selectedFootprint1);
        // console.log("Range: ", range);

        var range_array = range.split('-', 2);
        var val1: any;
        var val2: any;

        if (
            [
                'core_count',
                'node_count',
                'gpu_count',
                'time_limit',
                'duration',
                'core_duration',
                'pending_time',
            ].includes(this.selectedFootprint1)
        ) {
            //val1 = Math.floor(parseFloat(range_array[0]));
            //val2 = Math.ceil(parseFloat(range_array[1]));
            if (
                [
                    'time_limit',
                    'duration',
                    'core_duration',
                    'pending_time',
                ].includes(this.selectedFootprint1)
            ) {
                val1 = Math.ceil(parseFloat(range_array[0]) / 3600);
                val2 = Math.floor(parseFloat(range_array[1]) / 3600);
            } else {
                val1 = parseFloat(range_array[0]);
                val2 = parseFloat(range_array[1]);
            }
        } else {
            val1 = Math.round(parseFloat(range_array[0]) * 100) / 100;
            val2 = Math.round(parseFloat(range_array[1]) * 100) / 100;
        }

        // we do not have zero nodes/cores
        if (
            ['core_count', 'node_count', 'gpu_count'].includes(
                this.selectedFootprint1
            ) &&
            val1 == 0
        )
            val1 = 1;
        if (
            ['core_count', 'node_count', 'gpu_count'].includes(
                this.selectedFootprint2
            ) &&
            val2 == 0
        )
            val2 = 1;
        val1 = this.monitoringservice.shortenLargeNumber(val1, 2);
        val2 = this.monitoringservice.shortenLargeNumber(val2, 2);

        if (val1 == val2) return String(val1);
        return String(val1 + '-' + val2);
    }

    convertValue(value: number, footprint: string) {
        if (
            [
                'time_limit',
                'duration',
                'core_duration',
                'pending_time',
            ].includes(footprint)
        )
            return value / 3600;
        return value;
    }

    reconvertValue(value: number, footprint: string) {
        if (
            [
                'time_limit',
                'duration',
                'core_duration',
                'pending_time',
            ].includes(footprint)
        )
            return value * 3600;
        return value;
    }

    histogram_chart() {
        // console.log('rows1: ', this.rows1);
        // console.log('rows1_label: ', this.rows1_label);
        // console.log('rows2: ', this.rows2);

        var trace: any = {
            x: this.rows1,
            y: this.rows2,
            type: 'bar',
        };

        if (this.selectedFootprint1 == 'states') {
            var colors = this.rows1.map((status: string) => {
                if (['completed', 'running'].includes(status)) return '#2ca02c'; //green
                if (['failed', 'OOM', 'corrupt'].includes(status))
                    return 'd62728'; //red
                if (['timeout', 'cancelled'].includes(status)) return '#ff7f0e'; //orange
                return '#1f77b4'; //blue
            });

            trace = {
                x: this.rows1,
                y: this.rows2,
                type: 'bar',
                marker: {
                    color: colors, // Assigning the color array
                },
            };
        }

        if (
            this.selectedFootprint1 == 'states' ||
            this.selectedFootprint1 == 'tags' ||
            this.selectedClassNumber < 16
        ) {
            trace['hoverinfo'] = 'none';
            trace['cliponaxis'] = false;
            trace['text'] = this.rows2;
            trace['textposition'] = 'outside';
            //trace['textangle'] = '0';
        }

        this.data = [trace];

        this.layout = {
            height: 350,
            //title: this.title,
            xaxis: {
                //tickangle: -45,
                automargin: true,
                tickmode: 'array',
                tickvals: this.label_array,
                ticktext: this.rows1_label,
                type: 'category',
            },
            yaxis: {
                title: { text: 'Number of Jobs' },
                type: 'log',
                autorange: true,
            },
            margin: {
                l: 60,
                r: 40,
                t: 20,
                b: 40,
            },
            plot_bgcolor: 'rgb(250, 250,250)',
            paper_bgcolor: 'rgb(250, 250,250)',
            font: {
                size: 12,
            },
        };
    }

    scatterplot() {
        var trace = {
            x: this.rows1,
            y: this.rows2,
            mode: 'markers',
            type: 'scatter',
            marker: {
                color: this.rows3,
                colorscale: 'Portland',
                // size: 4,
                colorbar: {
                    thickness: 30,
                },
            },
        };

        this.data = [trace];

        this.layout = {
            height: 350,
            // title: scatterplot_captions[this.selectedFootprint1] + " / " + scatterplot_captions[this.selectedFootprint2],
            hovermode: 'closest',
            xaxis: {
                title: {
                    text: this.getFootprintTitle(this.selectedFootprint1),
                },
                //type:'log',
                autorange: true,
                //hoverformat: '.2f'
            },
            yaxis: {
                title: {
                    text: this.getFootprintTitle(this.selectedFootprint2),
                },
                //type:'log',
                autorange: true,
                //hoverformat: '.2f'
            },
            margin: {
                l: 60,
                r: 40,
                t: 20,
                b: 70,
            },
            plot_bgcolor: 'rgb(250, 250,250)',
            paper_bgcolor: 'rgb(250, 250,250)',
            font: {
                size: 12,
            },
        };
    }

    selectJobs(event: any) {
        if (this.draw_histogram == true) this.selectBar(event);
        else this.selectPoint(event);
    }

    zoom(event: any) {
        if (!event['xaxis.range[0]'] && !event['xaxis.range[1]']) return;

        if (this.draw_histogram == false) {
            if (!event['xaxis.range[0]'] || !event['xaxis.range[1]']) {
                this.x_axis_start = this.last_x_axis_start;
                this.x_axis_end = this.last_x_axis_end;
            } else {
                this.x_axis_start = this.reconvertValue(
                    event['xaxis.range[0]'],
                    this.selectedFootprint1
                );
                this.x_axis_end = this.reconvertValue(
                    event['xaxis.range[1]'],
                    this.selectedFootprint1
                );
                // avoid negative start values
                if (this.x_axis_start < 0) this.x_axis_start = 0;
            }

            if (!event['yaxis.range[0]'] || !event['yaxis.range[1]']) {
                this.y_axis_start = this.last_y_axis_start;
                this.y_axis_end = this.last_y_axis_end;
            } else {
                this.y_axis_start = this.reconvertValue(
                    event['yaxis.range[0]'],
                    this.selectedFootprint2
                );
                this.y_axis_end = this.reconvertValue(
                    event['yaxis.range[1]'],
                    this.selectedFootprint2
                );
                // avoid negative start values
                if (this.y_axis_start < 0) this.y_axis_start = 0;
            }

            //remember last values for next zoom
            this.last_x_axis_start = this.x_axis_start;
            this.last_x_axis_end = this.x_axis_end;
            this.last_y_axis_start = this.y_axis_start;
            this.last_y_axis_end = this.y_axis_end;

            // console.log('x-axis start:', this.x_axis_start);
            // console.log('x-axis end:', this.x_axis_end);
            // console.log('y-axis start:', this.y_axis_start);
            // console.log('y-axis end:', this.y_axis_end);
            // console.log('last_x-axis start:', this.last_x_axis_start);
            // console.log('last_x-axis end:', this.last_x_axis_end);
            // console.log('last_y-axis start:', this.last_y_axis_start);
            // console.log('last_y-axis end:', this.last_y_axis_end);

            this.redraw(false);
        }
    }

    selectBar(event: any) {
        if (event.points) {
            //console.log('event: ', event.points[0].x);
            //console.log("selected footprint: ", this.selectedFootprint1);

            var data = event.points[0];
            var metric_name: string = 'job_state';
            var metric_filter: any;
            var val1 = 0;
            var val2 = 0;

            if (
                this.selectedFootprint1 == 'states' ||
                this.selectedFootprint1 == 'tags'
            ) {
                // states and tags do not have ranges
                if (this.selectedFootprint1 == 'tags') {
                    metric_name = 'tags';
                    metric_filter = [data.x.substring(1)];
                } else {
                    metric_name = 'job_state';
                    metric_filter = data.x;
                }
            } else {
                var val_array = data.x.split('-', 2);
                val1 = val_array[0];
                val2 = val_array[1];

                if (this.selectedFootprint1 == 'gpu_count' && val1 == 0)
                    val1 = 1;

                if (
                    this.selectedFootprint1 == 'core_count' ||
                    this.selectedFootprint1 == 'node_count' ||
                    this.selectedFootprint1 == 'gpu_count' ||
                    this.selectedFootprint1 == 'time_limit' ||
                    this.selectedFootprint1 == 'duration' ||
                    this.selectedFootprint1 == 'core_duration' ||
                    this.selectedFootprint1 == 'pending_time'
                ) {
                    metric_name = this.selectedFootprint1 + '_interval';
                    metric_filter = { min: val1, max: val2 };
                } else {
                    metric_name = 'footprint';
                    metric_filter = [
                        { name: this.selectedFootprint1, min: val1, max: val2 },
                    ];
                }
            }

            // console.log('jobs: ', event.points[0].y);
            // console.log('class_num: ', Number(this.selectedClassNumber));
            // this.monitoringservice.clearFootprintFilter();

            // determine max binning number
            var max_binning = this.selectedClassNumber;
            if (['states', 'tags'].includes(this.selectedFootprint1) == false) {
                var binning_interval = val_array[1] - val_array[0];
                if (
                    [
                        'time_limit',
                        'duration',
                        'core_duration',
                        'pending_time',
                    ].includes(this.selectedFootprint1)
                )
                    binning_interval /= 3600;
                binning_interval = Math.floor(binning_interval);

                if (binning_interval < this.selectedClassNumber)
                    max_binning = binning_interval;
            }

            // show jobs only if number of jobs < 2000
            if (
                event.points[0].y < 2000 ||
                max_binning < 3 ||
                this.selectedFootprint1 == 'states' ||
                this.selectedFootprint1 == 'tags'
            ) {
                // this is for the footprint column of the filter component
                this.monitoringservice.addFootprintFilterItem(
                    metric_name,
                    metric_filter
                );
                if (this.selectedFootprint1 == 'states')
                    this.router.navigate([data.x], { relativeTo: this.route });
                else if (this.selectedFootprint1 == 'tags')
                    this.router.navigate(
                        [this.monitoringservice.getJobtags(data.x)],
                        { relativeTo: this.route }
                    );
                else
                    this.router.navigate([this.selectedFootprint1], {
                        relativeTo: this.route,
                    });
            } else {
                console.log('relayout...');

                this.x_axis_start = val1;
                this.x_axis_end = val2;
                this.last_x_axis_start = this.x_axis_start;
                this.last_x_axis_end = this.x_axis_end;

                this.redraw(false, max_binning);
            }
        }
    }

    selectPoint(event: any) {
        if (event.points) {
            var data = event.points[0];

            var x: number;
            var y: number;
            var x_left: number;
            var x_right: number;
            var y_left: number;
            var y_right: number;
            var metric_name: string[] = [];
            var metric_filter: any[] = [];

            x = data.x;
            y = data.y;

            x_left = x - this.x_width / 2;
            x_right = x + this.x_width / 2;
            y_left = y - this.y_width / 2;
            y_right = y + this.y_width / 2;

            // we need integer values for those metrics
            if (
                ['core_count', 'node_count', 'gpu_count'].includes(
                    this.selectedFootprint1
                )
            ) {
                x_left = Math.round(x_left) - 1;
                x_right = Math.round(x_right) + 1;
                y_left = Math.round(y_left) - 1;
                y_right = Math.round(y_right) + 1;
            }

            if (x_left < 0) x_left = 0;
            if (y_left < 0) y_left = 0;

            if (
                [
                    'core_count',
                    'node_count',
                    'gpu_count',
                    'time_limit',
                    'duration',
                    'core_duration',
                    'pending_time',
                ].includes(this.selectedFootprint1)
            ) {
                metric_name.push(this.selectedFootprint1 + '_interval');
                metric_filter.push({
                    min: this.reconvertValue(x_left, this.selectedFootprint1),
                    max: this.reconvertValue(x_right, this.selectedFootprint1),
                });
            } else {
                metric_name.push('footprint');
                metric_filter.push([
                    {
                        name: this.selectedFootprint1,
                        min: this.reconvertValue(
                            x_left,
                            this.selectedFootprint1
                        ),
                        max: this.reconvertValue(
                            x_right,
                            this.selectedFootprint1
                        ),
                    },
                ]);
            }

            if (
                [
                    'core_count',
                    'node_count',
                    'gpu_count',
                    'time_limit',
                    'duration',
                    'core_duration',
                    'pending_time',
                ].includes(this.selectedFootprint2)
            ) {
                metric_name.push(this.selectedFootprint2 + '_interval');
                metric_filter.push({
                    min: this.reconvertValue(y_left, this.selectedFootprint2),
                    max: this.reconvertValue(y_right, this.selectedFootprint2),
                });
            } else {
                if (metric_name[0] == 'footprint') {
                    metric_filter[0].push({
                        name: this.selectedFootprint2,
                        min: this.reconvertValue(
                            y_left,
                            this.selectedFootprint2
                        ),
                        max: this.reconvertValue(
                            y_right,
                            this.selectedFootprint2
                        ),
                    });
                } else {
                    metric_name.push('footprint');
                    metric_filter.push([
                        {
                            name: this.selectedFootprint2,
                            min: this.reconvertValue(
                                y_left,
                                this.selectedFootprint2
                            ),
                            max: this.reconvertValue(
                                y_right,
                                this.selectedFootprint2
                            ),
                        },
                    ]);
                }
            }

            var job_num = this.rows3[data.pointIndex];
            // this.monitoringservice.clearFootprintFilter();
            // console.log("job number: ",job_num);
            // console.log("x: ", x);
            // console.log("y: ", y);
            // console.log("dx: ", x_right - x_left);
            // console.log("dy: ", y_right - y_left);
            // console.log("class_num: ", Number(this.selectedClassNumber));
            // console.log("x_width: ", this.x_width);
            // console.log("y_width: ", this.y_width);

            // show jobs only if number of jobs < 2000
            if (job_num < 2000) {
                // this is for the footprint column of the filter component
                for (let i = 0; i < metric_name.length; i++) {
                    this.monitoringservice.addFootprintFilterItem(
                        metric_name[i],
                        metric_filter[i]
                    );
                }
                this.router.navigate([this.selectedFootprint1], {
                    relativeTo: this.route,
                });
            } else {
                console.log('relayout...');

                this.x_axis_start = this.reconvertValue(
                    x_left,
                    this.selectedFootprint1
                );
                this.x_axis_end = this.reconvertValue(
                    x_right,
                    this.selectedFootprint1
                );
                this.last_x_axis_start = this.x_axis_start;
                this.last_x_axis_end = this.x_axis_end;
                this.y_axis_start = this.reconvertValue(
                    y_left,
                    this.selectedFootprint2
                );
                this.y_axis_end = this.reconvertValue(
                    y_right,
                    this.selectedFootprint2
                );
                this.last_y_axis_start = this.y_axis_start;
                this.last_y_axis_end = this.y_axis_end;

                // console.log("this.x_axis_start: ", this.x_axis_start);
                // console.log("this.x_axis_end: ", this.x_axis_end);
                // console.log("this.y_axis_start: ", this.y_axis_start);
                // console.log("this.y_axis_end: ", this.y_axis_end);

                this.redraw(false);
            }
        }
    }
}
