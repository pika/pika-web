import { Component } from '@angular/core';
import { MonitoringService } from '../service/monitoring.service';
import { ActivatedRoute, Router } from '@angular/router';
import { JobBaseComponent } from './job-base.component';

@Component({
    selector: 'app-project',
    templateUrl: './job-base.component.html',
    styleUrls: ['./job-base.component.css'],
})
export class ProjectComponent extends JobBaseComponent {
    stateKey: string = 'project';
    constructor(
        monitoringservice: MonitoringService,
        route: ActivatedRoute,
        router: Router,
    ) {
        super(monitoringservice, route, router);
    }

    override componentInit() {
        this.stateKey = 'project';
        this.monitoringservice.setActiveComponent(this.stateKey);
        this.monitoringservice.Reload.subscribe((data) => {
            if (this.stateKey == data) {
                console.log('Reload project data');
                this.reloadTable();
            }
        });
    }

    override setTableColums() {
        this.cols = [];
        /* get Project table */
        var project_table =
            this.monitoringservice.getTableDefinition('Project_Table');
        for (let index of project_table) {
            this.cols.push(this.col_defines.get(index));
        }
        this.caption_total_name = 'Projects';
        this.footprint_route = 'project_name';
    }

    override getTableData() {
        if (sessionStorage.getItem(this.sessionStorageName)) {
            console.log(
                'Session found for ',
                this.stateKey,
                this.sessionStorageName,
            );
            this.rows = JSON.parse(
                sessionStorage.getItem(this.sessionStorageName)!,
            );
            this.loading = false;
        } else {
            this.monitoringservice.getProjectData().subscribe({
                next: (data) => {
                    this.rows = data;
                    try {
                        sessionStorage.setItem(
                            this.sessionStorageName,
                            JSON.stringify(data),
                        );
                    } catch {
                        console.log(
                            'SessionStorage failed due to quota. Reset all stored sessions.',
                        );
                        sessionStorage.clear();
                    }
                    this.loading = false;
                },
                error: (e) => {
                    this.loading = false;
                    this.show_token_expired_dialog =
                        this.monitoringservice.showExpiredSessionDialog(e);
                },
            });
        }
    }
}
