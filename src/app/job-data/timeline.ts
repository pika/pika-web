import { MonitoringService } from '../service/monitoring.service';
import { MenuItem } from 'primeng/api';
import { Subscription } from 'rxjs';
declare const Plotly: any;

class PlotlyChart {
    monitoringservice: MonitoringService;
    display_index: number;
    display_num: number; // number of displays for hoverAllDisplays and unhoverAllDisplays
    unix_job_start: number;
    unix_job_end: number;

    layout: any;
    data: any;

    histogram_layout: any;
    histogram_data: any;

    config: any;

    trace_num: number = 0;
    current_range: number[] = [];

    constructor(
        monitoringservice: MonitoringService,
        display_index: number,
        display_num: number,
        height: number,
        unix_job_start: number,
        unix_job_end: number
    ) {
        this.monitoringservice = monitoringservice;
        this.display_index = display_index;
        this.display_num = display_num;
        this.unix_job_start = unix_job_start;
        this.unix_job_end = unix_job_end;

        this.layout = {
            height: height,
            legend: {
                orientation: 'h',
                traceorder: 'reversed',
            },
            hovermode: 'x unified',
            hoverlabel: {
                bgcolor: '#F0ECEC',
                bordercolor: '#F0ECEC',
                position: 'middle',
            },
            hoveron: 'lines',
            margin: {
                l: 40,
                r: 20,
                t: 5,
                b: 40,
            },
            plot_bgcolor: 'rgb(250, 250,250)',
            paper_bgcolor: 'rgb(250, 250,250)',
            font: {
                size: 11,
            },
            spikedistance: -1,
            xaxis: {
                spikemode: 'across+toaxis',
                spikesnap: 'cursor',
                spikedash: 'solid',
                spikecolor: 'red',
                showspikes: true,
                showline: true,
                showgrid: true,
                spikethickness: 1,
                fixedrange: false,
                zeroline: false,
                rangemode: 'nonnegative',
                tickmode: 'array',
                tickvals: [],
                ticktext: [],
            },
            yaxis: {
                ticksuffix: '  ',
                fixedrange: true,
                hoverformat: ',.4s',
                exponentformat: 'SI',
                autorange: true,
                zeroline: false,
                rangemode: 'nonnegative',
                range: null,
                dtick: null,
                nticks: null,
            },
            shapes: [],
            annotations: [],
            //colorway : ['#1f77b4', '#ff7f0e', '#2ca02c' ,'#d62728', '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf']
        };

        this.data = [];

        this.histogram_layout = {
            height: height,
            margin: {
                l: 40,
                r: 20,
                t: 5,
                b: 40,
            },
            plot_bgcolor: 'rgb(250, 250,250)',
            paper_bgcolor: 'rgb(250, 250,250)',
            font: {
                size: 11,
            },
            xaxis: {
                automargin: true,
                fixedrange: true,
                ticksuffix: null,
                exponentformat: 'SI',
                rangemode: 'nonnegative',
            },
            yaxis: {
                title: {
                    text: null,
                },
                type: 'log',
                fixedrange: true,
                autorange: true,
                nticks: height > 200 ? null : 3,
            },
        };

        this.histogram_data = [];

        this.config = {
            responsive: true,
            displaylogo: false,
            displayModeBar: false,
            showTips: false,
        };
    }

    plotTimelines(
        metric: string,
        timelines: any,
        value_type: string,
        auto_range: boolean,
        show_only_default_io_timelines: boolean,
        height: number
    ) {
        this.data = [];

        const timestamps: number[] = timelines[0]['x'];
        const first_timestamp = timestamps[0];
        const last_timestamp = timestamps[timestamps.length - 1];

        for (let timeline of timelines) {
            var trace: any = {
                x: timeline.x,
                y: timeline.y,
                text: timeline.text,
                type: 'scatter',
                mode: 'lines',
                connectgaps: true,
                marker: {
                    size: 5,
                },
                visible: 'true',
                hovertemplate: '%{y}' + timeline.hover_info,
                hoverlabel: { namelength: -1 }, //avoid truncating of long names
                name: timeline.name,
            };

            // adjust mode

            /* show markers for range smaller than 10 minutes */
            if (
                last_timestamp - first_timestamp <= 60 * 10 &&
                metric != 'node_power'
            ) {
                trace['mode'] = 'lines+markers';
            }

            if (
                metric == 'job_mem_used' &&
                value_type == 'aggregated' &&
                timeline.name == 'requested'
            )
                trace['mode'] = 'lines';

            // extra line for meanstddev
            if (value_type == 'meanstddev') {
                trace['error_y'] = {
                    type: 'data',
                    //color: '#555',
                    width: 2,
                    array: timeline.meanstddev,
                };
            }

            if (!metric.includes('_io')) {
                const colorMap: Record<string, string> = {
                    min: '#ff7f0e', // orange
                    mean: '#1f77b4', // blue
                    max: '#2ca02c', // green
                    lowest: '#ff7f0e', // orange
                    best: '#2ca02c', // green
                };
                trace['marker']['color'] =
                    colorMap[timeline.name] ||
                    colorMap[value_type] ||
                    trace['marker']['color'];
                // Special case for `job_mem_used` and `aggregated`
                if (metric === 'job_mem_used' && value_type === 'aggregated') {
                    const memColorMap: Record<string, string> = {
                        requested: 'd62728', // red
                        used: '#1f77b4', // blue
                    };
                    trace['marker']['color'] =
                        memColorMap[timeline.name] || trace['marker']['color'];
                }
            }

            this.data.push(trace);
        }

        // adjust layout

        /* disable zoom for range smaller than 4 minutes */
        this.layout['xaxis']['fixedrange'] = false;
        if (last_timestamp - first_timestamp <= 60 * 4)
            this.layout['xaxis']['fixedrange'] = true;

        this.layout['height'] = height;
        this.layout['yaxis']['nticks'] = null;
        if (height < 200) this.layout['yaxis']['nticks'] = 3;

        // reduce y-ticks if we have a small range of y values
        if (this._reduceTicks(timelines[0].y) === true) {
            this.layout['yaxis']['nticks'] = null;
            if (!metric.includes('usage') && !metric.includes('ipc'))
                // Displays numbers with SI units and 3 significant digits
                this.layout['yaxis']['tickformat'] = '.3s';
        }

        // adjust hoverformat for small y values
        if (metric.includes('usage') || metric.includes('ipc')) {
            this.layout['yaxis']['hoverformat'] = null; //'.3f';
        }

        // fix range for cpu and gpu usage
        if (metric == 'cpu_usage' || metric == 'gpu_usage') {
            if (auto_range == false && value_type != 'aggregated') {
                this.layout['yaxis'] = {
                    ...this.layout['yaxis'],
                    autorange: false,
                    range: [0, 1.1],
                    dtick: height < 200 ? 0.5 : 0.2,
                };
            } else {
                this.layout['yaxis'] = {
                    ...this.layout['yaxis'],
                    autorange: true,
                    range: null,
                    dtick: null,
                };
            }
        }

        this.trace_num = this.data.length;

        // do not show all io traces per default
        if (metric.includes('_io') && show_only_default_io_timelines) {
            // show only the first traces that contains at least one of the keywords
            const trace_keywords = ['read', 'write', 'open', 'close'];
            this.trace_num = this._hideIOTraces(
                this.data,
                trace_keywords,
                false //'legendonly'
            );
        }

        // use this color scheme for io metrics
        if (metric.includes('_io')) {
            this.layout['colorway'] = [
                '#d62728',
                '#9467bd',
                '#8c564b',
                '#e377c2',
                '#7f7f7f',
                '#bcbd22',
                '#17becf',
                '#1f77b4',
                '#ff7f0e',
                '#2ca02c',
            ];

            this.layout['legend']['traceorder'] = 'normal';
        } else {
            this.layout['legend']['traceorder'] = 'reversed';
        }

        // Adjust legend based on current chart height and width
        this.adjustLegendAndCreateXLabels(height);
    }

    adjustLegendAndCreateXLabels(height: number) {
        // this function is called at initialization and after the autosize event
        setTimeout(() => {
            let update: Record<string, any> = {};
            const chart_width = this._chart_width();

            if (chart_width) {
                const legend_width = this.trace_num * 150 - 50;
                //console.log('***', legend_width, chart_width);

                if (height < 200) {
                    update['legend.y'] = -1;
                    if (legend_width > chart_width) update['margin.b'] = 85;
                    else update['margin.b'] = 40;
                } else {
                    update['legend.y'] = null;
                    if (legend_width > chart_width) update['legend.y'] = -0.2;
                }

                // Convert timestamps to humanreadable duration using tickvals and ticktext
                const timestamps: number[] = this.data[0]['x'];
                const unix_start = timestamps[0];
                const unix_end = timestamps[timestamps.length - 1];
                const duration_seconds = unix_end - unix_start;
                const unit_seconds: Record<string, any> = {
                    min: 60,
                    h: 3600,
                    d: 86400,
                };
                let unit = 'min';

                /* determine duration unit based on length of job */
                if (duration_seconds > unit_seconds['d'] * 4) unit = 'd';
                else if (duration_seconds > unit_seconds['h'] * 4) unit = 'h';

                let x_start = unix_start;
                const offset = x_start - this.unix_job_start;
                if (offset < 60) x_start = this.unix_job_start;
                let x_end = unix_end;

                const relative_duration = timestamps.map(
                    (ts) => (ts - x_start) / unit_seconds[unit]
                );
                // Define tick values
                const max_duration = Math.ceil(
                    relative_duration[relative_duration.length - 1]
                );

                const tickvals = [];
                const ticktext = [];
                const intervals_size = 89; //pixel
                let tickvals_step = Math.ceil(
                    (max_duration / chart_width) * intervals_size
                );

                if (
                    unit_seconds['d'] * 1 < duration_seconds &&
                    duration_seconds < unit_seconds['d'] * 2
                ) {
                    tickvals_step = 6;
                } else if (
                    unit_seconds['d'] * 2 < duration_seconds &&
                    duration_seconds < unit_seconds['d'] * 4
                ) {
                    tickvals_step = 12;
                } else {
                    // Round up to the nearest allowed step size
                    if (tickvals_step > 15) tickvals_step = 30;
                    else if (tickvals_step > 10) tickvals_step = 15;
                    else if (tickvals_step > 5) tickvals_step = 10;
                    else if (tickvals_step > 2) tickvals_step = 5;
                    else if (tickvals_step > 1) tickvals_step = 2;
                    else tickvals_step = 1;
                }

                function _determineTicktext(value: number): string {
                    function _formatUnixDuration(
                        unix_seconds: number,
                        last_unit: string
                    ): string {
                        const ONE_MINUTE = 60;
                        const ONE_HOUR = ONE_MINUTE * 60;
                        const ONE_DAY = ONE_HOUR * 24;

                        const days = Math.floor(unix_seconds / ONE_DAY);
                        unix_seconds %= ONE_DAY;

                        const hours = Math.floor(unix_seconds / ONE_HOUR);
                        unix_seconds %= ONE_HOUR;

                        const minutes = Math.floor(unix_seconds / ONE_MINUTE);

                        // remove unnecessary zero values
                        return last_unit === 'd'
                            ? `${days}\u200Ad`
                            : last_unit === 'h'
                            ? days > 0
                                ? `${days}\u200Ad${
                                      hours > 0 ? `:${hours}\u200Ah` : ''
                                  }`
                                : `${hours}\u200Ah`
                            : days > 0
                            ? `${days}\u200Ad${
                                  hours > 0 ? `:${hours}\u200Ah` : ''
                              }${minutes > 0 ? `:${minutes}\u200Amin` : ''}`
                            : hours > 0
                            ? `${hours}\u200Ah${
                                  minutes > 0 ? `:${minutes}\u200Amin` : ''
                              }`
                            : `${minutes}\u200Amin` || `0\u200Amin`;
                    }
                    let text = '';
                    if (offset < 60)
                        text = _formatUnixDuration(
                            value * unit_seconds[unit],
                            unit
                        );
                    else {
                        text = _formatUnixDuration(
                            offset + value * unit_seconds[unit],
                            unit
                        );
                    }

                    return text;
                }

                for (let i = 0; i <= max_duration; i += tickvals_step) {
                    tickvals.push(x_start + i * unit_seconds[unit]); // Convert back to timestamps
                    ticktext.push(
                        i === 0 && x_start !== this.unix_job_start
                            ? ''
                            : _determineTicktext(i)
                    ); // Integer values only
                }

                update = {
                    ...update,
                    'xaxis.tickvals': tickvals,
                    'xaxis.ticktext': ticktext,
                    'xaxis.range': [x_start - 2, x_end + 2],
                    'xaxis.autorange': false,
                };

                this.current_range = [x_start - 2, x_end + 2];

                Plotly.relayout('timeline_' + this.display_index, update);
            }
        }, 0);
    }

    addHorizontalLine(x0: number, y0: number, x1: number, y1: number) {
        var shapes = [
            {
                type: 'line',
                x0: x0,
                y0: y0,
                x1: x1,
                y1: y1,
                line: {
                    color: '#555',
                    width: 1,
                    dash: 'dot',
                },
            },
        ];
        this.layout['shapes'] = shapes;
    }

    plotHistogram(metric: string, data: any, bin_size: number, height: number) {
        this.histogram_data = [
            {
                x: data,
                type: 'histogram',
                xbins: {
                    size: bin_size,
                    start: null,
                    end: null,
                },
                marker: {
                    line: {
                        color: 'rgb(250, 250,250)',
                        width: 2,
                    },
                },
            },
        ];

        // adjust layout based on metric and height
        this.histogram_layout['height'] = height;

        var histogram_yaxis_title_prefix = 'Number of ';
        if (height < 200) histogram_yaxis_title_prefix = '#';
        var histogram_yaxis_title =
            histogram_yaxis_title_prefix +
            this.monitoringservice.getTimelineCaptions(metric)[
                this.monitoringservice.getTimelineCaptions(metric).length - 1
            ] +
            's';

        this.histogram_layout['yaxis']['title'] = {
            text: histogram_yaxis_title,
        };
    }

    hoverAllDisplays(event: any, index: number) {
        const xValue = event.xvals[0]; // Replace with your desired x-value

        for (let i = 0; i < this.display_num; i++) {
            const element = document.getElementById('timeline_' + i);
            if (element) {
                if (i == index) continue;
                Plotly.Fx.hover('timeline_' + i, {
                    xval: xValue,
                    curveNumber: 0,
                });

                // Adjust the spike (crosshair) position to simulate the spike line
                Plotly.relayout('timeline_' + i, {
                    'xaxis.showspikes': true,
                    'xaxis.spikecolor': 'red',
                    'xaxis.spikethickness': 1,
                    'xaxis.spikedash': 'solid',
                    'xaxis.spikemode': 'across+toaxis',
                    'xaxis.spikesnap': 'data',
                });
            }
        }
    }

    unhoverAllDisplays(event: any, index: number) {
        for (let i = 0; i < this.display_num; i++) {
            const element = document.getElementById('timeline_' + i);
            if (element) {
                if (i == index) continue;
                Plotly.Fx.unhover('timeline_' + i, {
                    curveNumber: 0,
                });
            }
        }
    }

    toggleAutoFixedRangeOnYAxis(
        index: number,
        auto_range: boolean,
        height: number
    ) {
        var range = null;
        var dtick = null;
        //var nticks = null;
        if (auto_range == true) {
            range = [0, 1.1];
            dtick = 0.2;
            if (height < 200) {
                dtick = 0.5;
                //nticks = 3;
            }
        }
        auto_range = !auto_range;
        var update = {
            yaxis: {
                autorange: auto_range,
                range: range,
                dtick: dtick,
                //nticks: nticks,
                rangemode: 'nonnegative',
            },
        };
        Plotly.relayout('timeline_' + index, update);
    }

    _chart_width(): number | undefined {
        // determine current container width
        const container = document.getElementById(
            'timeline_' + this.display_index
        );
        const container_width = container ? container.clientWidth : undefined;
        return container_width;
    }

    _reduceTicks(arr: number[]): boolean {
        // Check if the array is empty
        if (arr.length === 0) {
            return false; // Return null or handle the empty case as needed
        }

        const nonNullItems = arr.filter((item) => item !== null);
        if (nonNullItems.length < 3) return true;

        // Initialize min and max values
        let min = nonNullItems[0];
        let max = nonNullItems[0];

        // Iterate over the array to find the min and max values
        for (const num of nonNullItems) {
            if (num < min) {
                min = num;
            }
            if (num > max) {
                max = num;
            }
        }

        if (max - min == 0) return true;
        return false;
    }

    _hideIOTraces(
        traces: any,
        trace_keywords: any,
        visible_flag: string | boolean = 'legendonly'
    ): number {
        let show_count = 0;
        // to remember first occurance of keyword
        let trace_keywords_dict: any = {};
        for (let trace of trace_keywords) {
            trace_keywords_dict[trace] = false;
        }

        for (let trace of traces) {
            const key = trace.name;

            trace['visible'] = visible_flag;
            // Iterate over keywords and handle visibility logic
            for (const keyword of trace_keywords) {
                if (key.includes(keyword) && !trace_keywords_dict[keyword]) {
                    trace['visible'] = true;
                    trace_keywords_dict[keyword] = true;
                    show_count++;
                    break;
                }
            }
        }
        return show_count;
    }
}

export class TimelineChart {
    monitoringservice: MonitoringService;
    display_num: number;
    display_index: number;
    caption: string = '';

    height: number;
    no_data_padding_top: number;
    column_number: number;
    synchronous_spike_line: boolean;
    metric: string;
    value_type: string;
    job_data: any;
    aggregated_mem: number | undefined = undefined;
    traces: any;
    unit: string;
    start_x1: number;
    start_x2: number;
    is_live: boolean;
    x1: number;
    x2: number;
    zoom_stack: any[];
    start_offset: number;
    loading: boolean;
    // duration_unit: string;
    timeline_visible: boolean;
    histogram_visible: boolean;
    timeline_show_message: boolean;
    show_mean_line: boolean = false;

    // menu
    menu_items: MenuItem[] = [];
    io_sub_menu_num: number = 0;
    selected_menu_item_id: string = 'mean';
    selected_io_menu_item_id: string = '';
    selected_io_submenu_item_id: string = '';
    current_io_metric_caption: string = '';
    show_only_default_io_timelines: boolean = true;

    partition: string;
    io_category: string = '';
    shared_resources: boolean;
    issue_tagged: boolean = false;
    issue: string = '';
    smt_metric: boolean = false;
    smt_info: string = '';
    auto_range: boolean;
    show_token_expired_dialog = false;

    sub: Subscription;

    plotly_chart: PlotlyChart;

    constructor(
        monitoringservice: MonitoringService,
        synchronous_spike_line: boolean,
        height: number,
        column_number: number,
        display_index: number,
        metric: string,
        value_type: string,
        job_data: any
    ) {
        this.monitoringservice = monitoringservice;
        this.synchronous_spike_line = synchronous_spike_line;
        this.height = height;
        this.column_number = column_number;
        this.no_data_padding_top = 80;
        if (height < 200) this.no_data_padding_top = 40;

        this.sub = this.monitoringservice.TimelinePropagation.subscribe(
            (data) => {
                this.performAction(data);
            }
        );

        this.display_index = display_index;

        this.metric = metric;

        /* get partition */
        this.partition = 'default';
        var partition_str = job_data[0]['partition_name'];
        var partition_list: string[] = this.monitoringservice.getPartitions();
        for (let i = 0; i < partition_list.length; i++) {
            if (partition_str.includes(partition_list[i])) {
                this.partition = partition_list[i];
                break;
            }
        }

        this.display_num = this.monitoringservice.getDisplays(
            this.partition,
            job_data[0]['start_unixtime']
        ).length;

        /* get io information for the current partition */
        if (this.metric == 'io' || this.metric == 'io_meta') {
            var io_infos = this.monitoringservice.getIOinfos(this.partition);
            this.io_category = this.metric;
            this.metric = io_infos[this.metric][0];
        }

        this.value_type = value_type;
        if (this.metric == 'job_mem_used') this.value_type = 'aggregated';
        this.unit = '';

        /* check if information about shared resources is needed */
        this.shared_resources = false;
        if (
            this.metric != 'cpu_usage' &&
            this.metric != 'ipc' &&
            this.metric != 'flops' &&
            this.metric != 'job_mem_used' &&
            !this.metric.includes('gpu')
        ) {
            if (Number(job_data[0]['exclusive']) != 1) {
                //check if job is using all available cores
                var job_core_num = job_data[0]['core_count'];
                var job_node_num = job_data[0]['node_count'];
                var partition_core_num =
                    this.monitoringservice.getPartitionSpec(
                        job_data[0]['partition_name']
                    )['cpu_num'];
                if (job_core_num / job_node_num != partition_core_num)
                    this.shared_resources = true;
            }
        }

        this.auto_range = true;
        if (this.metric == 'cpu_usage' || this.metric == 'gpu_usage')
            this.auto_range = false;

        /* create display caption */
        this.createDisplayCaption();

        this.start_x1 = job_data[0]['start_unixtime'];
        this.start_offset = 0;
        this.is_live = false;

        if (job_data[0]['job_state'] != 'running') {
            this.start_x2 = job_data[0]['end_unixtime'];
        } else {
            this.is_live = true;
            this.start_x2 = Math.round(new Date().getTime() / 1000);
        }

        /* limit duration to two weeks */
        if (this.start_x2 - this.start_x1 > 1209600)
            this.start_x2 = this.start_x1 + 1209600;

        this.x1 = this.start_x1;
        this.x2 = this.start_x2;

        this.timeline_visible = false;
        this.histogram_visible = false;
        this.timeline_show_message = true;

        this.zoom_stack = [];
        this.zoom_stack.push([this.x1, this.x2]);

        // determine issue string if contained in job_data
        this.checkForIssue(this.metric, job_data[0]);

        // check for smt description
        const smt_mode = this.monitoringservice.getPartitionSpec(
            job_data[0]['partition_name']
        )['smt_mode'];

        if (
            smt_mode > 1 &&
            (this.metric == 'cpu_usage' ||
                this.metric == 'ipc' ||
                this.metric == 'flops')
        )
            this.checkSMT(this.metric, smt_mode);

        if (this.metric == 'job_mem_used') {
            this.aggregated_mem = 0;
            let node_count = job_data[0]['node_count'];
            let job_smt_mode = 1;
            if (job_data[0]['smt_mode']) job_smt_mode = job_data[0]['smt_mode'];
            let core_count = job_data[0]['core_count'] * job_smt_mode;
            let mem_per_cpu = job_data[0]['mem_per_cpu'];
            let mem_per_node = job_data[0]['mem_per_node'];

            if (
                job_data[0]['exclusive'] == 1 ||
                (mem_per_cpu == null && mem_per_node == 0)
            ) {
                // --exclusive flag or --mem=0 set by user
                this.aggregated_mem =
                    this.monitoringservice.getPartitionSpec(
                        job_data[0]['partition_name']
                    )['mem_per_node'] * node_count;
            } else {
                if (!mem_per_node && mem_per_cpu)
                    this.aggregated_mem = mem_per_cpu * core_count;
                else if (mem_per_node && !mem_per_cpu)
                    this.aggregated_mem = mem_per_node * node_count;
            }

            // console.log('Total Memory in GB', this.aggregated_mem / 10 ** 9);
        }

        this.job_data = job_data;
        this.loading = true;

        /* create timeline menu */
        this.createMenu();
        this.checkMenu();
        if (this.metric == 'job_mem_used') {
            // use aggreagted type in menu per default
            this.selectMenuItem('aggregated', 'select');
        }

        // create plotly chart
        this.plotly_chart = new PlotlyChart(
            this.monitoringservice,
            this.display_index,
            this.display_num,
            this.height - 33,
            this.start_x1,
            this.start_x2
        );

        this.getTimelineData(this.x1, this.x2);
    }

    createMenu() {
        // create IO menu for IO metrics
        if (this.metric.includes('_io')) {
            this.createIOMenu();
        }

        // set auto range for cpu_usage and gpu_usage
        if (this.metric == 'cpu_usage' || this.metric == 'gpu_usage') {
            this.menu_items.push({
                label: 'Set Auto Range',
                id: 'range',
                icon: undefined,
                command: () => {
                    this.selectMenuItem('range', 'toggle');
                    this.internalRelayout('range');
                },
            });
            this.menu_items.push({
                separator: true,
            });
        }

        // get remaining items from global timeline menu
        var first_separator_skipped: boolean = false;
        for (let item of this.monitoringservice.getTimelineMenu()) {
            if (item['id'] == 'spike_line') continue;
            if (item['id'] == 'mean_line') continue;
            if ('separator' in item && !first_separator_skipped) {
                first_separator_skipped = true;
                continue;
            }
            if ('separator' in item) this.menu_items.push(item);
            else {
                this.menu_items.push({
                    label: item['label'],
                    id: item['id'],
                    icon: item['checked'] ? 'fas fa-check' : undefined,
                    command: () => {
                        this.selectMenuItem(item['id'], item['state']);
                        this.internalRelayout(item['id']);
                    },
                });
            }
        }
    }

    createIOMenu() {
        // get IO data for the current partition
        var io_partition_data = this.monitoringservice.getIOinfos(
            this.partition
        )[this.io_category];

        // get all IO definitions
        var io_type_definitions = this.monitoringservice.getFileSystems(
            this.io_category
        );

        var first_io_type_selected: boolean = false;
        for (let io_type of io_partition_data) {
            var io_definition = io_type_definitions[io_type];

            this.menu_items.push({
                label: io_definition['label'],
                id: io_type,
                icon: first_io_type_selected ? undefined : 'fas fa-check',
                command: () => {
                    this.selectMenuItem(io_type, 'io_select');
                    this.changeIOType(
                        io_type,
                        io_type_definitions[io_type]['options']
                    );
                    this.metric = io_type;
                    this.show_only_default_io_timelines = true;
                    this.timeline_visible = true;
                    this.createDisplayCaption();
                    this.internalRelayout();
                    // disable enabled value types in top menu
                    this.checkMenu(true);
                },
            });
            if (!first_io_type_selected) {
                first_io_type_selected = true;
                this.selected_io_menu_item_id = io_type;
            }
        }

        this.menu_items.push({
            label: 'Select I/O Operations',
            id: this.io_category + '_operations',
            items: this.createIOSubMenu(
                io_type_definitions[this.metric]['options']
            ),
        });
        this.menu_items.push({
            separator: true,
        });
    }

    createIOSubMenu(io_options: any) {
        var io_options_menu: any = [];
        io_options_menu.push({
            label: 'All Available Options',
            id: 'all_' + this.io_category + '_operations',
            icon: undefined,
            command: () => {
                this.selectMenuItem(
                    'all_' + this.io_category + '_operations',
                    'io_sub_select'
                );
                this.metric = this.selected_io_menu_item_id;
                this.show_only_default_io_timelines = false;
                this.timeline_visible = true;
                this.createDisplayCaption();
                this.internalRelayout();
                // disable enabled value types in top menu
                this.checkMenu(true);
            },
        });
        io_options_menu.push({
            label: 'Default Options',
            id: 'default_' + this.io_category + '_operations',
            icon: 'fas fa-check',
            command: () => {
                this.selectMenuItem(
                    'default_' + this.io_category + '_operations',
                    'io_sub_select'
                );
                this.metric = this.selected_io_menu_item_id;
                this.show_only_default_io_timelines = true;
                this.timeline_visible = true;
                this.createDisplayCaption();
                this.internalRelayout();
                // disable enabled value types in top menu
                this.checkMenu(true);
            },
        });
        this.selected_io_submenu_item_id =
            'default_' + this.io_category + '_operations';
        io_options_menu.push({
            separator: true,
        });
        for (let key in io_options) {
            //console.log(key, io_options[key]);
            io_options_menu.push({
                label: io_options[key],
                id: key,
                icon: undefined,
                command: () => {
                    this.selectMenuItem(key, 'io_sub_select');
                    this.metric = key;
                    this.current_io_metric_caption = io_options[key];
                    this.timeline_visible = true;
                    this.createDisplayCaption();
                    this.internalRelayout();
                    // enable all disabled value types in top menu
                    this.checkMenu(false);
                },
            });
        }
        this.io_sub_menu_num = io_options_menu.length;
        return io_options_menu;
    }

    changeIOType(io_type: string, io_options: any) {
        // change io submenu
        const topMenuItem = this.menu_items.find(
            (item) => item.id === this.io_category + '_operations'
        );
        // console.log('changeIOType', io_type, io_options, topMenuItem);
        if (topMenuItem) {
            topMenuItem.items = this.createIOSubMenu(io_options);
            this.selected_io_menu_item_id = io_type;
        }
    }

    selectMenuItem(id: string, state: string) {
        if (state == 'io_sub_select') {
            const topMenuItem = this.menu_items.find(
                (item) => item.id === this.io_category + '_operations'
            );
            if (topMenuItem && topMenuItem.items) {
                const submenuItem = topMenuItem.items.find(
                    (item) => item.id === id
                );
                if (submenuItem) {
                    if (id != this.selected_io_submenu_item_id) {
                        const last_item = topMenuItem.items.find(
                            (item) =>
                                item.id === this.selected_io_submenu_item_id
                        );
                        if (last_item) {
                            submenuItem['icon'] = 'fas fa-check';
                            last_item['icon'] = undefined;
                            this.selected_io_submenu_item_id = id;
                        }
                    }
                }
            }
        } else {
            const item = this.menu_items.find((item) => item.id === id);
            if (item) {
                if (state == 'toggle') {
                    item['icon'] =
                        item['icon'] === 'fas fa-check'
                            ? undefined
                            : 'fas fa-check';
                } else if (state == 'select') {
                    if (id != this.selected_menu_item_id) {
                        const last_item = this.menu_items.find(
                            (item) => item.id === this.selected_menu_item_id
                        );
                        if (last_item) {
                            item['icon'] = 'fas fa-check';
                            last_item['icon'] = undefined;
                            this.selected_menu_item_id = id;
                        }
                    }
                } else if (state == 'io_select') {
                    if (id != this.selected_io_menu_item_id) {
                        const last_item = this.menu_items.find(
                            (item) => item.id === this.selected_io_menu_item_id
                        );
                        if (last_item) {
                            item['icon'] = 'fas fa-check';
                            last_item['icon'] = undefined;
                            this.selected_io_menu_item_id = id;
                        }
                    }
                }
            }
        }
    }

    checkMenu(disable_item: boolean = true) {
        if (this.metric == 'node_power') {
            // node_power does not provide the following value types
            for (let item of this.menu_items) {
                if (
                    ['meanstddev', 'best', 'lowest', 'best_lowest'].includes(
                        item.id!
                    )
                )
                    item.disabled = disable_item;
            }
        } else if (this.io_category.includes('io')) {
            // disable the following value types for multiple traces
            for (let item of this.menu_items) {
                if (
                    [
                        'min_mean_max',
                        'meanstddev',
                        'best',
                        'lowest',
                        'best_lowest',
                    ].includes(item.id!)
                )
                    item.disabled = disable_item;
            }
        }
    }

    checkIOSubMenu(available_io_metrics: string[]) {
        const topMenuItem = this.menu_items.find(
            (item) => item.id === this.io_category + '_operations'
        );
        var hide_cnt = 0;
        if (topMenuItem) {
            for (let item of topMenuItem.items!) {
                if (
                    item.id &&
                    !item.id.includes('all_') &&
                    !item.id.includes('default_')
                ) {
                    if (!available_io_metrics.includes(item.id)) {
                        item.visible = false;
                        hide_cnt++;
                    }
                }
            }
            this.io_sub_menu_num = topMenuItem.items!.length - hide_cnt;
        }
    }

    unsubscribeTimelineChart() {
        this.sub.unsubscribe();
    }

    createDisplayCaption(unix_time?: number) {
        // console.log(
        //     'metric',
        //     this.metric,
        //     this.monitoringservice.getTimelineCaptions(this.metric)
        // );

        var value_type = this.value_type;

        switch (this.value_type) {
            case 'min':
                value_type = 'minimum';
                break;
            case 'max':
                value_type = 'maximum';
                break;
            case 'meanstddev':
                value_type = 'mean + stddev';
                break;
            default:
                // Keep the original value if no case matches
                break;
        }

        var caption_def = this.monitoringservice.getTimelineCaptions(
            this.metric
        );

        // check if we have an io submenu item
        if (caption_def[2] == 'submenu-option') {
            this.caption =
                caption_def[0] +
                ' ' +
                this.current_io_metric_caption +
                ' in ' +
                caption_def[1] +
                ' ' +
                caption_def[3]
                    .replace('%', value_type)
                    .replace('%', caption_def[4] + 's');
        } else {
            var caption_str = caption_def[0];
            if (caption_def[1] != '') caption_str += ' in ' + caption_def[1];

            if (unix_time) {
                this.caption =
                    caption_str +
                    ' after ' +
                    this.formatDuration(unix_time - this.start_x1);
            } else {
                if (
                    value_type == 'min_mean_max' ||
                    value_type == 'best_lowest'
                ) {
                    this.caption =
                        caption_str +
                        ' ' +
                        caption_def[2]
                            .replace('% ', '')
                            .replace('%', caption_def[3] + 's');
                } else {
                    if (value_type == 'best' || value_type == 'lowest')
                        this.caption =
                            caption_str +
                            ' ' +
                            caption_def[2]
                                .replace('% across', value_type)
                                .replace('%', caption_def[3]);
                    else
                        this.caption =
                            caption_str +
                            ' ' +
                            caption_def[2]
                                .replace('%', value_type)
                                .replace('%', caption_def[3] + 's');
                }
            }
        }
    }

    formatDuration(seconds: number | string): string {
        const ONE_MINUTE = 60,
            ONE_HOUR = 3600,
            ONE_DAY = 86400;

        const parsedSeconds = Number(seconds);
        if (isNaN(parsedSeconds)) return String(seconds);

        let d = Math.floor(parsedSeconds / ONE_DAY);
        let h = Math.floor((parsedSeconds % ONE_DAY) / ONE_HOUR);
        let m = Math.floor((parsedSeconds % ONE_HOUR) / ONE_MINUTE);
        let s = parsedSeconds % ONE_MINUTE;

        return [
            d ? `${d}\u200Ad` : '',
            h ? `${h}\u200Ah` : '',
            m ? `${m}\u200Amin` : '',
            `${s}\u200As`,
        ]
            .filter(Boolean)
            .join(' ');
    }

    fromUnixToHumanTime(unix_time: number[]) {
        return unix_time.map((time) =>
            this.monitoringservice.getFormattedTime(time)
        );
    }

    checkNullArray(array: any[]) {
        if (Array.isArray(array))
            return array.join().replace(/,/g, '').length === 0;
        return true;
    }

    getTimelineData(start: number, end: number) {
        this.monitoringservice
            .getTimelineData(
                start,
                end,
                this.metric,
                this.value_type,
                this.job_data,
                this.show_mean_line
            )
            .subscribe({
                next: (data: any) => {
                    // little hack if REST API returns error messages
                    if (this.metric == 'node_power' && 'error' in data)
                        data = [];

                    var timestamps: any[] = [];
                    var humantime: any[] = [];
                    //var timeline_num = 0;

                    var intern_timeline_visible = false;
                    var mean_treshold = [];

                    // // determine length of IO legend
                    // if (this.metric.includes('_io')) {
                    //     for (var key in data) {
                    //         if (key == 'unit' || key == 'timestamp') continue;
                    //         console.log('Trace name:', key);
                    //     }
                    // }

                    //
                    if (
                        this.metric == 'job_mem_used' &&
                        this.value_type == 'aggregated' &&
                        Object.keys(data).length > 0
                    ) {
                        // add horizontal line for aggregated allocated memory
                        data['requested'] = [];
                        data['requested'][0] = Array(
                            data['used'][0].length
                        ).fill(this.aggregated_mem);
                    }

                    var available_io_metrics: string[] = [];

                    // create timelines for current metric
                    var timelines: any = [];
                    for (var key in data) {
                        if (key == 'unit') {
                            // not needed anymore, since unit is in caption
                            continue;
                        }
                        if (key == 'timestamps') {
                            timestamps = data[key];
                            humantime = this.fromUnixToHumanTime(data[key]);
                            continue;
                        }

                        if (this.metric.includes('_io')) {
                            available_io_metrics.push(key);
                        }

                        var footprint_info = '';

                        /* Workaround for interpolation issue in influxdb: if minimum is required and we get only null values, replace them with zero values. Note, in case influxdb delivers negative values, the php backend replaces them with null values. */
                        if (
                            (this.value_type == 'min' ||
                                key == 'min' ||
                                this.value_type == 'lowest' ||
                                key == 'lowest') &&
                            data[key][0].length > 0 &&
                            !this.metric.includes('_io')
                        ) {
                            for (let i = 1; i < data[key][0].length; i++)
                                if (data[key][0][i] == null)
                                    data[key][0][i] = 0;
                        }

                        if (
                            data[key][0].length > 0 &&
                            this.checkNullArray(data[key][0]) == false
                        ) {
                            intern_timeline_visible = true;

                            /* beautification if only one timestamp and value */
                            if (timestamps.length == 1) {
                                timestamps.push(timestamps[0] + 30);
                                data[key][0].push(null);
                            }

                            // special hover infos
                            if (key == 'best' || key == 'lowest') {
                                if (data[key][1] != null) {
                                    footprint_info =
                                        '(' + data[key][1][key + '_node'] + ')';
                                }
                                if (this.metric == 'job_mem_used') {
                                    // remove :cpu from node_info
                                    if (footprint_info.includes(':'))
                                        footprint_info =
                                            footprint_info.split(':')[0] + ')';
                                }
                            }

                            var meanstddev = [];
                            if (this.value_type == 'meanstddev') {
                                meanstddev = data[key][2];
                            }

                            var timeline = {
                                name: key,
                                x: timestamps,
                                y: data[key][0],
                                text: humantime,
                                hover_info: footprint_info,
                                meanstddev: meanstddev,
                            };
                            timelines.push(timeline);
                        }

                        if (data[key][1] != null) {
                            mean_treshold.push(data[key][1]['mean']);
                        }
                    }

                    this.loading = false;
                    if (intern_timeline_visible == true) {
                        this.plotly_chart.plotTimelines(
                            this.metric,
                            timelines,
                            this.value_type,
                            this.auto_range,
                            this.show_only_default_io_timelines,
                            this.height - 33
                        );

                        // hide all not available io metrics in submenu
                        if (this.metric.includes('_io')) {
                            this.checkIOSubMenu(available_io_metrics);
                        }

                        if (
                            this.show_mean_line &&
                            !this.metric.includes('_io')
                        ) {
                            this.plotly_chart.addHorizontalLine(
                                timestamps[0],
                                mean_treshold[0],
                                timestamps[timestamps.length - 1],
                                mean_treshold[0]
                            );
                        }

                        this.timeline_visible = true;
                    }
                    // little hack to switch io filesystems
                    // if (this.metric.includes('_io'))
                    //     this.timeline_visible = true;
                },
                error: (e) => {
                    console.error('Error ', this.metric);
                    this.loading = false;
                    this.show_token_expired_dialog =
                        this.monitoringservice.showExpiredSessionDialog(e);
                },
            });
    }

    performAction(data: any) {
        for (var key in data) {
            if (key == 'zoom') {
                this.redraw(data['zoom']);
            }
            if (key == 'reset-zoom') {
                this.resetZoom();
            }
            if (key == 'undo-zoom') {
                this.undoZoom();
            }
            if (key == 'relayout') {
                this.internalRelayout();
            }
            if (key == 'setting_relayout') {
                this.settingRelayout(
                    data[key]['height'],
                    data[key]['column_number']
                );
            }
        }
    }

    redraw(range: any) {
        this.timeline_visible = false;
        if (this.histogram_visible) {
            this.histogram_visible = false;
            this.createDisplayCaption();
        }
        this.loading = true;
        var x1 = range[0];
        var x2 = range[1];
        // console.log('redraw', x1,x2);
        /* do not allow zooming ranges smaller than 60s */
        if (x2 - x1 < 60) {
            x2 += 60 - x2 + x1;
        }
        /* remember current zoom for undo */
        this.zoom_stack.push([x1, x2]);

        this.getTimelineData(x1, x2);
    }

    resetZoom() {
        // console.log("is_live=", this.is_live);
        this.timeline_visible = false;
        if (this.histogram_visible) {
            this.histogram_visible = false;
            this.createDisplayCaption();
        }
        this.loading = true;
        this.x1 = this.start_x1;
        if (this.is_live == true)
            this.start_x2 = Math.round(new Date().getTime() / 1000);
        this.x2 = this.start_x2;
        this.zoom_stack = [];
        this.zoom_stack.push([this.x1, this.x2]);
        this.getTimelineData(this.x1, this.x2);
    }

    propagateResetZoom() {
        this.monitoringservice.propagateTimelineAction({ 'reset-zoom': '' });
    }

    undoZoom() {
        /* remove last zoom from stack */
        this.zoom_stack.pop();
        /* remove zoom from stack again and use for redraw */
        var range = this.zoom_stack.pop();
        this.redraw(range);
    }

    propagateUndoZoom() {
        this.monitoringservice.propagateTimelineAction({ 'undo-zoom': '' });
    }

    internalRelayout(value_type = this.value_type) {
        if (value_type == 'range' && this.value_type == 'aggregated') {
            // do not replot if user changes auto range in aggregated mode
            this.auto_range = !this.auto_range;
            return;
        }
        if (value_type == 'range') {
            // if user switches between auto range and fixed range of y-axis
            this.plotly_chart.toggleAutoFixedRangeOnYAxis(
                this.display_index,
                this.auto_range,
                this.height - 33
            );
            this.auto_range = !this.auto_range;
            return;
        }
        if (this.timeline_visible || this.histogram_visible) {
            if (this.value_type != value_type) {
                this.value_type = value_type;
                this.createDisplayCaption();
                this.histogram_visible = false;
            }
            this.redraw(this.zoom_stack.pop());
        }
    }

    settingRelayout(height: number, column_number: number) {
        this.height = height;
        this.column_number = column_number;
        this.no_data_padding_top = 80;
        if (height < 200) this.no_data_padding_top = 40;

        this.resetZoom();
    }

    getClosestDataPointIndex(data: any, xValue: number): number {
        let closestIndex = 0;
        let minDistance = Infinity;

        for (let i = 0; i < data.x.length; i++) {
            const distance = Math.abs(data.x[i] - xValue);
            if (distance < minDistance) {
                minDistance = distance;
                closestIndex = i;
            }
        }

        return closestIndex;
    }

    roundToNearestMinute(unixtime: number) {
        /* round to nearest minute */
        unixtime = unixtime - (unixtime % 60) + 60;
        return unixtime;
    }

    relayout(event: any) {
        // check autosize
        if (event['autosize']) {
            this.plotly_chart.adjustLegendAndCreateXLabels(this.height);
        }
        /* check if user has zoomed */
        if (
            event['xaxis.range[0]'] != undefined &&
            event['xaxis.range[1]'] != undefined
        ) {
            let zoom_start = Number(event['xaxis.range[0]']);
            let zoom_end = Number(event['xaxis.range[1]']);

            // check whether the user has moved the x-axis
            if (
                zoom_end - zoom_start ==
                this.plotly_chart.current_range[1] -
                    this.plotly_chart.current_range[0]
            )
                return;

            // check if zoomed to the very left
            if (zoom_start == this.plotly_chart.current_range[0])
                zoom_start = this.start_x1;

            // check if zoomed to the very right
            if (zoom_end == this.plotly_chart.current_range[1])
                zoom_end = this.start_x2;

            zoom_start = this.roundToNearestMinute(zoom_start);
            zoom_end = this.roundToNearestMinute(zoom_end);

            var range: number[];
            range = [zoom_start, zoom_end];

            this.monitoringservice.propagateTimelineAction({ zoom: range });
        }
    }

    backToTimeline() {
        this.histogram_visible = false;
        this.createDisplayCaption();
        this.timeline_visible = true;
    }

    getTimelineHistogramData(unix_time: number) {
        this.monitoringservice
            .getTimelineHistogramData(unix_time, this.metric, this.job_data)
            .subscribe({
                next: (data: any) => {
                    this.plotly_chart.plotHistogram(
                        this.metric,
                        data['x'],
                        data['bin_size'],
                        this.height - 33
                    );
                    this.loading = false;
                },
                error: (e) => {
                    this.loading = false;
                    this.show_token_expired_dialog =
                        this.monitoringservice.showExpiredSessionDialog(e);
                },
            });
    }

    drawHistogram(event: any) {
        if (this.value_type == 'meanstddev') {
            this.loading = true;
            var points = event['points'];
            var unix_time = points[0]['x'];
            //var unix_time = this.fromDurationToUnixTime(duration_time, 0);
            this.timeline_visible = false;
            this.createDisplayCaption(unix_time);
            this.getTimelineHistogramData(unix_time);
            this.histogram_visible = true;
        }
    }

    checkForIssue(metric: string, job_data: any): void {
        // console.log('checkForIssue: ', metric, job_data);
        var idle_core_ratio = job_data['core_idle_ratio'];
        var idle_gpu_ratio = job_data['gpu_idle_ratio'];
        switch (true) {
            case metric === 'cpu_usage' && idle_core_ratio > 0.2:
                this.issue_tagged = true;
                this.issue = `CPUs not used efficiently\n
                    Idle Core Ratio: ${idle_core_ratio}
                    Unused Core Ratio: ${job_data['core_unused_ratio']}
                    Core Load Imbalance: ${job_data['core_load_imbalance']}`;
                break;
            case metric === 'gpu_usage' && idle_gpu_ratio > 0.2:
                this.issue_tagged = true;
                this.issue = `GPUs not used efficiently\n
                        Idle GPU Ratio: ${idle_gpu_ratio}
                        Unused GPU Ratio: ${job_data['gpu_unused_ratio']}
                        GPU Load Imbalance: ${job_data['gpu_load_imbalance']}`;
                break;
            case metric === 'job_mem_used' || metric === 'mem_used':
                let mem_leak_found = false;
                if (job_data['mem_leak'] > 0) {
                    this.issue_tagged = true;
                    this.issue = `Memory leak suspected`;
                    mem_leak_found = true;
                }
                if (job_data['unused_mem'] >= 0.8) {
                    this.issue_tagged = true;
                    let umem = job_data['unused_mem'] * 100;
                    if (mem_leak_found) this.issue += '\n\n';
                    this.issue += `${umem}% of the requested memory was not used`;
                }
                break;
            case metric === 'lustre_io' && job_data['io_blocking'] > 9:
                this.issue_tagged = true;
                this.issue = `I/O blocking in ${job_data['io_blocking']} phases suspected`;
                break;
            case metric === 'lustre_io_meta' &&
                job_data['io_congestion'] > 1000:
                this.issue_tagged = true;
                this.issue = `Too many I/O meta operations
                            Number of open+close operations at a measurement point: ${job_data['io_congestion']}`;
                break;
            default:
                this.issue_tagged = false;
                break;
        }
    }

    checkSMT(metric: string, smt_mode: number) {
        this.smt_metric = true;

        switch (metric) {
            case 'cpu_usage':
                this.smt_info =
                    'In this partition, simultaneous multithreading (SMT) is enabled, and thus the CPU usage per hardware thread is recorded. To save disk space, the average CPU usage of the hardware threads is stored for each core.';
                break;
            case 'ipc':
                this.smt_info =
                    'In this partition, simultaneous multithreading (SMT) is enabled, and thus the IPC value per hardware thread is recorded. To save disk space, the aggregated IPC value of the hardware threads is stored for each core.';
                break;
            case 'flops':
                this.smt_info =
                    'In this partition, simultaneous multithreading (SMT) is enabled, and thus the FLOPS value per hardware thread is recorded. To save disk space, the aggregated FLOPS value of the hardware threads is stored for each core.';
                break;
            default:
                this.smt_info = '';
        }
    }
}
