import { Component } from '@angular/core';
import { MonitoringService } from '../service/monitoring.service';
import { ActivatedRoute, Router } from '@angular/router';
import { JobBaseComponent } from './job-base.component';

@Component({
    selector: 'app-node',
    templateUrl: './job-base.component.html',
    styleUrls: ['./job-base.component.css'],
})
export class NodeComponent extends JobBaseComponent {
    stateKey: string = 'node';
    col_defines_deep_copy: any; // needed to change column headers
    sample_time: number | undefined = undefined;
    constructor(
        monitoringservice: MonitoringService,
        route: ActivatedRoute,
        router: Router
    ) {
        super(monitoringservice, route, router);
    }

    override componentInit() {
        this.stateKey = 'node';
        this.monitoringservice.setActiveComponent(this.stateKey);
        this.monitoringservice.Reload.subscribe((data) => {
            if (this.stateKey == data) {
                console.log('Reload node data');
                this.reloadTable();
            }
        });
        this.show_node_partition_selection = true;
        var sample_time = this.roundUnixTimestampTo5Minutes(
            this.monitoringservice.global_end
        );
        this.node_partition_date_string = this.unixTimestampToDateString(
            sample_time,
            this.monitoringservice.getDateFormat(true, true)
        );
    }

    override setTableColums() {
        this.resizableColumns = false;
        this.cols = [];

        /* get Node table */
        var node_table =
            this.monitoringservice.getTableDefinition('Node_Table');

        for (let index of node_table) {
            this.cols.push(this.col_defines.get(index));
        }
        this.caption_total_name = 'Nodes';
    }

    // Function to split node_name into prefix and numeric part
    extractPrefixAndNumber(nodeName: string): {
        prefix: string;
        number: number;
    } {
        const match = nodeName.match(/^([a-zA-Z]+)(\d+)$/);
        if (!match) {
            // If no numeric part, treat the entire nodeName as prefix and number as 0
            return { prefix: nodeName, number: 0 };
        }
        return { prefix: match[1], number: parseInt(match[2], 10) };
    }

    // Custom sort function
    nodeSort(a: { node_name: string }, b: { node_name: string }): number {
        const { prefix: prefixA, number: numberA } =
            this.extractPrefixAndNumber(a.node_name);
        const { prefix: prefixB, number: numberB } =
            this.extractPrefixAndNumber(b.node_name);

        // First, compare prefixes lexicographically
        if (prefixA < prefixB) return -1;
        if (prefixA > prefixB) return 1;

        // If prefixes are the same, compare numeric parts
        return numberA - numberB;
    }

    override getTableData() {
        var sample_time: number;
        if (this.sample_time) {
            sample_time = this.sample_time;
        } else {
            sample_time = this.roundUnixTimestampTo5Minutes(
                this.monitoringservice.global_end
            );
        }
        this.node_partition_date_string = this.unixTimestampToDateString(
            sample_time,
            this.monitoringservice.getDateFormat(true, true)
        );
        if (sessionStorage.getItem(this.sessionStorageName)) {
            console.log(
                'Session found for ',
                this.stateKey,
                this.sessionStorageName
            );
            this.rows = JSON.parse(
                sessionStorage.getItem(this.sessionStorageName)!
            );
            this.loading = false;
        } else {
            this.monitoringservice.getNodeData(sample_time).subscribe({
                next: (data) => {
                    // special treatment for capella node c1...c144
                    if (Array.isArray(data)) {
                        // Sort the array
                        data.sort(this.nodeSort.bind(this));
                    }
                    this.rows = data;
                    try {
                        sessionStorage.setItem(
                            this.sessionStorageName,
                            JSON.stringify(data)
                        );
                    } catch {
                        console.log(
                            'SessionStorage failed due to quota. Reset all stored sessions.'
                        );
                        sessionStorage.clear();
                    }
                    this.loading = false;
                },
                error: (e) => {
                    this.loading = false;
                    this.show_token_expired_dialog =
                        this.monitoringservice.showExpiredSessionDialog(e);
                },
            });
        }
    }

    roundUnixTimestampTo5Minutes(timestamp: number): number {
        return (timestamp -= timestamp % 300);
    }

    override changeNodeSample(value: string): void {
        var unix_time = this.dateStringToUnixTimestamp(
            value,
            this.monitoringservice.getDateFormat(true, true)
        );
        this.sample_time = this.roundUnixTimestampTo5Minutes(unix_time);
        this.node_partition_date_string = this.unixTimestampToDateString(
            this.sample_time,
            this.monitoringservice.getDateFormat(true, true)
        );
    }

    override substractFiveMinutes() {
        if (this.node_partition_date_string) {
            var unix_time = this.dateStringToUnixTimestamp(
                this.node_partition_date_string,
                this.monitoringservice.getDateFormat(true, true)
            );
            unix_time -= 300;
            this.node_partition_date_string = this.unixTimestampToDateString(
                unix_time,
                this.monitoringservice.getDateFormat(true, true)
            );
            this.sample_time = unix_time;
        }
    }

    override addFiveMinutes() {
        if (this.node_partition_date_string) {
            var unix_time = this.dateStringToUnixTimestamp(
                this.node_partition_date_string,
                this.monitoringservice.getDateFormat(true, true)
            );
            unix_time += 300;
            var current_date = new Date();
            if (unix_time <= Math.floor(current_date.getTime() / 1000)) {
                this.node_partition_date_string =
                    this.unixTimestampToDateString(
                        unix_time,
                        this.monitoringservice.getDateFormat(true, true)
                    );
                this.sample_time = unix_time;
            }
        }
    }

    override getTableNodeInputClass() {
        if (this.monitoringservice.getTimeFormat() == 'en-US')
            return 'table-node-us';
        return 'table-node-gb';
    }

    override getTableNodeSampleMask() {
        if (this.monitoringservice.getTimeFormat() == 'en-US')
            return '99/99/99 99:99 aa';
        return '99/99/99 99:99';
    }
}
