import { Component } from '@angular/core';
import { MonitoringService } from '../service/monitoring.service';
import { ActivatedRoute, Router } from '@angular/router';
import { JobBaseComponent } from './job-base.component';

@Component({
    selector: 'app-job-detail',
    templateUrl: './job-base.component.html',
    styleUrls: ['./job-base.component.css'],
})
export class JobDetailComponent extends JobBaseComponent {
    stateKey: string = 'job-detail';
    table_definition = 'Jobdetail_Table';
    constructor(
        monitoringservice: MonitoringService,
        route: ActivatedRoute,
        router: Router
    ) {
        super(monitoringservice, route, router);
    }

    override componentInit() {
        if (this.monitoringservice.getFilterItem('live') == 'true') {
            // we only show live mode buttons, if the route starts with the Live tab
            if (
                this.live_tab_start &&
                this.monitoringservice.enablePendingJobs()
            )
                this.show_live_modes = true;

            if (this.monitoringservice.getLiveMode() == 'pending') {
                this.stateKey = 'job-detail-pending';
                this.caption_total_name = 'Runs';
                this.table_definition = 'Jobdetail_Pend_Table';
            } else {
                this.stateKey = 'job-detail-running';
                this.caption_total_name = 'Runs';
                this.table_definition = 'Jobdetail_Live_Table';
            }
        } else {
            this.stateKey = 'job-detail';
            this.caption_total_name = 'Runs';
            this.table_definition = 'Jobdetail_Table';
        }

        // used for reloading job table after changing date in menubar
        this.monitoringservice.setActiveComponent(this.stateKey);
        this.monitoringservice.Reload.subscribe((data) => {
            if (this.stateKey == data) {
                console.log('Reload ', this.stateKey);
                this.reloadTable();
            }
        });
    }

    override setTableColums() {
        this.cols = [];
        if (
            this.monitoringservice.getFilterItem('live') == 'true' &&
            this.monitoringservice.getLiveMode() == 'pending'
        ) {
            this.cols.push(this.col_defines.get('job_id'));
        } else {
            this.cols.push(this.col_defines.get('job_id_link'));
        }
        if (this.project == '')
            this.cols.push(this.col_defines.get('project_name'));
        if (this.user == '') this.cols.push(this.col_defines.get('user_name'));
        if (this.job == '') this.cols.push(this.col_defines.get('job_name'));

        /* get table columns from appConfig.json */
        var jobdetail_table = this.monitoringservice.getTableDefinition(
            this.table_definition
        );
        for (let index of jobdetail_table) {
            this.cols.push(this.col_defines.get(index));
        }
    }

    override getTableData() {
        if (
            sessionStorage.getItem(this.sessionStorageName) &&
            !this.monitoringservice.getFilterItem('live')
        ) {
            console.log(
                'Session found for ',
                this.stateKey,
                this.sessionStorageName
            );
            this.rows = JSON.parse(
                sessionStorage.getItem(this.sessionStorageName)!
            );
            this.loading = false;
        } else {
            var pend = false;
            if (
                this.monitoringservice.getFilterItem('live') == 'true' &&
                this.monitoringservice.getLiveMode() == 'pending'
            )
                pend = true;
            this.monitoringservice
                .getJobDetailData(this.user, this.job, this.project, pend)
                .subscribe({
                    next: (data) => {
                        this.rows = data;
                        try {
                            sessionStorage.setItem(
                                this.sessionStorageName,
                                JSON.stringify(data)
                            );
                        } catch {
                            console.log(
                                'SessionStorage failed due to quota. Reset all stored sessions.'
                            );
                            sessionStorage.clear();
                        }
                        this.loading = false;
                    },
                    error: (e) => {
                        this.loading = false;
                        this.show_token_expired_dialog =
                            this.monitoringservice.showExpiredSessionDialog(e);
                    },
                });
        }
    }
}
