import { Component, HostListener } from '@angular/core';
import { OnInit, AfterViewInit, OnDestroy, ViewChild } from '@angular/core';
import { MonitoringService } from '../service/monitoring.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Table } from 'primeng/table';
import { MenuItem } from 'primeng/api';
import { SortEvent } from 'primeng/api';
import { SelectItem } from 'primeng/api';
import { parse, format } from 'date-fns';

// Mapping custom format placeholders to date-fns format tokens
const date_format_mapping: { [key: string]: string } = {
    '%m': 'MM', // Month as two digits
    '%d': 'dd', // Day as two digits
    '%Y': 'yyyy', // 4-digit year
    '%y': 'yy', // 2-digit year
    '%H': 'HH', // Hour (24-hour format)
    '%I': 'hh', // Hour (12-hour format)
    '%M': 'mm', // Minutes as two digits
    '%S': 'ss', // Seconds as two digits
    '%p': 'a', // AM/PM
};

@Component({
    template: '',
})
export class JobBaseComponent implements OnInit, AfterViewInit, OnDestroy {
    @ViewChild(Table, { static: false }) tableComponent: Table | any;

    constructor(
        protected monitoringservice: MonitoringService,
        protected route: ActivatedRoute,
        protected router: Router
    ) {}

    table_scroll_height = 500;
    loading: boolean = false;
    resizableColumns: boolean = false;
    rows: any;
    rows_per_page: number = 10;
    cols: any[] = [];
    col_defines: any;
    caption_total_name: string = '';
    column_sort: boolean = false;
    paginator: boolean = false;

    project: string = '';
    user: string = '';
    job: string = '';
    jobid: string = '';
    job_start: string = '';
    partition: string = '';
    footprint_route: string = '';

    show_job_script_button: boolean = false;
    show_job_script: boolean = false;
    job_script: string = '';
    show_download_button: boolean = false;
    footprint_loading: boolean = false;
    footprint_rows: any;

    // timeline layout
    timeline_heights: SelectItem[] = [
        { label: 'large', value: 248 },
        { label: 'small', value: 148 },
    ];
    selected_timeline_height: number = 148;

    timeline_column_numbers: SelectItem[] = [
        { label: '1', value: 1 },
        { label: '2', value: 2 },
        { label: '3', value: 3 },
        { label: '4', value: 4 },
        { label: '6', value: 6 },
    ];
    selected_timeline_column_number: number = 2;
    timeline_column_number = 2;

    // timeline menu
    show_timeline_menu: boolean = false;
    selected_timeline_menu_item_id: string = 'mean';
    timeline_menu_items: MenuItem[] = [];
    timeline_mean_line: boolean = false;
    timeline_synchronous_spike_line: boolean = true;

    // for live mode
    live_stateOptions: any[] = [
        { label: 'Pending', value: 'pending' },
        { label: 'Running', value: 'running' },
    ];
    live_tab_start: boolean = false;
    show_live_modes: boolean = false;

    // for chart component
    showChartButton: boolean = false;
    showCharts: boolean = false;

    // for timeline
    show_timelines: boolean = false;
    show_text_for_short_jobs: boolean = false;
    timelines: any[] = [];
    tag_value: string = '';

    // for issue
    issue_stateOptions: any[] = [
        { label: 'All', value: 'all' },
        { label: 'CPU', value: 'cpu' },
        { label: 'GPU', value: 'gpu' },
        { label: 'Other', value: 'other' },
    ];
    show_issue_selection: boolean = false;

    // for node view
    node_partition_stateOptions: any[] = [];
    node_partition_date_string: string | undefined;
    show_node_partition_selection: boolean = false;

    // navigation_bar
    navigation_array: any[] = [];

    // for session storage
    sessionStorageName: string = '';

    recorded_footprints: number = 0;
    recorded_jobs: number = 0;
    displays: any[] = [];

    download_data: string[] = ['meta', 'data'];
    show_download_error_dialog: boolean = false;
    download_error_text: string = 'Cannot download job data.';
    show_token_expired_dialog = false;

    ngOnInit() {
        // create timeline menu for job-chart component
        for (let item of this.monitoringservice.getTimelineMenu()) {
            if ('separator' in item) this.timeline_menu_items.push(item);
            else {
                this.timeline_menu_items.push({
                    label: item['label'],
                    id: item['id'],
                    icon: item['checked'] ? 'fas fa-check' : undefined,
                    command: () => {
                        this.selectMenuItem(item['id'], item['state']);
                        if (item['id'] == 'spike_line') {
                            this.timeline_synchronous_spike_line =
                                !this.timeline_synchronous_spike_line;
                            this.toggleTimelineSpikeLine(
                                this.timeline_synchronous_spike_line
                            );
                        } else {
                            if (item['id'] == 'mean_line')
                                this.timeline_mean_line =
                                    !this.timeline_mean_line;
                            this.updateTimelineData(this.timeline_mean_line);
                        }
                    },
                });
            }
        }

        this.col_defines =
            this.monitoringservice.getTableDefinition('Definition');

        this.caption_total_name = 'Results';
        this.column_sort = true;
        this.paginator = true;

        // fill node_partition_stateOptions
        for (const partition of this.monitoringservice.getActivePartitions()) {
            this.node_partition_stateOptions.push({
                label: partition,
                value: partition,
            });
        }

        this.getRouteParams();
        this.componentInit();
        this.resizableColumns = true;
        this.setTableColums();
        this.getTableDataWithLoadingSpinner();
    }

    ngAfterViewInit() {
        setTimeout(() => {
            this.adjustScrollHeight();
        }, 500);
    }

    ngOnDestroy() {}

    dateStringToUnixTimestamp(dateString: string, dateFormat: string): number {
        if (dateString == 'n/a') return 0;
        // Replace the placeholders in the input format
        const parsedDateFormat = dateFormat.replace(
            /%[mdYyHIMSap]/g,
            (match) => date_format_mapping[match]
        );

        // Parse the date string
        const parsedDate = parse(dateString, parsedDateFormat, new Date());

        // Return the Unix timestamp (seconds since 1970-01-01)
        return Math.floor(parsedDate.getTime() / 1000);
    }

    unixTimestampToDateString(
        unixTimestamp: number,
        dateFormat: string
    ): string {
        // Replace custom format with date-fns format tokens
        const parsedDateFormat = dateFormat.replace(
            /%[mdYyHIMSap]/g,
            (match) => date_format_mapping[match]
        );

        // Convert Unix timestamp to JavaScript Date object
        const date = new Date(unixTimestamp * 1000); // Multiply by 1000 to convert seconds to milliseconds

        // Format the date using the parsed format
        return format(date, parsedDateFormat);
    }

    shortenDateString(
        dateString: string,
        sourceFormat: string,
        targetFormat: string
    ): string {
        // Replace placeholders in the source format and target format with date-fns format tokens
        const parseFormat = sourceFormat.replace(
            /%[mdYyHIMSap]/g,
            (match) => date_format_mapping[match]
        );
        const formatFormat = targetFormat.replace(
            /%[mdYyHIMSap]/g,
            (match) => date_format_mapping[match]
        );
        // Parse the original date string using the source format
        const parsedDate = parse(dateString, parseFormat, new Date());

        // Format the date using the target format
        const formattedDate = format(parsedDate, formatFormat);

        return formattedDate;
    }

    formatColumnName(column: string): string {
        // Extract numeric part and pad with leading zeros to 4 digits
        const numberPart = column.substring(1).padStart(4, '0');
        // Return the formatted column
        return `c${numberPart}`;
    }

    customSort(event: SortEvent) {
        event.data!.sort((data1, data2) => {
            let value1 = data1[event.field!];
            let value2 = data2[event.field!];
            let result = null;

            if (event.field === 'reservation_name') {
                if (value1 === 'none' && value2 !== 'none') {
                    return event.order!;
                }
                if (value2 === 'none' && value1 !== 'none') {
                    return -event.order!;
                }
                // Compare strings normally (case-insensitive) for non-"none" values
                return event.order! * value1.localeCompare(value2);
            }

            if (
                event.field == 'start_time' ||
                event.field == 'end_time' ||
                event.field == 'submit_time' ||
                event.field == 'estimate_start_time'
            ) {
                value1 = this.dateStringToUnixTimestamp(
                    value1,
                    this.monitoringservice.getDateFormat()
                );
                value2 = this.dateStringToUnixTimestamp(
                    value2,
                    this.monitoringservice.getDateFormat()
                );
            }

            if (
                [
                    'core_duration_idle',
                    'gpu_duration_idle',
                    'core_duration',
                    'sum_core_duration',
                ].includes(event.field!)
            ) {
                value1 = this.monitoringservice.revertLargeNumber(value1);
                value2 = this.monitoringservice.revertLargeNumber(value2);
            }

            // special sorting for capella c1,c2,...c144
            if (event.field == 'node_name') {
                if (value1[0] == 'c' && value2[0] == 'c') {
                    value1 = this.formatColumnName(value1);
                    value2 = this.formatColumnName(value2);
                }
            }

            if (value1 == null && value2 != null) result = -1;
            else if (value1 != null && value2 == null) result = 1;
            else if (value1 == null && value2 == null) result = 0;
            else if (typeof value1 === 'string' && typeof value2 === 'string')
                result = value1.localeCompare(value2);
            else result = value1 < value2 ? -1 : value1 > value2 ? 1 : 0;

            return event.order! * result;
        });
    }

    refreshTableAndCharts() {
        this.refreshTableData();
        this.monitoringservice.propagateTimelineAction({ 'reset-zoom': '' });
    }

    reloadTable() {
        this.resetTable();
        this.getTableDataWithLoadingSpinner();
    }

    getTableDataWithLoadingSpinner() {
        this.loading = true;
        this.getTableData();
        this.getChartData();
    }

    getRouteParams() {
        var footprint_link = '';
        this.monitoringservice.setFootprintRoute(false);

        //add filter item 'live' if live state was refreshed by user
        this.route.data.subscribe((data) => {
            // if route starts from the beginning, clear current filter
            if (data['clear_filter'] == 'true') {
                this.monitoringservice.clearFilter();
            }
            if (data['clear_footprint_filter'] == 'true') {
                this.monitoringservice.clearFootprintFilter();
            }

            // check of route starts from the Live tab
            if (data['live_tab_start'] == 'true') {
                this.live_tab_start = true;
            }
            /* check if current route contains footprint charts */
            if (data['footprint_link']) {
                footprint_link = data['footprint_link'];
                this.monitoringservice.setFootprintRoute(true);
            }
            if (data['state'] == 'live')
                this.monitoringservice.addFilterItem('live', 'true');
            else {
                if (data['state'] != 'all')
                    this.monitoringservice.addFilterItem('live', 'false');
            }

            this.route.params.subscribe((params) => {
                // remember session after selecting a footprint bar or point
                let footprint_filter_string: string = '';
                if (this.monitoringservice.getFootprintFilter()) {
                    footprint_filter_string = JSON.stringify(
                        this.monitoringservice.getFootprintFilter()
                    );
                }

                this.sessionStorageName =
                    this.route.snapshot.url.join('') +
                    '_' +
                    footprint_filter_string;

                console.log('session name: ', this.sessionStorageName);

                // // check if current user is valid
                // if (this.monitoringservice.getAdminStatus() == false) {
                //     if (
                //         this.route.routeConfig?.path?.split('/', 1)[0] !=
                //             this.monitoringservice.getUser() &&
                //         params['user'] != this.monitoringservice.getUser()
                //     ) {
                //         console.log(
                //             'Wrong user:',
                //             this.monitoringservice.getUser()
                //         );
                //         this.router.navigate(['/']);
                //     }
                // }

                this.project = '';
                this.user = '';
                this.job = '';
                this.jobid = '';
                this.job_start = '';
                this.partition = '';

                this.navigation_array = [];

                console.log('params:', params);

                if (this.monitoringservice.getUserRole() == 'user')
                    this.user = this.monitoringservice.getUser();

                for (var item in params) {
                    if (item == 'project') {
                        var back_tabs_num;
                        if (footprint_link == 'project') back_tabs_num = 2;
                        else back_tabs_num = 1;
                        this.navigation_array.push([
                            'Project',
                            params['project'],
                            back_tabs_num,
                        ]);
                        this.project = params['project'];
                    } else if (item == 'user') {
                        if (
                            this.monitoringservice.getUserRole() == 'admin' ||
                            this.monitoringservice.getUserRole() == 'pi'
                        ) {
                            var back_tabs_num;
                            if (footprint_link == 'user') back_tabs_num = 2;
                            else back_tabs_num = 1;
                            this.navigation_array.push([
                                'User',
                                params['user'],
                                back_tabs_num,
                            ]);
                        }
                        this.user = params['user'];
                    } else if (item == 'job') {
                        var job_name = params['job'];
                        if (job_name.length > 32)
                            job_name = params['job'].substring(0, 29) + '...';
                        var back_tabs_num;
                        if (footprint_link == 'job') back_tabs_num = 2;
                        else back_tabs_num = 1;
                        this.navigation_array.push([
                            'Job Name',
                            job_name,
                            back_tabs_num,
                        ]);
                        this.job = params['job'];
                    } else if (item == 'jobid') {
                        this.navigation_array.push([
                            'Job ID',
                            params['jobid'],
                            3,
                        ]);
                        this.jobid = params['jobid'];
                    } else if (item == 'start') {
                        this.job_start = params['start'];
                    } else if (item == 'partition') {
                        this.partition = params['partition'];
                    } else if (item == 'footprint') {
                        /* remove all line breaks */
                        var footprint = params['footprint'].replace(
                            /<br>/g,
                            ''
                        );
                        this.navigation_array.push(['Footprint', footprint, 1]);
                    } else if (item == 'filter') {
                        this.navigation_array.push([
                            'Search',
                            params['filter'],
                            2,
                        ]);
                    }
                }

                /* determine number of back tabs ('../') for navigation item */
                var last_back_tabs = 0;
                for (let i = this.navigation_array.length - 1; i >= 0; i--) {
                    this.navigation_array[i][2] += last_back_tabs;
                    last_back_tabs = this.navigation_array[i][2];
                }

                console.log('navigation_array: ', this.navigation_array);
            });
        });
    }

    backTabsString(num: number) {
        var back_tabs_string = './../';
        for (let i = 1; i < num; i++) back_tabs_string += '../';
        return back_tabs_string;
    }

    resetTable() {
        sessionStorage.clear();
        this.tableComponent.reset();
        this.tableComponent.clearState();
    }

    selectMenuItem(id: string, state: string) {
        const item = this.timeline_menu_items.find((item) => item.id === id);
        if (item) {
            if (state == 'toggle') {
                item['icon'] =
                    item['icon'] === 'fas fa-check'
                        ? undefined
                        : 'fas fa-check';
            }

            if (state == 'select') {
                if (id != this.selected_timeline_menu_item_id) {
                    const last_item = this.timeline_menu_items.find(
                        (item) =>
                            item.id === this.selected_timeline_menu_item_id
                    );
                    if (last_item) {
                        item['icon'] = 'fas fa-check';
                        last_item['icon'] = undefined;
                        this.selected_timeline_menu_item_id = id;
                    }
                }
            }
        }
    }

    getCurrentYear() {
        return this.monitoringservice.getCurrentYear();
    }

    // getHeight(): string {
    //     var x = document.getElementById('menubar')!.scrollHeight;
    //     var y = document.getElementById('fixedHeader')!.scrollHeight;
    //     return (
    //         'height:calc(100vh - ' +
    //         (x + y + 30).toString() +
    //         'px);overflow-y: auto;overflow-x: hidden;'
    //     );
    // }

    changeIssueSelection(event: any): void {
        this.monitoringservice.setIssueValue(event['value']);
        this.monitoringservice.setSortField(event['value']);
        this.setTableColums();
    }

    changeLiveMode(event: any): void {
        this.monitoringservice.setLiveMode(event['value']);
        this.componentInit();
        this.resizableColumns = true;
        this.setTableColums();
        this.getTableDataWithLoadingSpinner();
    }

    getFormattedRessourcesString(input: string): string {
        var n = 20;
        if (input.length > 100) n = 30;
        if (input.length > 1000) n = 40;

        const elements = [];
        let currentElement = '';

        for (let i = 0; i < input.length; i++) {
            currentElement += input[i];

            if (currentElement.length >= n && input[i] === ',') {
                elements.push(currentElement);
                currentElement = '';
            }
        }

        if (currentElement.length > 0) {
            elements.push(currentElement);
        }

        const formattedString = elements.join('\n');
        return formattedString;
    }

    shortenFormattedDateString(value: string): string {
        return this.shortenDateString(
            value,
            this.monitoringservice.getDateFormat(),
            this.monitoringservice.getDateFormat(true, true)
        );
    }

    adjustScrollHeight() {
        if (
            document.getElementById('menubar')!.clientHeight &&
            document.getElementById('pika-table-caption')!.clientHeight &&
            document.getElementById('pika-table-header')!.clientHeight
        ) {
            this.table_scroll_height =
                document.documentElement.clientHeight -
                document.getElementById('menubar')!.clientHeight -
                document.getElementById('pika-table-caption')!.clientHeight -
                document.getElementById('pika-table-header')!.clientHeight -
                80;

            // console.log(
            //     document.documentElement.clientHeight,
            //     document.getElementById('menubar')!.clientHeight,
            //     document.getElementById('pika-table-caption')!.clientHeight,
            //     document.getElementById('pika-table-header')!.clientHeight
            // );
        }
    }

    @HostListener('window:resize', ['$event'])
    onResize(event: any) {
        this.adjustScrollHeight();
    }

    changeNodeSample(value: string) {}
    substractFiveMinutes() {}
    addFiveMinutes() {}
    componentInit() {}
    setTableColums() {}
    getTableData() {}
    refreshTableData() {}
    getChartData() {}
    updateTimelineData(mean_line: boolean) {}
    getFootprintData(job_id: string, start: string) {}
    showJobScript() {}
    toggleTimelineSpikeLine(synchronous_spike_line: boolean) {}
    getFootprintTable() {}
    addChart(footprint1: string, footprint2: string, class_num: number) {}
    resetCharts() {}
    changeSettings() {}
    getColumnClass() {}
    getTableNodeInputClass() {}
    getTableNodeSampleMask() {}

    download(
        jobid: string,
        job_start: string,
        partition: string,
        json_data: string[]
    ) {}
}
