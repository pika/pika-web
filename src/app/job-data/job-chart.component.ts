import { Component, isDevMode } from '@angular/core';
import { MonitoringService } from '../service/monitoring.service';
import { ActivatedRoute, Router } from '@angular/router';
import { JobBaseComponent } from './job-base.component';
import { TimelineChart } from './timeline';
import { Time } from '@angular/common';

@Component({
    selector: 'app-chart',
    templateUrl: './job-base.component.html',
    styleUrls: ['./job-base.component.css'],
})
export class JobChartComponent extends JobBaseComponent {
    stateKey: string = 'job-chart';
    // detailed_footprints_init: boolean;
    override show_timeline_menu: boolean = false;
    override timelines: TimelineChart[] = [];
    override show_timelines: boolean = false;
    override tag_value: string = '';
    dragged_timeline_index: number | undefined = undefined;

    constructor(
        monitoringservice: MonitoringService,
        route: ActivatedRoute,
        router: Router
    ) {
        super(monitoringservice, route, router);
    }

    override componentInit() {
        if (this.monitoringservice.getFilterItem('live') == 'true')
            this.stateKey = 'job-chart-live';
        else this.stateKey = 'job-chart';

        this.monitoringservice.setActiveComponent(this.stateKey);
        this.monitoringservice.Reload.subscribe((data) => {
            if (this.stateKey == data) {
                console.log('Reload ', this.stateKey);
                this.reloadTable();
            }
        });
        this.show_timeline_menu = true;
        // this.selected_timeline_menu_item_id = 'Mean';

        this.timelines = [];
        this.show_timelines = false;
        this.tag_value = '';
    }

    override ngOnDestroy() {
        for (let timeline of this.timelines)
            timeline.unsubscribeTimelineChart();
        this.timelines = [];
    }

    loadTableColums(smt = false, gpu_count = false) {
        this.cols = [];
        if (this.project == '')
            this.cols.push(this.col_defines.get('project_name'));
        if (this.user == '') this.cols.push(this.col_defines.get('user_name'));
        if (this.job == '') this.cols.push(this.col_defines.get('job_name'));

        var table_def = 'Jobchart_Table';
        if (this.monitoringservice.getFilterItem('live') == 'true')
            table_def = 'Jobchart_Live_Table';

        /* check for live job if search component was used */
        if (this.navigation_array[0].includes('Search')) {
            this.monitoringservice
                .getUniqueJobData(this.jobid, this.job_start, this.partition)
                .subscribe((rows: any) => {
                    //console.log("Status: ", rows[0]["job_state"]);
                    if (rows[0]['job_state'] == 'running')
                        table_def = 'Jobchart_Live_Table';
                    var jobchart_table =
                        this.monitoringservice.getTableDefinition(table_def);
                    for (let index of jobchart_table) {
                        if (
                            index == 'job_energy' &&
                            !this.monitoringservice.enableShowEnergy()
                        ) {
                            continue;
                        }
                        if (
                            index == 'job_energy_efficiency' &&
                            !this.monitoringservice.enableShowEnergy()
                        ) {
                            continue;
                        }
                        if (index == 'smt_mode' || index == 'gpu_count') {
                            if (index == 'smt_mode' && smt)
                                this.cols.push(this.col_defines.get(index));
                            if (index == 'gpu_count' && gpu_count)
                                this.cols.push(this.col_defines.get(index));
                        } else this.cols.push(this.col_defines.get(index));
                    }
                });
        } else {
            var jobchart_table =
                this.monitoringservice.getTableDefinition(table_def);
            for (let index of jobchart_table) {
                if (
                    index == 'job_energy' &&
                    !this.monitoringservice.enableShowEnergy()
                ) {
                    continue;
                }
                if (
                    index == 'job_energy_efficiency' &&
                    !this.monitoringservice.enableShowEnergy()
                ) {
                    continue;
                }
                if (index == 'smt_mode' || index == 'gpu_count') {
                    if (index == 'smt_mode' && smt)
                        this.cols.push(this.col_defines.get(index));
                    if (index == 'gpu_count' && gpu_count)
                        this.cols.push(this.col_defines.get(index));
                } else this.cols.push(this.col_defines.get(index));
            }
        }
    }

    override setTableColums() {
        this.caption_total_name = 'None';
        this.paginator = false;
        this.column_sort = false;
        this.show_job_script_button = true;
        this.show_job_script = false;
        this.show_download_button = false;
    }

    override showJobScript() {
        if (this.show_job_script == false) this.show_job_script = true;
        else this.show_job_script = false;
        setTimeout(() => {
            this.monitoringservice.jobDataTableHeaderResize();
        }, 500);
    }

    getFieldIndex(field: string) {
        var index = -1;
        for (let i = 0; i < this.cols.length; i++) {
            if (this.cols[i]['field'] == field) {
                index = i;
                break;
            }
        }
        return index;
    }

    loadJobMetadata(partition_spec: any, rows: any, loadColumn = true) {
        // get tag value
        if (rows[0]['tags'] > 0)
            this.tag_value = this.monitoringservice
                .getJobtags(String(rows[0]['tags']))
                .replace(/<br>/g, '');

        if (loadColumn) {
            var smt = false;
            var gpu_num = false;
            if (partition_spec['smt_mode'] > 1) smt = true;
            if (partition_spec['gpu_num'] > 0 && rows[0]['gpu_count'] > 0)
                gpu_num = true;

            this.loadTableColums(smt, gpu_num);
        }

        if (rows[0]['job_state'] != 'running') this.show_download_button = true;
        this.rows = rows;

        if (rows[0]['job_script'] && rows[0]['job_script'] != '(null)')
            this.job_script = rows[0]['job_script']
                .replace(/ /g, '\u00A0')
                .replace(/(\r\n|\r|\n){2,}/g, '$1\n')
                .replace(/\\"/g, '"')
                .split('\n');
        else this.show_job_script_button = false;
    }

    override refreshTableData() {
        this.monitoringservice
            .getUniqueJobData(this.jobid, this.job_start, this.partition)
            .subscribe({
                next: (rows: any) => {
                    // get partition specification
                    var partition_spec =
                        this.monitoringservice.getPartitionSpec(
                            rows[0]['partition_name']
                        );
                    this.loadJobMetadata(partition_spec, rows, false);
                },
                error: (e) => {
                    this.loading = false;
                    this.show_token_expired_dialog =
                        this.monitoringservice.showExpiredSessionDialog(e);
                },
            });
    }

    override getTableData() {
        this.monitoringservice
            .getUniqueJobData(this.jobid, this.job_start, this.partition)
            .subscribe({
                next: (rows: any) => {
                    // get partition specification
                    var partition_spec =
                        this.monitoringservice.getPartitionSpec(
                            rows[0]['partition_name']
                        );
                    // console.log("partition spec: ", partition_spec);

                    this.loadJobMetadata(partition_spec, rows);

                    this.timelines = [];

                    // default partition
                    var partition = 'default';

                    // get current partition name
                    var list: string[] = this.monitoringservice.getPartitions();
                    // check first if string perfectly matches
                    if (list.find((e) => e === rows[0]['partition_name'])) {
                        partition = rows[0]['partition_name'];
                    } else {
                        for (let i = 0; i < list.length; i++) {
                            if (rows[0]['partition_name'].includes(list[i])) {
                                partition = list[i];
                                break;
                            }
                        }
                    }

                    // get display names for the current partition
                    var displays: string[] = this.monitoringservice.getDisplays(
                        partition,
                        rows[0]['start_unixtime']
                    );

                    var metadata = this.rows;
                    delete metadata[0]['job_script'];

                    // create timelines
                    for (let i = 0; i < displays.length; i++) {
                        let metric = displays[i];

                        this.timelines.push(
                            new TimelineChart(
                                this.monitoringservice,
                                this.timeline_synchronous_spike_line,
                                this.selected_timeline_height,
                                this.selected_timeline_column_number,
                                i,
                                metric,
                                this.selected_timeline_menu_item_id,
                                metadata
                            )
                        );
                    }
                    this.loading = false;

                    // only show timelines if jobs took at least 60s
                    if (rows[0]['job_state'] != 'running') {
                        if (
                            Number(rows[0]['end_unixtime']) -
                                Number(rows[0]['start_unixtime']) <
                            60
                        ) {
                            this.show_text_for_short_jobs = true;
                        }
                    }

                    if (!this.show_text_for_short_jobs)
                        this.show_timelines = true;

                    var delay = 500;
                    if (isDevMode()) delay = 2000;
                    setTimeout(() => {
                        this.monitoringservice.jobDataTableHeaderResize();
                    }, delay);
                },
                error: (e) => {
                    this.loading = false;
                    this.show_token_expired_dialog =
                        this.monitoringservice.showExpiredSessionDialog(e);
                },
            });
    }

    override updateTimelineData(mean_line: boolean = false) {
        for (let timeline of this.timelines) {
            // the following value types can only used for single traces
            if (
                timeline.metric.includes('_io') &&
                (this.selected_timeline_menu_item_id == 'min_mean_max' ||
                    this.selected_timeline_menu_item_id == 'meanstddev' ||
                    this.selected_timeline_menu_item_id == 'best_lowest' ||
                    this.selected_timeline_menu_item_id == 'best' ||
                    this.selected_timeline_menu_item_id == 'lowest' ||
                    this.selected_timeline_menu_item_id == 'best_lowest')
            ) {
                continue;
            } else if (
                timeline.metric == 'node_power' &&
                (this.selected_timeline_menu_item_id == 'meanstddev' ||
                    this.selected_timeline_menu_item_id == 'best_lowest' ||
                    this.selected_timeline_menu_item_id == 'best' ||
                    this.selected_timeline_menu_item_id == 'lowest' ||
                    this.selected_timeline_menu_item_id == 'best_lowest')
            ) {
                continue;
            } else {
                timeline.selectMenuItem(
                    this.selected_timeline_menu_item_id,
                    'select'
                );
                timeline.show_mean_line = mean_line;
                if (timeline.metric == 'node_power')
                    timeline.show_mean_line = false;
                timeline.internalRelayout(this.selected_timeline_menu_item_id);
            }
        }
    }

    override toggleTimelineSpikeLine(synchronous_spike_line: boolean) {
        for (let timeline of this.timelines)
            timeline.synchronous_spike_line = synchronous_spike_line;
    }

    override download(
        jobid: string,
        job_start: string,
        partition: string,
        download_data: string[]
    ) {
        console.log(download_data);

        if (download_data.length === 0) return;

        this.loading = true;

        this.monitoringservice
            .download(jobid, job_start, partition, download_data)
            .subscribe({
                next: (blobData: Blob) => {
                    if (!('error' in blobData)) {
                        const url = URL.createObjectURL(blobData); // Create a URL for the downloaded data
                        const a = document.createElement('a'); // Create a new anchor element
                        a.href = url; // Set the anchor element's href attribute to the URL of the downloaded data
                        if (blobData.type === 'application/json') {
                            //Check for blobData Mime Type
                            a.download = download_data.toString() + '.json'; // Set the filename for the downloaded data
                        } else if (
                            blobData.type === 'application/x-zip-compressed'
                        ) {
                            //Check for blobData Mime Type
                            a.download =
                                jobid +
                                '_' +
                                job_start +
                                '_' +
                                partition +
                                '.zip'; // Set the filename for the downloaded data
                        }
                        document.body.appendChild(a);
                        a.click(); // Simulate a click on the anchor element to start the download
                        URL.revokeObjectURL(url); // Revoke the URL to free up memory
                    } else {
                        this.show_download_error_dialog = true;
                    }
                    this.loading = false;
                },
                error: (e) => {
                    console.error('Error here', e);
                    this.loading = false;
                    this.show_download_error_dialog = true;
                },
            });
    }

    override getColumnClass(): string {
        return `col-12 md:col-${12 / this.timeline_column_number}`;
    }

    override changeSettings() {
        this.timeline_column_number = this.selected_timeline_column_number;
        this.monitoringservice.propagateTimelineAction({
            setting_relayout: {
                height: this.selected_timeline_height,
                column_number: this.selected_timeline_column_number,
            },
        });
    }

    moveItem<T>(array: T[], fromIndex: number, toIndex: number): void {
        // Validate indices
        if (
            fromIndex < 0 ||
            fromIndex >= array.length ||
            toIndex < 0 ||
            toIndex >= array.length
        ) {
            console.error('Invalid indices');
            return;
        }

        // Remove the item from the original position
        const [item] = array.splice(fromIndex, 1);

        // Insert the item into the new position
        array.splice(toIndex, 0, item);
    }

    onDragStart(event: any, index: number) {
        this.dragged_timeline_index = index;
    }

    onDragEnd(event: any, index: number) {
        this.dragged_timeline_index = undefined;
    }

    onDrop(event: any, index: number) {
        var swap: boolean = true;
        if (this.dragged_timeline_index != undefined) {
            if (this.dragged_timeline_index != index) {
                // swapping is more intuitive
                if (swap)
                    [
                        this.timelines[this.dragged_timeline_index],
                        this.timelines[index],
                    ] = [
                        this.timelines[index],
                        this.timelines[this.dragged_timeline_index],
                    ];
                else
                    this.moveItem(
                        this.timelines,
                        this.dragged_timeline_index,
                        index
                    );
            }
        }
    }
}
