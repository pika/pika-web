import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { firstValueFrom, timeout } from 'rxjs';
import { environment } from '../environments/environment';
import { Router } from '@angular/router';

@Injectable()
export class AppConfigService {
    private appConfig: any;
    private server_version: string | undefined = undefined;
    private client_version: string | undefined = undefined;
    private client_server_compatible: boolean = true;
    private backend_user: any;
    private token: any;
    private token_expired_timestamp: any;
    private is_pi: boolean;
    private is_admin: boolean;
    private partition_data: any;
    private job_tag_data: any;

    constructor(private http: HttpClient, private router: Router) {
        this.appConfig = {};
        this.backend_user = 'unknown';
        this.token = {};
        this.token_expired_timestamp = 0;
        this.is_pi = false;
        this.is_admin = false;
        this.partition_data = {};
        this.job_tag_data = {};
        this.client_version = environment.version;
    }

    checkClientServerVersion(
        client_version: string | undefined,
        server_version: string | undefined
    ) {
        if (client_version != server_version) {
            console.error('Error: client and server version do not match.');
            this.router.navigate(['/error']);
            this.client_server_compatible = false;
        }
    }

    async loadAppConfig() {
        const timestamp = new Date().getTime();
        // prevent the browser to cache this file
        this.appConfig = await firstValueFrom(
            this.http.get(`./assets/appConfig.json?ts=${timestamp}`)
        );
        try {
            const backendConfig = (await firstValueFrom(
                this.http
                    .get(this.appConfig.AppConfig.backend + '/credential')
                    .pipe(timeout(3000))
            )) as Record<string, any>;
            this.server_version = backendConfig['version'];
            console.log('pika-client: version', this.client_version);
            console.log('pika-server: version', this.server_version);
            this.checkClientServerVersion(
                this.client_version,
                this.server_version
            );
            this.backend_user = backendConfig['user_name'];
            this.token['user'] = backendConfig['user_token'];
            if (backendConfig['is_pi'] == true) {
                this.is_pi = backendConfig['is_pi'];
                this.token['pi'] = backendConfig['pi_token'];
            }
            if (backendConfig['is_admin'] == true) {
                this.is_admin = true;
                this.token['admin'] = backendConfig['admin_token'];
            }
            let token_expire_seconds =
                backendConfig['token_expire_minutes'] * 60;
            this.token_expired_timestamp =
                Math.floor(Date.now() / 1000) + token_expire_seconds;
        } catch {
            console.error('Error: Cannot get user from backend.');
        }
        const headers = {
            'content-type': 'application/json',
            Authorization: this.token['user'],
        };
        try {
            this.partition_data = await fetch(
                this.appConfig.AppConfig.backend + '/partition',
                { headers: headers }
            ).then((response) => response.json());
            // this.partition_data = { "alpha" = {"name": "alpha", "cpu_num": 50 ...}, "haswell" = {...}}
        } catch {
            console.error('Error: Cannot get partition data from backend.');
        }
        try {
            this.job_tag_data = await fetch(
                this.appConfig.AppConfig.backend + '/job_tag',
                { headers: headers }
            ).then((response) => response.json());
        } catch {
            console.error('Error: Cannot get job tag data from backend.');
        }
    }

    getConfig() {
        return this.appConfig;
    }

    clientServerCompatible() {
        return this.client_server_compatible;
    }

    getUserFromBackend() {
        return this.backend_user;
    }

    getPIStatus() {
        return this.is_pi;
    }

    getAdminStatus() {
        return this.is_admin;
    }

    getTokenFromBackend(user_role: string) {
        return this.token[user_role];
    }

    getTokenExpiredTimestamp() {
        return this.token_expired_timestamp;
    }

    getPartitionData() {
        return this.partition_data;
    }

    getJobTags(): { [key: string]: string } {
        return this.job_tag_data;
    }
}
